1.  # Introducción
    

El presente documento tiene por objetivo definir los Términos y Condiciones, en adelante “TYC”, proporcionados por OSSE - Oficina de Soluciones y Servicios Empresariales para la utilización de Los Servicios de O3 - OSSE Odoo Online, OSSE Enterprise Edition, Oxe360 Enterprise Edition y Oxe360 SaaS en adelante “Los Servicios”. Estos TYC pueden ser consultados de forma pública en los siguientes links de **OSSE** [http://www.osse.com.pe/page/terminos-y-condiciones](http://www.osse.com.pe/page/terminos-y-condiciones) y en el de Oxe360 [http://www.oxe360.com/page/terminos-y-condiciones](http://www.oxe360.com/page/terminos-y-condiciones)

2.  # Aceptación de los Términos y Condiciones
    

El presente texto regula el uso de Los Servicios denominados O3 - OSSE Odoo Online, Oxe360 SaaS provistos por la Oficina de Soluciones y Servicios Empresariales S.A.C. (en adelante, OSSE) en relación a El Software OSSE Enterprise Edition u Oxe360 Enterprise Edition, en cualquier tipo de hosting, sea en la nube o local.

El suscribirse a Los Servicios le atribuye a usted la condición de Usuario (en adelante, “El Usuario”) e implica la aceptación plena y sin reservas de todas y cada una de las disposiciones incluidas en el presente texto. Asimismo, en el supuesto en el que se utilicen Los Servicios en nombre de una organización, también se están aceptando estos TYC para toda esa organización.

Para mayor información sobre los SLA de OSSE para con sus clientes los puede consultar desde el siguiente link: [SLA OSSE](https://docs.google.com/document/d/1dGA8rxCGyWOTEL4X7KbNTTlnVpYXslGNIXWYRGQNeUI/edit).

3.  # Definiciones
    

Usuario. Cualquier cuenta de usuario activa con acceso al Software y que pueda crear o editar cualquier tipo de información en el sistema.

Aplicación. Una Aplicación es un grupo especializado de funciones disponibles mediante la cual se puede registrar y consultar datos y transacciones de una forma estructurada.

Bug. Se considera un error o falla del Software que resulte en una parada completa, o violación de seguridad, y que no sea causada directamente por una instalación, configuración, o registro de datos defectuosa. El incumplimiento de las especificaciones o requisitos específicos se considerará como errores a discreción de OSSE.

Versiones Cubiertas. A menos que se especifique lo contrario, Los Servicios provistos bajo estos TYC son aplicables solo a las Versiones Cubiertas de El Software, que incluyen las 3 (tres) últimas versiones principales lanzadas.

4.  # Política de Uso Aceptable de Los Servicios
    

El Usuario se compromete a utilizar Los Servicios de conformidad con la normativa vigente y los TYC, así como los demás avisos, reglamentos de uso e instrucciones puestos en su conocimiento por cualquier medio. OSSE no se encuentra obligada a realizar desarrollos o ajustes sobre Los Servicios; en consecuencia, El Usuario deberá utilizar Los Servicios  "AsIs" es decir con las características vigentes al momento de la contratación. Los Servicios podrán ser modificados por OSSE en el tiempo, teniendo OSSE exclusiva y total potestad de excluir, sustituir o incluir funciones, así como detener, suspender o modificar Los Servicios en cualquier momento sin previo aviso, en tanto se considere que dichas modificaciones favorecerán el funcionamiento de Los Servicios.

El Usuario estará impedido de realizar modificaciones en Los Servicios, sin la autorización previa de OSSE. Asimismo sus servicios podrán suspenderse o resolverse por violación de su contrato de prestación de servicios con OSSE o de la siguiente Política de Uso Aceptable:

1.  ## Abuso
    

El Usuario se abstendrá de utilizar cualquiera de Los Servicios con fines o efectos ilícitos, prohibidos por la Ley, lesivos de los derechos e intereses de terceros, o que de cualquier forma puedan dañar, inutilizar, sobrecargar, deteriorar o impedir la normal utilización de Los Servicios, los equipos informáticos o los documentos, archivos y toda clase de contenidos almacenados en cualquier equipo informático de OSSE, de otros Usuarios o de cualquier Usuario de Internet incluyendo:

-   Uso de una cuenta internet o computadora sin autorización del propietario;
    
-   Recopilar o usar direcciones electrónicas, nombres de pantalla u otros identificadores sin el consentimiento de la persona identificada (incluyendo sin limitación, suplantación de identidad (phishing), fraude por internet, robo de clave secreta, uso de araña web (spidering) y extracción de datos web (harvesting);
    
-   Recopilación o uso de información sin el consentimiento del propietario de la información;
    
-   Acceso o uso no autorizado de datos, sistemas o redes, incluyendo algún intento de sondear, escanear o probar la vulnerabilidad de un sistema o red o para violar la seguridad o medidas de autenticación sin la autorización expresa del propietario del sistema o red;
    
-   Uso de cabecera de paquete TCP-IP falso en correo electrónico o participaciones en grupos de noticias u otros foros;
    
-   Uso del servicio para distribuir software que, de manera encubierta, recopile información sobre un usuario o transmita, de manera encubierta, información sobre el usuario;
    
-   Cualquier conducta que pueda resultar en la toma de represalias contra la red o sitio web de OSSE, o los empleados, funcionarios u otros agentes de OSSE, incluyendo tener un comportamiento que dé como resultado el que un servidor sea objeto de un ataque de negación de servicio (DoS);
    
-   Monitorear datos o tráfico de cualquier red o sistema sin la autorización expresa del propietario del sistema o red;
    
-   Interferencia con el servicio de un usuario de Efact u otra red incluyendo, sin sentido limitativo, bombardeo de correo, inundación, intentos deliberados de sobrecargar un sistema y ataques de transmisión.
    

2.  ## Uso excesivo de los recursos del sistema
    

El Usuario no podrá utilizar Los Servicios de manera que interfiera con la operación normal de otros Usuarios que comparten los entornos, o que consuma una porción desproporcionada de los recursos del sistema. Se restringirá el uso de procesos o tareas automáticas o pre-programadas en Los Servicios si tienen un impacto negativo en el sistema o si ocasionan conflicto con el uso de otros Usuarios de Los Servicios compartidos.

Si los datos están infectados con un virus, o si de otro modo se corrompen, y existe la posibilidad de infectar o corromper el sistema o los datos de otros Usuarios que se encuentran almacenados en el mismo sistema, El Usuario debe aceptar la puesta en cuarentena o eliminación de los datos almacenados en Los Servicios compartidos.

3.  ## Pruebas de vulnerabilidad
    

Queda terminantemente prohibido intentar sondear, escanear, penetrar o probar la vulnerabilidad de Los Servicios, redes o cualquier otro software de OSSE, o violar la seguridad o medidas de autenticación de OSSE, mediante técnicas pasivas o intrusivas, sin el consentimiento escrito expreso de OSSE.

4.  ## Contenido Ofensivo
    

El Usuario podrá utilizar Los Servicios para publicar, transmitir o almacenar, contenido o enlaces a un contenido que OSSE considere que:

-   Constituye, muestra, fomenta, promueve o se relaciona de alguna manera con pornografía o actos sexuales no consensuales;
    
-   Es excesivamente violento, incita la violencia, amenaza con violencia o tiene contenido acosador o mensaje de odio;
    
-   Es injusta o engañosa de conformidad con las leyes de protección al consumidor de alguna jurisdicción, incluyendo cartas cadena y pirámides fraudulentas;
    
-   Es difamatorio o viola la privacidad de una persona;
    
-   Crea un riesgo a la seguridad o salud de una persona, crea un riesgo a la seguridad o salud públicas, compromete la seguridad nacional o interfiere con una investigación a cargo de las fuerzas del orden público;
    
-   Expone de manera inadecuada secretos de fabricación u otra información confidencial o exclusiva de otra persona;
    
-   Tiene la intención de ayudar a otros a eludir las protecciones de derechos de propiedad intelectual técnica;
    
-   Infringe los derechos de autor, marcas de productos o de servicio u otro derecho de propiedad de otra persona;
    
-   Promueve drogas ilegales, viola las leyes de control de exportación, se relaciona con juegos ilegales o tráfico ilegal de armas;
    
-   Promueve ideas relacionadas con el proselitismo ideológico o político;
    
-   Es de otro modo ilegal o solicita una conducta que es ilegal de conformidad con la legislación aplicable a usted o a OSSE; o
    
-   Es de otro modo malicioso, fraudulento o puede resultar en represalias contra Efact por parte de visitantes ofendidos.
    

5.  ## Propiedad Intelectual y Derechos de Autor
    

Los contenidos e información, así como las obras existentes en Los Servicios son de propiedad de OSSE o de sus respectivos autores o de las personas y entidades que poseen la titularidad de los derechos de mismos, de acuerdo a lo que se indique en cada obra en particular.

El Software, otras tecnologías y know-how utilizados para proporcionar Los Servicios están protegidos por derechos de autor, marcas registradas y otras leyes en el marco normativo nacional e internacional.

En ese sentido, El Usuario no podrá copiar, modificar, cargar, descargar ni compartir archivos a menos que tenga el derecho de hacerlo. El Usuario, será completamente responsable de lo que copie, modifique, comparta, cargue, descargue o utilice de alguna otra manera durante el uso de Los Servicios. No debe cargar software espía ni ningún otro software malintencionado a Los Servicios.

No podrá usar Los Servicios para descargar, publicar, distribuir, o de otro modo copiar o usar de alguna manera algún texto, música, software, arte, imágenes u otro trabajo protegido por la ley de derechos de autor a menos que cuente con la autorización expresa del propietario de los derechos de autor del trabajo para copiarlo de dicha manera; o esté de otro modo autorizado por la ley de derechos de autor en vigor para copiar el trabajo de dicha manera.

6.  ## Buenas prácticas para el uso de facturación electrónica
    

Es responsabilidad del usuario documentarse seguir los lineamientos y buenas prácticas recomendadas para el uso del servicio PSE-SUNAT para le emisión de CPE - Comprobantes de Pago Electrónicos publicada en:

[C.7.1 Política de Buenas Prácticas Para el Uso del Servicio PSE](https://drive.google.com/open?id=1JD-aydXr1fWiFufIEcvp3va2h28LXQLg)

7.  ## Otros
    

No podrá registrarse para usar Los Servicios bajo un nombre falso, ni usar, en relación con cualquiera de Los Servicios, una tarjeta de crédito u otros medios de pagos no válidos, fraudulentos o no autorizados.

OSSE no se encuentra obligado a realizar desarrollos o modificaciones sobre Los Servicios; en consecuencia, el Usuario deberá utilizar Los Servicios con las características vigentes al momento de la contratación.

Los Servicios pueden modificarse en el tiempo, teniendo OSSE la plena potestad de excluir, sustituir o incluir funciones, así como detener, suspender o modificar Los Servicios en cualquier momento sin previo aviso, en tanto se considere de manera parcial que dichas modificaciones favorecen el funcionamiento de los Servicios.

5.  # Acceso al Software
    

El cliente puede usar El Software alojado en la Plataforma de Nube provista por OSSE o elegir una opción de autohospedaje. La plataforma de nube está alojada y totalmente administrada por OSSE SAC, y el usuario accede de forma remota. Con la opción de Autohospedaje, el usuario aloja El Software en los sistemas informáticos de su elección, que no están bajo el control de OSSE SAC.

Durante la vigencia de estos TYC, OSSE otorga al usuario una licencia no exclusiva e intransferible para usar, ejecutar y modificar El Software Oxe360 Enterprise Edition u OSSE Enterprise Edition en los términos establecidos en la licencia [Odoo Enterprise Edition License v1.0](https://www.odoo.com/documentation/user/12.0/legal/licenses/licenses.html#odoo-community-license).

El usuario acepta tomar todas las medidas necesarias para garantizar la ejecución sin modificaciones de la parte del Software que verifica la validez del uso de El Software y recopila estadísticas para este propósito, incluyendo entre otros, la ejecución de una instancia, el número de usuarios y las aplicaciones instaladas.

OSSE se compromete a no divulgar figuras individuales o nombradas a terceros sin el consentimiento del usuario, y a tratar todos los datos recopilados de conformidad con su Política de Privacidad publicada en [http://www.osse.com.pe/page/privacidad](http://www.osse.com.pe/page/privacidad).

Al vencimiento o terminación de este acuerdo, esta licencia se revoca de inmediato y el usuario acepta desinstalar y dejar de usar el software Oxe360 Enterprise Edition u OSSE Enterprise Edition. Si el usuario incumple los términos de esta sección, acepta pagar a OSSE SAC una tarifa adicional equivalente al 300% del precio de lista aplicable por el número real de usuarios y aplicaciones instaladas.

6.  # Cargos y Tarifas
    

1.  ## Cargos Estándares
    

Los cargos estándares para la suscripción a Los Servicios se basan en el número de usuarios, las aplicaciones instaladas, la versione del software utilizada por el usuario.

Si durante el periodo de vigencia de este acuerdo se incrementa la cantidad de usuarios o aplicaciones, el cliente acepta pagar una tarifa adicional equivalente al precio de lista aplicable al inicio del periodo por dichos usuarios o aplicaciones por el resto del plazo.

Si al momento de la conclusión del acuerdo, el cliente utiliza una versión cubierta que no es la más reciente, los cargos estándares pueden incrementarse en un 50% durante el primer plazo, a discreción de OSSE SAC para cubrir los costos adicionales de mantenimiento.

2.  ## Cargos por renovación
    

En la renovación del acuerdo, si los cargos aplicados en el plazo anterior son más bajos que los cargos de la lista aplicable más actual, estos cargos de renovación podrán aumentar únicamente hasta en un 7%.

3.  ## Impuestos
    

Todas las tarifas y cargos excluyen todos los impuestos, tarifas o cargos gubernamentales, provinciales, locales u otros. El usuario es responsable de pagar todos los impuestos asociados con las compras realizadas en virtud de este acuerdo, excepto cuando OSSE SAC está legalmente obligado a recaudar o pagar los impuestos de los cuales el usuario es responsable.

7.  # No Licencia de Signos Distintivos
    

Todas las marcas, nombres comerciales o signos distintivos de cualquier clase que aparecen en Los Servicios son propiedad de sus respectivos propietarios, sin que pueda entenderse que el uso o acceso la aplicación web y/o Servicios atribuya al Usuario derecho alguno sobre las marcas, nombres comerciales y/o signos distintivos.

8.  # Publicidad
    

Excepto cuando se notifique lo contrario por escrito, cada parte otorga a la otra una licencia mundial, no exclusiva, libre de regalías para reproducir y mostrar el nombre, logotipos y marcas registradas de la otra parte con el único fin de referirse a la otra parte como cliente o proveedor, en sitios web, comunicados de prensa y cualquier otro material de marketing.

9.  # Legislación Aplicable
    

Los TYC se rigen, en todos y cada uno de sus extremos, por las leyes de la República del Perú, con exclusión de toda norma de su sistema de derecho internacional privado o que remita a la aplicación de cualquier ley que no sea peruana, renunciando El Usuario a cualquier otra ley a cuya aplicación podría tener derecho.

10.  # Resolución de Disputas
    

OSSE y El Usuario convienen en resolver todo litigio o controversia derivados o relacionados con los TYC, mediante arbitraje, de conformidad con los Reglamentos Arbitrales del Centro de Arbitraje de la Cámara de Comercio de Lima, a cuyas normas, administración y decisión se someten las partes en forma incondicional, declarando conocerlas y aceptarlas en su integridad.

11.  # Límite de Responsabilidad
    

En la medida máxima permitida por la ley, la responsabilidad total de cada una de las partes, junto con sus afiliadas, que surjan de o estén relacionadas con este Contrato no excederán el 100% del monto total pagado por el Cliente bajo este Acuerdo durante los 12 meses inmediatamente anteriores a la fecha del hecho que da lugar a tal reclamación. Las reclamaciones múltiples no ampliarán esta limitación.

En ningún caso, ninguna de las partes o sus afiliados serán responsables de cualquier daño indirecto, especial, ejemplar, incidental o consecuente de cualquier tipo, incluidos, entre otros, la pérdida de ingresos, ganancias, pérdidas, pérdida de negocios u otra pérdida financiera, costos de suspensión o demora, datos perdidos o corrompidos, que surjan de o estén relacionados con este Acuerdo independientemente de la forma de acción, ya sea por contrato, agravio (incluida negligencia estricta) o cualquier otra teoría legal o equitativa, incluso si una parte o sus asociados han sido informados de la posibilidad de tales daños, o si una de las partes o el remedio de sus delegados no cumple con su propósito esencial.

12.  # Tratamiento de Datos Personales
    

Para utilizar Los Servicios, Los Usuarios que sean personas naturales deben proporcionar previamente a OSSE ciertos datos de carácter personal (en adelante, Los Datos Personales). En cumplimiento de lo dispuesto en la normativa vigente en materia de protección de datos personales, al aceptar estos TYC  usted otorga expresamente autorización a OSSE para hacer uso y trato de los datos personales de su titularidad que proporcione durante la ejecución de El Servicio.

La captación y tratamiento de su información personal tiene como finalidad: (i) la creación de perfiles para el envío de ofertas personalizadas de OSSE y la adecuación de estas a sus características particulares, o de productos o servicios de terceros que pueden ser distintos a los contratados, siendo esta publicidad enviada directamente por OSSE, o inclusive a través de terceros, pudiendo enviar publicidad e información en general, a través de sus diferentes canales; (ii) la mejora continua de los servicios, contenidos y experiencias de OSSE; (iii) realizar encuestas y estudios para conocer los niveles de satisfacción, conocer preferencias y sugerencias de los clientes y probar funciones en fase de desarrollo.

OSSE es titular y responsable de los Bancos de Datos originados por el tratamiento de los datos personales que recopile y/o trate y declara que ha adoptado los niveles de seguridad apropiados para el resguardo de la información, de acuerdo a la normativa vigente. La información brindada por usted será almacenada en el banco de datos de denominación “Clientes”, correspondiente a lo declarado ante la Dirección de Protección de Datos Personales. El tiempo de almacenamiento y tratamiento de los datos personales se realizará durante el tiempo en que usted mantenga una relación contractual con OSSE y, con posterioridad, se mantendrán bloqueados hasta por un total de diez (10) años. Transcurrido dicho tiempo, serán removidos por OSSE. Usted declara que ha sido informado y que podrá ejercer sus derechos de acceso, rectificación, cancelación y oposición vía correo electrónico a [support@crm.osse.com.pe](mailto:support@crm.osse.com.pe) aportando copia de su documento de identidad o documentación equivalente, para acreditar su identidad.

13.  # Privacidad
    

Toda la información que proporcione durante el proceso de registro para la utilización de Los Servicio debe ser correcta, actual y completa. OSSE podrá recopilar y utilizar la información recopilada alineada a la Política de Privacidad de OSSE en [http://www.osse.com.pe/page/privacidad](http://www.osse.com.pe/page/privacidad)

14.  # Seguridad de su Cuenta
    

Usted es responsable de proteger la contraseña que utiliza para acceder a Los Servicios y acepta no revelar su contraseña a ningún tercero. Usted es responsable de cualquier actividad que use su cuenta, independientemente de que haya autorizado o no esa actividad. Debe notificarnos de inmediato cualquier uso no autorizado de su cuenta.

Las contraseñas utilizadas para acceder a Los Servicios son para uso personal. Usted será responsable de la seguridad de su contraseña.

Está prohibido utilizar Los Servicios o instalaciones proporcionados en relación con Los Servicios para comprometer la seguridad o manipular los recursos del sistema y / o las cuentas. Está estrictamente prohibido el uso o la distribución de herramientas diseñadas para comprometer la seguridad (por ejemplo, programas de adivinación de contraseñas, herramientas de craqueo o herramientas de sondeo de red). Si usted se involucra en cualquier violación de la seguridad del sistema, nos reservamos el derecho de divulgar sus datos a los administradores del sistema en otros sitios web para ayudarlos a resolver incidentes de seguridad así como de proceder con las denuncias legales correspondientes.

Si su información de contacto, u otra información relacionada con su cuenta, cambia, debe notificarnos con prontitud y mantener su información actualizada.

Los Servicios no están destinados a ser utilizados por usted si usted es menor de 18 años de edad. Al aceptar estos TYC, está aceptando que tiene más de 18 años.

15.  # Cláusula de rescisión
    

En el caso de que cualquier de las partes no cumpla con cualquiera de sus obligaciones derivadas del presente TYC, y si dicha violación no se ha subsanado dentro de los 60 días calendario posteriores a la notificación por escrito, la parte que no haya incumplido puede terminar este acuerdo inmediatamente.

Además, OSSE SAC puede terminar este acuerdo de inmediato en el caso de que el usuario no pague las tarifas aplicables por Los Servicios dentro de la fecha de vencimiento especificada en los comprobantes de pago correspondientes.

En caso de terminar la provisión de los Servicios, OSSE S.A.C. se obliga únicamente a devolver al Usuario los datos que proporcionó.

16.  # Modificaciones
    

OSSE se reserva los derechos de modificar a su discreción estos TYC sin previo aviso; en consecuencia, la versión actualizada siempre se publicará en la página web de OSSE (www.osse.com.pe); por lo que, le sugerimos revisarla periódicamente. Al continuar accediendo o usar Los Servicios una vez que las revisiones entren en vigencia, usted acepta estar sujeto a los TYC actualizados.

Si tuviera alguna pregunta acerca de los TYC, póngase en contacto con nosotros en support@crm.osse.com.pe.

# Welcome to StackEdit!

Hi! I'm your first Markdown file in **StackEdit**. If you want to learn about StackEdit, you can read me. If you want to play with Markdown, you can edit me. Once you have finished with me, you can create new files by opening the **file explorer** on the left corner of the navigation bar.


# Files

StackEdit stores your files in your browser, which means all your files are automatically saved locally and are accessible **offline!**

## Create files and folders

The file explorer is accessible using the button in left corner of the navigation bar. You can create a new file by clicking the **New file** button in the file explorer. You can also create folders by clicking the **New folder** button.

## Switch to another file

All your files and folders are presented as a tree in the file explorer. You can switch from one to another by clicking a file in the tree.

## Rename a file

You can rename the current file by clicking the file name in the navigation bar or by clicking the **Rename** button in the file explorer.

## Delete a file

You can delete the current file by clicking the **Remove** button in the file explorer. The file will be moved into the **Trash** folder and automatically deleted after 7 days of inactivity.

## Export a file

You can export the current file by clicking **Export to disk** in the menu. You can choose to export the file as plain Markdown, as HTML using a Handlebars template or as a PDF.


# Synchronization

Synchronization is one of the biggest features of StackEdit. It enables you to synchronize any file in your workspace with other files stored in your **Google Drive**, your **Dropbox** and your **GitHub** accounts. This allows you to keep writing on other devices, collaborate with people you share the file with, integrate easily into your workflow... The synchronization mechanism takes place every minute in the background, downloading, merging, and uploading file modifications.

There are two types of synchronization and they can complement each other:

- The workspace synchronization will sync all your files, folders and settings automatically. This will allow you to fetch your workspace on any other device.
	> To start syncing your workspace, just sign in with Google in the menu.

- The file synchronization will keep one file of the workspace synced with one or multiple files in **Google Drive**, **Dropbox** or **GitHub**.
	> Before starting to sync files, you must link an account in the **Synchronize** sub-menu.

## Open a file

You can open a file from **Google Drive**, **Dropbox** or **GitHub** by opening the **Synchronize** sub-menu and clicking **Open from**. Once opened in the workspace, any modification in the file will be automatically synced.

## Save a file

You can save any file of the workspace to **Google Drive**, **Dropbox** or **GitHub** by opening the **Synchronize** sub-menu and clicking **Save on**. Even if a file in the workspace is already synced, you can save it to another location. StackEdit can sync one file with multiple locations and accounts.

## Synchronize a file

Once your file is linked to a synchronized location, StackEdit will periodically synchronize it by downloading/uploading any modification. A merge will be performed if necessary and conflicts will be resolved.

If you just have modified your file and you want to force syncing, click the **Synchronize now** button in the navigation bar.

> **Note:** The **Synchronize now** button is disabled if you have no file to synchronize.

## Manage file synchronization

Since one file can be synced with multiple locations, you can list and manage synchronized locations by clicking **File synchronization** in the **Synchronize** sub-menu. This allows you to list and remove synchronized locations that are linked to your file.


# Publication

Publishing in StackEdit makes it simple for you to publish online your files. Once you're happy with a file, you can publish it to different hosting platforms like **Blogger**, **Dropbox**, **Gist**, **GitHub**, **Google Drive**, **WordPress** and **Zendesk**. With [Handlebars templates](http://handlebarsjs.com/), you have full control over what you export.

> Before starting to publish, you must link an account in the **Publish** sub-menu.

## Publish a File

You can publish your file by opening the **Publish** sub-menu and by clicking **Publish to**. For some locations, you can choose between the following formats:

- Markdown: publish the Markdown text on a website that can interpret it (**GitHub** for instance),
- HTML: publish the file converted to HTML via a Handlebars template (on a blog for example).

## Update a publication

After publishing, StackEdit keeps your file linked to that publication which makes it easy for you to re-publish it. Once you have modified your file and you want to update your publication, click on the **Publish now** button in the navigation bar.

> **Note:** The **Publish now** button is disabled if your file has not been published yet.

## Manage file publication

Since one file can be published to multiple locations, you can list and manage publish locations by clicking **File publication** in the **Publish** sub-menu. This allows you to list and remove publication locations that are linked to your file.


# Markdown extensions

StackEdit extends the standard Markdown syntax by adding extra **Markdown extensions**, providing you with some nice features.

> **ProTip:** You can disable any **Markdown extension** in the **File properties** dialog.


## SmartyPants

SmartyPants converts ASCII punctuation characters into "smart" typographic punctuation HTML entities. For example:

|                |ASCII                          |HTML                         |
|----------------|-------------------------------|-----------------------------|
|Single backticks|`'Isn't this fun?'`            |'Isn't this fun?'            |
|Quotes          |`"Isn't this fun?"`            |"Isn't this fun?"            |
|Dashes          |`-- is en-dash, --- is em-dash`|-- is en-dash, --- is em-dash|


## KaTeX

You can render LaTeX mathematical expressions using [KaTeX](https://khan.github.io/KaTeX/):

The *Gamma function* satisfying $\Gamma(n) = (n-1)!\quad\forall n\in\mathbb N$ is via the Euler integral

$$
\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.
$$

> You can find more information about **LaTeX** mathematical expressions [here](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference).


## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/). For example, this will produce a sequence diagram:

```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```

And this will produce a flow chart:

```mermaid
graph LR
A[Square Rect] -- Link text --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
```
