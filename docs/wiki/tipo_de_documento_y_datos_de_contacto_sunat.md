
| modelo | propiedad/método | definición |
| ------ | ------ | ------ |
|res.company|_update_ruc_when_vat_change|Actualiza campo ruc cuando vat cambia|
|res.partner|province_id|Provincia del Partner|
|res.partner|district_id|Distrito del Partner|
|res.partner|ubigeo|Ubigeo del Partner|
|res.partner|doc_type|Tipo de documento del Partner|
|res.partner|doc_number|Número de documento del Partner|
|res.partner|first_name|Primer nombre del Partner|
|res.partner|middle_name|Segundo nombre del Partner|
|res.partner|surname|Apellido paterno del Partner|
|res.partner|mother_name|Apellido materno del Partner|
|res.partner|company_type|Tipo de compañía del Partner|
|res.partner|property_account_receivable_boe_id|Cuenta por cobrar para letras|
|res.partner|property_account_payable_boe_id|Cuenta por pagar para letras|
|res.partner|country_id|Compañía del Partner|
|res.partner|_get_default_country|Devuelve el país por defecto|
|res.partner|_update_ruc_in_company|Actualiza campo ruc en compañía relacionada al partner|
|res.partner|_check_doc_number_and_doc_type|Valida que el doc_number sea correcto|
|res.partner|_update_vat|Actualiza el VAT a partir del RUC|

* Datos de contacto agregado en una vista de partner individual.

![screenshot-localhost_8120-2019-02-20-18-41-00](uploads/d1c684c49ca974e1affeee1440dfdea7/screenshot-localhost_8120-2019-02-20-18-41-00.png)

*  La función  ***_update_ruc_when_vat_change***, actualiza campo ruc cuando vat cambia, cmo se muestra.

![screenshot-localhost_8120-2019-02-20-17-53-54](uploads/aab0cfad5dc7a422432ad3ac513fe09f/screenshot-localhost_8120-2019-02-20-17-53-54.png)


*  La función  ***_update_vat***, actualiza el vat si el tipo de documento(doc_type), es de tipo DNI o RUC, si el partner registrado es de Peru el vat seria codigo del pais + tipo de documento + numero de documento, caso contrario solo codigo del pais +  numero de documento, esto se realiza para las validaciones de vat de odoo en el modulo base_vat.

![screenshot-localhost_8120-2019-02-20-18-06-04](uploads/72521f98d19ca4c1bbf7449f13bb6656/screenshot-localhost_8120-2019-02-20-18-06-04.png)
