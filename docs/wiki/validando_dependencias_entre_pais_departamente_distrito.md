
Mediante este feature, manejamos los campos de Pais, Departamento, Provincia y Distrito, se deja el desarrollo en este módulo pero la carga de los códigos UBIGEO se hará en l10n_pe_account_invoicing.

| modelo | propiedad/método | definición |
| ------ | ------ | ------ |
|res.partner|_onchange_country_id|Actualiza los departamentos si cambia el país|
|res.partner|_onchange_state_id|Actualiza las provincias si cambia el departamento|
|res.partner|_onchange_province_id|Actualiza los distritos si cambia la provincia|
|res.partner|_onchange_district_id|Actualiza el UBIGEO si cambia el distrito|

![screenshot-jolevq-osse-enterprise-110-11-0-288235.dev.odoo.com-2019-02-20-19-11-08](uploads/8134a2aeb563ae6333f81990b48db072/screenshot-jolevq-osse-enterprise-110-11-0-288235.dev.odoo.com-2019-02-20-19-11-08.png)

![screenshot-jolevq-osse-enterprise-110-11-0-288235.dev.odoo.com-2019-02-20-19-13-47](uploads/42c8d0cae549bbf07c7b6f42fadacef4/screenshot-jolevq-osse-enterprise-110-11-0-288235.dev.odoo.com-2019-02-20-19-13-47.png)

![screenshot-jolevq-osse-enterprise-110-11-0-288235.dev.odoo.com-2019-02-20-19-15-11](uploads/2c8eb741a675b190ff9c804a5f4bc7d2/screenshot-jolevq-osse-enterprise-110-11-0-288235.dev.odoo.com-2019-02-20-19-15-11.png)

