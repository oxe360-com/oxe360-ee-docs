# Módulo: l10n_pe_stock

* Se adiciono el campo *Oxe360 Picking Sequence*, en los campos de la funcionalidad *Tipos de operaciones*.

Para visualizar esto se debe de acceder al módulo **Inventario**.

<img src="img_stock/l10n_pe_stock_2.png" alt="texto alternativo" >

En la pestaña  *Configuración*, dentro del agrupador, *Gestión de Almacenes*, seleccionar la funcionalidad *Tipos de operaciones*.

<img src="img_stock/l10n_pe_stock_3.png" alt="texto alternativo" >

Al acceder a esta funcionalidad damos *clic* en el botón *Crear* y se va visualizar la siguiente interfaz con el campo adicionado.

<img src="img_stock/l10n_pe_stock_1.png" alt="texto alternativo" >

* Se adiciono el campo *Document Serie/Numbe*, en la funcionalidad *Transferencias*.

Para visualizar este campo se debe de acceder al módulo **Inventario**.

En la pestaña *Operaciones*, se seleccionará la funcionalidad *Transferencias*.

<img src="img_stock/l10n_pe_stock_4.png" alt="texto alternativo" >

Al acceder a esta funcionalidad, se selecciona el botón *Crear*, y se visualiza la siguiente interfaz con el campo que se ha adicionado.

<img src="img_stock/l10n_pe_stock_5.png" alt="texto alternativo" >


* Se adiciono un nuevo elemento en la funcionalidad *Secuencia*, denominado  *oxe360_picking_sequence*.

Para visualizar este elemento se debe de acceder al módulo **Ajustes**.

<img src="img_stock/l10n_pe_stock_7.png" alt="texto alternativo" >

En la pestaña *Técnico*, en el agrupador *Secuencias e identificadores*, seleccionar la funcionalidad *Secuencias*.

<img src="img_stock/l10n_pe_stock_8.png" alt="texto alternativo" >

Se va a visualizar un listado de los elementos existentes, para verificar el nuevo elementos, el usuario se puede apoyar  dando *clic* en el paginado, y visualizará el elementos adicionado.

<img src="img_stock/l10n_pe_stock_9.png" alt="texto alternativo" >

Para mayor información del mismo, se debe de dar clic encima de él y se mostrará la información que se le adiciono.

<img src="img_stock/l10n_pe_stock_6.png" alt="texto alternativo" >





