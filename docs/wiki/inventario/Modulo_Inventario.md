Módulo Inventario
--------

El inventario representa la existencia de bienes almacenados destinados a  realizar una operación, sea de compra, alquiler, venta, uso o transformación. Debe aparecer, contablemente, dentro del activo como un activo circulante.

Los inventarios de una compañía están constituidos por sus materias primas, sus productos en proceso, los suministros que utiliza en sus operaciones y los productos terminados.

# Resumen de inventarios

Al acceder al módulo de Inventario de Odoo 13  

<img src="img_inventario/invent-1.png" alt="texto alternativo" >

Se muestra una interfaz con el Resumen de inventarios registrados hasta el momento:

<img src="img_inventario/invent-2.png" alt="texto alternativo" >

Como se muestra están agrupados  en Recepciones y Órdenes de entrega. Estos a su vez presentan el estado de sus elementos.

<img src="img_inventario/invent-3.png" alt="texto alternativo" >

Al seleccionar el botón Procesar, en Recepciones o en Ordenes de entrega,  va a mostrar el listado de los elementos que se encuentran enumerados de forma detallada. 

<img src="img_inventario/invent-4.png" alt="texto alternativo" >

Como se muestra el sistema va a filtrar el contenido que pertenece a la vista Recepciones, mostrando solo los que están en estado Preparado que corresponde al estado Retrasado definido en la vista Resumen de inventario.

Lo mismo va a ocurrir si accedemos al botón Procesar de Ordenes de entrega, se va a mostrar el listado de los elementos que están en los estados identificados en la vista Resumen de inventario

En la Vista lista de la funcionalidades que permitan introducir o listar contenido , se van a mostrar los botones de Crear, Importar y Exportar <img src="img_inventario/flecha exportar.jpg" alt="texto alternativo" > 

Al seleccionar el botón Crear , se mostrara una interfaz que mostrara los campos necesarios para crear la nueva funcionalidad. 

Al seleccionar el botón Importar, permitirá seleccionar desde el ordenador el fichero o documento que se desee cargar en el sistema, que cumpla con las características que presente la funcionalidad a la que se desee adicionar la información.

Al seleccionar la opción Exportar <img src="img_inventario/flecha exportar.jpg" alt="texto alternativo" > se mostrara en un fichero.xlsx, o Excel todo el contenido que se encuentra listado hasta el momento.  

Al acceder a las Recepciones, dando clic encima de la palabra Recepciones en el tablero, se va a mostrar una interfaz con todas las Recepciones realizadas hasta el momento, es decir estos serían los recursos que entran a la empresa, con los diferentes estados por los que pasan los mismos.

<img src="img_inventario/invent-5.png" alt="texto alternativo" > 

Al seleccionar la opción Crear se va a mostrar una  interfaz que va a tener la información necesaria para crear una nueva Operación que puede ser de Tipo Recepción o de Orden de entrega.

<img src="img_inventario/invent-6.png" alt="texto alternativo" >

Se muestran los botones *Guardar* , *Descartar*, *Validad* y *Cancelar*

Se muestran los distintos estados por los que puede pasar una Recepción Borrador, En espera, Preparado y Hecho.

Los campos  a introducir son:

*Recibir de*: Es un campo de selección donde se puede seleccionar el nombre de uno de los clientes cargados en el sistema o se puede crear uno nuevo.

*Tipo de Operación*: Se va a definir si va a ser Recepción u Órdenes de entrega. 

En caso de que se seleccione la operación Ordenes de entrega, el campo Recibir de, va a cambiar y el que se va a mostrar va a ser Dirección de entrega. Donde se va a registrar el nombre del cliente, tomando del mismo la dirección de la Orden de entrega. Además se va a mostrar un nuevo botón *Desechar*.

*Fecha prevista*: Se debe de introducir la fecha prevista para la operación, el sistema por defecto va a tomar la fecha en que se está registrando la operación, es decir, la actual. 

*Documento origen*: Se debe de introducir el número del documento del que se originó la operación.

Se muestran además las siguientes pestañas: 

**Operaciones**: al seleccionar la opción Agregar línea me va a permitir introducir los datos de Producto y  Hecho.

*Producto*: Campo selección donde permitirá seleccionar el nombre del producto que se va a registrar. Puede ser de los que se muestran en el sistema o crear uno nuevo.

*Hecho*: Se va a introducir el monto definido para el producto que se ha introducido. 

**Información adicional**: Se va a mostrar los campos correspondientes a las características de la operación que se va a registrar.

*Política de entrega*: Se va a seleccionar la opción ‘Lo antes posible’ o  ‘Cuando todos los productos estén listos’.

*Prioridad*: Se va a dar un valor a partir de la selección de estrellas. 

*Responsable*: Se selecciona el nombre del cliente responsable de esta operación. El sistema va a tomar por defecto el nombre del usuario que está registrado en el sistema y que está registrando la operación. 

*Grupo de abastecimiento: Este campo se muestra desactivado.

*Compañía*: Toma el valor de la compañía registrada por el usuario del sistema. 

**Nota**: Se va a introducir alguna nota que el usuario estime conveniente que deba de tener en cuenta para registrar la operación. 

<img src="img_inventario/invent-7.png" alt="texto alternativo" >

<img src="img_inventario/invent-8.png" alt="texto alternativo" >

Al introducir todos los campos la operación se va a mostrar en estado Borrador, al seleccionar el botón Guardar va a pasar al estado Preparado y se van a mostrar los botones Editar, Crear, Validar , Imprimir, Desbloquear y Cancelar.

<img src="img_inventario/invent-9.png" alt="texto alternativo" >

En dependencia del contenido que se  introduzca en la pestaña Operaciones se van a mostrar el botón Marcar por realizar. Este se muestra cuando al guardar la operación se muestra en estado Borrador, al dar clic en este botón la operación pasa a estado Preparado. 

<img src="img_inventario/invent-8.1.png" alt="texto alternativo" >

Al seleccionar el botón Validar el estado de la operación pasa a *Hecho*, mostrándose el botón *Devolver*.
Se muestra el campo *Fecha efectiva*, donde tomara la fecha en que se validó la operación. 

Al seleccionar el botón *Devolver* se va a mostrar una interfaz para revertir la trasferencia realizada.

<img src="img_inventario/invent-15.png" alt="texto alternativo" > 

Al seleccionar el botón *Devolver* el estado de la operación cambia y el valor del campo *Hecho* de la operación que se registró pasa a 0. 

<img src="img_inventario/invent-10.png" alt="texto alternativo" >

Una operación no se puede validar si no presenta ninguna operación registrada, es decir si el usuario no ha introducido el producto con el *Hecho* o *Monto* del mismo, se lanzara un mensaje de error permitiendo que corrija la información. 

Al seleccionar el botón *Imprimir* se va a mostrar una vista de cómo quedaría impreso el documento que se desea imprimir. 

<img src="img_inventario/invent-11.png" alt="texto alternativo" >

Al seleccionar la opción *Guardar* se va a mostrar un documento en formato pdf el cual puede ser imprimido.

Al seleccionar el botón *Desechar* se va a mostrar una interfaz en el que se debe de introducir los campos *Producto* y *Cantidad*.

Ahí se va a seleccionar de los Productos que se introdujeron cual se va a desechar.

En caso de que la cantidad que se desea desechar esté disponible y se pueda ejecutar la acción se va a mostrar la pestaña *Desechos*, y al dar clic encima de ella se van a mostrar las **Órdenes de desechos** que se han realizado.

<img src="img_inventario/invent-13.png" alt="texto alternativo" >

<img src="img_inventario/invent-14.png" alt="texto alternativo" >

Al seleccionar el botón *Desbloquear* se va a permitir editar la demanda inicial o las cantidades realizadas.

Una vez desbloqueado se va a mostrar un botón *Bloquear*, y al seleccionar nuevamente el botón *Bloquear* van a salir los botones que me salían anteriormente. 

# Operaciones
Al seleccionar la pestaña Operaciones se van a mostrar las distintas operaciones que se van a realizar en el sistemas, las cuales son *Transferencia*, *Ajustes de inventario*, *Desechar* y *Ejecutar planificador*.

Transferencia 
------

Al acceder a la funcionalidad Transferencia se muestra una vista lista que va a mostrar todas las transferencias realizadas hasta el momento. 

<img src="img_inventario/invent-17.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* se muestra una interfaz con los datos necesarios para crear una nueva Transferencia.

Los datos a introducir son:

*Contacto*: Se van a mostrar los nombres de los usuarios registrados en el sistema. 

*Tipo de operación*: Se va a introducir el tipo de operación que se desee realizar y que se han creado en la funcionalidad **Tipos de operaciones** , puede ser *Recepción* ,*Órdenes de entrega* o *PoS Orders*, según las que estén creadas. 

En caso de que se seleccione la operación *Ordenes de entrega* o *PoS Orders*, el campo *Recibir de*, va a cambiar y el que se va a mostrar va a ser *Dirección de entrega*. Donde se va a registrar el nombre del cliente.

*Fecha prevista*: Se va a cargar la fecha actual y la hora exacta en que se está registrando la Transferencia.

*Documento origen*: El número del documento origen de la Transferencia que se está creando. 

Pestañas **Operaciones**:

Se muestran los campos *Producto* (se va a introducir el nombre del producto), *Demanda* (monto de la demanda), *Reservado* y *Hecho*.

Los campos *Reservado* y *Hecho* se muestran desactivados al crear una nueva Transferencia.

Al seleccionar el botón *Guardar*, se registran la Transferencia en estado *Borrador* y se muestran los botones *Marcado por realizar*, *Desechar* y *Cancelar*. 

<img src="img_inventario/invent-18.png" alt="texto alternativo" >

Al seleccionar el botón *Marcado por realizar*, la Transferencia pasa a estado *En espera*, y además de los botones anteriores se muestran los botones *Comprobar disponibilidad*, *Validar* y *Desbloquear*.

Se incluye también una nueva pestaña denominada **Operaciones detalladas**, la cual va a mostrar los campos *Producto*, *Lote/No de serie*, *Reservado* y *Hecho*. 

<img src="img_inventario/invent-20.png" alt="texto alternativo" > 

Al seleccionar el botón *Comprobar disponibilidad*, la Transferencia para al estado *Preparado* y se muestran los botones *Validar*, *Imprimir*, *Anular Reserva*, *Desechar*, *Desbloquear* y *Cancelar*.

En la pestaña **Operación detallada** se muestran los datos de las operaciones que se describieron inicialmente, con el valor que se le introdujo. 

<img src="img_inventario/invent-21.png" alt="texto alternativo" >

En la pestaña **Operaciones** se va a mostrar los valores de *Demanda* y *Reservado* a partir del dato introducido inicialmente en *Hecho*. 

<img src="img_inventario/invent-22.png" alt="texto alternativo" >

Al seleccionar el botón *Anular reserva* la Transferencia pasa al estado *En espera* y se muestran los botones que se describieron que salían en ese estado.

Los datos que se habían generado en las pestañas desaparecen y solo se muestran lo que habían al estar en el estado de *En espera*. 

Al volver al estado *Preparado* se selecciona el botón *Validar*.

En caso que no se haya cargado las cantidades hechas, definidas en el campo *Hecho* se va a mostrar un mensaje que al seleccionar el botón *Aplicar* permitirá procesar todas las cantidades necesarias para validar la Transferencia. 

<img src="img_inventario/invent-23.png" alt="texto alternativo" > 

<img src="img_inventario/invent-24.png" alt="texto alternativo" >

Ajustes de inventario:
----

Al acceder a la funcionalidad Ajuste de Inventario se muestra una vista lista que va a mostrar todos los ajustes realizados hasta el momento. 

<img src="img_inventario/invent-26.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* se va a mostrar una interfaz con los campos necesarios para realizar un Ajuste  de Inventario.

<img src="img_inventario/invent-27.png" alt="texto alternativo" >

Se van a mostrar los botones *Guardar*, *Descartar* e *Iniciar inventario*.

Este proceso va a pasar por los estados *Borrador*, *En proceso* y *Validado*.

Campos:

*Referencia de inventario*: Se va a introducir el nombre del Inventario.

*Productos*: Es un campo de selección, donde se va a seleccionar un producto de los definidos en el sistema. 

*Fecha contable*: Se va a introducir la fecha.

*Compañía*: El sistema por defecto va registrar la compañía definida por el usuario registrado en el sistema. 

*Cantidades contadas*: Estas pueden ser Predeterminado a stock disponible (Stock disponible= Stock físico + Pendiente de recibir - Pendientes de servir) o Predeterminado a cero.

Se selecciona el botón *Guardar* se registran los datos en el sistema con el estado *Borrador*.

Al seleccionar el botón *Iniciar inventario* se va a mostrar una interfaz con los datos que hay que inventariar del producto que se  había introducido, y el Inventario pasa a estado *En proceso*.
Se va a mostrar el botón Validar inventario. 

<img src="img_inventario/invent-28.png" alt="texto alternativo" >

Al seleccionar el botón *Validar inventario*, el inventario pasa a estado *Validado* y se muestra una pestaña con nombre *Movimientos productos*, que va a mostrar los movimientos de los productos realizados. 

<img src="img_inventario/invent-29.png" alt="texto alternativo" > 

En caso de que se seleccione el botón *Inicial inventario*, y no se pueda *Validar* la operación , el inventario se va a mostrar en estado *En proceso* y al acceder a él en la vista lista, se van a mostrar los botones *Continuar inventario* , *Validar inventario* y *Cancelar inventario*.

<img src="img_inventario/invent-30.png" alt="texto alternativo" > 

Al seleccionar el botón *Continuar inventario* se va a mostrar la interfaz a la que antes nos referíamos para *Validar el inventario*. Esta operación también se puede realizar mediante el botón *Validar inventario* sin que se muestre el listado de los productos que se van a inventariar. 

Al seleccionar la opción *Cancelar inventario* se va a mostrar un mensaje indicándole al usuario que se van a perder la información relacionada con las líneas de su inventario. En caso de estar seguro se selecciona el botón *Aceptar*. Este al ser cancelado pasa al estado *Borrador*.

<img src="img_inventario/invent-31.png" alt="texto alternativo" >

Desechar
---

Al acceder a la funcionalidad Desechar se muestra una vista lista que va a mostrar todas las Órdenes de desechos realizadas hasta el momento. 

<img src="img_inventario/invent-32.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* se muestra una interfaz con los campos necesarios para crear una nueva Orden de desecho.

<img src="img_inventario/invent-33.png" alt="texto alternativo" > 

El sistema muestra la Orden de desecho con el nombre Nuevo y los campos:

*Producto*: Se va a seleccionar de la lista de productos registrados en el sistema.

*Cantidad*: Este valor va a ser definido por el usuario.

*Documento origen*: Se va a introducir la numeración que se le va a signar como origen.

*Compañía*: El sistema va a mostrar el nombre de la compañía del usuario que está registrado en el sistema. 
Se muestra el botón *Validar*, y los estados *Borrador* y *Hecho*.

Al seleccionar el botón *Guardar* se registra la Orden de desecho en el sistema, en estado *Borrador*. 

<img src="img_inventario/invent-34.png" alt="texto alternativo" >  

Al seleccionar el botón *Validar*, la Orden de desecho pasa a estado *Hecho* y se muestra el campo *Fecha*, con la fecha y la hora en que se realizó la operación, es decir la actual.

Además se muestra la pestaña  **Trazabilidad**. Al dar clic encima de ella se muestra una vista con los *Movimientos del producto* realizados hasta el momento. 

<img src="img_inventario/invent-35.png" alt="texto alternativo" > 

Los datos introducidos se muestran en la vista lista con los estados. 

Ejecutar planificador:
---

Al seleccionar la funcionalidad Ejecutar Planificador, se muestra una ventana con el siguiente mensaje: 

<img src="img_inventario/invent-36.png" alt="texto alternativo" > 

Se muestran los botones *Ejecutar Planificador* para llevar a cabo la operación y *Cancelar*. 


# Datos principales

Productos
--- 
Al dar clic a la funcionalidad Productos, se va a mostrar todos los productos registrados en el sistema hasta el momento.

<img src="img_inventario/invent-37.png" alt="texto alternativo" > 

Al seleccionar el botón *Crear* se muestra una interfaz con los campos necesarios para registrar un nuevo Producto.

<img src="img_inventario/invent-38.png" alt="texto alternativo" >  

*Nombre del producto*: Se va a introducir el nombre con el que se va a identificar al producto.

*Puede ser vendido* /*Puede ser comprado*: En el sistema se muestran marcados por defecto ambas opciones, pero el usuario puede seleccionar la que desee que se le aplique al producto. 

*Imagen*: Se muestra la opción para cargar alguna imagen con la que se podrá identificar al producto que se desea crear.

Pestañas:

**Información General**

*Tipo de producto*: Se van a mostrar una lista desplegable con los Tipos de producto *Almacenable*, *Consumible* y *Producto*.

Al seleccionar el Tipo de producto *Consumible*, se va a mostrar la opción *Trazabilidad* en la parte superior. Al seleccionar esta opción se van a mostrar los Movimientos productos.

<img src="img_inventario/invent-39.png" alt="texto alternativo" > 

Al seleccionar el Tipo de producto *Almacenable* se van a mostrar las opciones *Trazabilidad*, *A mano*, *Previsto* y *Regla de abastecimiento*. 

<img src="img_inventario/invent-40.png" alt="texto alternativo" > 

Al seleccionar la opción *A mano*, me mostrara una vista donde se va a poder Actualizar la cantidad existente en el sistema, así como el *Valor* y la *Compañía* a la que pertenece. 

<img src="img_inventario/invent-41.png" alt="texto alternativo" >  

Al seleccionar la *Opción prevista*, mostrará una gráfica mostrando la variación por fechas y por cantidad.

<img src="img_inventario/invent-42.png" alt="texto alternativo" > 

Al  seleccionar la opción Trazabilidad.

<img src="img_inventario/invent-43.png" alt="texto alternativo" > 

Al seleccionar la opción *Reglas de abastecimiento*, va a permitir crear las reglas para el abastecimiento de ese producto definiendo los campos necesarios.

<img src="img_inventario/invent-44.png" alt="texto alternativo" > 

*Referencia interna*: Se va a introducir el código único del producto.

*Código de barra*: Se debe de introducir el código de barra con el que se identificara el producto. 

*Precio de venta*: El precio con el que se va a comercializar el producto.

*Impuesto*: El sistema muestra un impuesto definido en el sistema, pero se puede seleccionar uno nuevo.  

*Coste*: Cantidad que se registra como el gasto para la creación del producto.

*Compañía*: Es un campo selección, donde se va a mostrar la compañía del producto. Se recomienda que se introduzca la misma compañía del usuario que está registrado en el sistema, para poder realizar las operaciones que corresponden a los datos que se está introduciendo. 

**Variantes**

*Atributo*: se van a mostrar los atributos que corresponden al Producto creado.

*Valores*: Estos datos se van a cargar a partir del atributo que se describirá del producto que se está creando. 

En caso de que el sistema introduzca valores en la pestaña **Variantes**, se va a mostrar una opción en la parte superior que se llama *Configurar variantes*. Al seleccionar esta opción se va a mostrar una interfaz con las Variantes que se introdujeron en el sistema y se va a permitir generar un informe de tipo xlsx. 

**Compras**
*Impuesto del proveedor*: Se va a mostrar el impuesto generado por la factura del proveedor, el sistema por defecto carga el 18%.

<img src="img_inventario/invent-45.png" alt="texto alternativo" >  

**Inventario**

Se van a definir los datos correspondientes a las *Operaciones*, la *Trazabilidad del producto*, las *Ubicaciones de contrapartida* y la *Logística*.

Además se van a introducir la *Descripción para pedidos de entrega*, la cual se va a adicionar al *Pedido de entrega*, y la *Descripción para Recepciones*, la que se va a adicionar a los Pedidos de recibo. 

<img src="img_inventario/invent-47.1.png" alt="texto alternativo" >  

En caso de que el *Tipo de servicio* sea *Almacenable*, se mostraran también las opciones *Configurar la cantidad* y *Reabastecer*. Al *Configurar la cantidad* permite *Actualizar la cantidad existente* y al seleccionar *Reabastecer*, se reabastecerá el producto, dependiendo de la Regla de abastecimiento que se haya definido para el mismo.  

<img src="img_inventario/invent-46.png" alt="texto alternativo" > 

<img src="img_inventario/invent-47.png" alt="texto alternativo" >  

Variantes de productos
----

Al acceder a la funcionalidad Variantes de productos se van a listar los productos registrados, con sus características y los atributos de variantes  que tienen asociados. 

<img src="img_inventario/invent-48.png" alt="texto alternativo" > 

Al seleccionar el botón *Crear* se va a mostrar una interfaz con los datos similares al Crear producto. 

<img src="img_inventario/invent-49.png" alt="texto alternativo" > 

Estos datos a introducir varían en dependencia al *Tipo de producto* que se seleccione.

Al seleccionar el Tipo de producto *Servicio*, se van a mostrar las pestañas **Información general** y **Compra**.

Al seleccionar el Tipo de producto  *Consumible*, se van a activar las pestañas  **Información general**, **Compra** e **Inventario**.

Al seleccionar el Tipo de producto  *Almacenable* se van a mostrar todos los datos que se tienen definidos en la funcionalidad Producto.

Reglas de abastecimiento
---

Al acceder a la funcionalidad Reglas de abastecimiento se van a mostrar un listado con los productos, sus datos  y las Reglas de abastecimiento de estos definidas hasta el momento.

<img src="img_inventario/invent-50.png" alt="texto alternativo" >  

Al seleccionar el botón *Crear* se van a mostrar los campos correspondientes para crear una nueva Regla de abastecimiento. 

<img src="img_inventario/invent-51.png" alt="texto alternativo" > 

El sistema va a generar un *Número de identificación* para la Regla de abastecimiento cada vez que se cree una nueva.

Se van a definir los campos:

*Producto*: Se va a seleccionar el producto al que se le va a realizar la regla.

*Cantidad mínima* y *Cantidad máxima*: se va a definir el intervalo de la cantidad que se puede adquirir del producto.

*Múltiplo de la cantidad*: Se va a introducir el múltiplo de la cantidad que se ha definido.

*Grupo de abastecimiento*: Este valor está por definir.

*Plazo de entrega*: Días en los que debe de ser entregado el producto.

*Compañía*: El sistema carga por defecto la compañía a la que pertenece el usuario que está registrado en el sistema.

Al seleccionar el botón *Guardar* se van a registrar los datos en el sistema.

Lotes/ Números de serie
--

Al seleccionar la funcionalidad Lotes/Número de serie se van a listar los Lotes/Número de series que se han registrado hasta el momento.

<img src="img_inventario/invent-52.png" alt="texto alternativo" >  

Al seleccionar el botón *Crear* se va a mostrar una interfaz con los datos necesarios para Crear un nuevo elemento.

<img src="img_inventario/invent-53.png" alt="texto alternativo" >  

Al crear un nuevo Lote/Número de serie, el sistema genera por defecto un *Número para identificar* el elemento.

Además se van a introducir los campos *Productos*, *Cantidad* (este valor no es editable), *Referencia interna* y *Compañía*.

Se van a mostrar las opciones de *Trazabilidad* y *Ubicación*.

<img src="img_inventario/invent-54.png" alt="texto alternativo" > 

Al seleccionar la opción *Ubicación*, se va a mostrar una interfaz con el botón *Inventario a fecha*. 

<img src="img_inventario/invent-55.png" alt="texto alternativo" > 

Al dar clic se va a mostrar una interfaz que permitirá al usuario seleccionar una fecha en la que se realizara el inventario. Por defecto muestra la fecha actual pero el usuario introduce la fecha deseada. 

<img src="img_inventario/invent-56.png" alt="texto alternativo" >  

Luego de introducida la fecha  y seleccionar el botón *Confirmar*, el sistema va a mostrar el listado de los productos a los que se les va a realizar el inventario. 

<img src="img_inventario/invent-57.png" alt="texto alternativo" >  

# Informes 

En está pestaña se van a mostrar los informes que se generan a partir del módulo Inventario. Algunos de los reportes se muestran agrupados a partir de filtros , para ver la información completa se elimina el filtro y se va a mostrar toda la información que se está listando en los distintos informes.

Análisis de Almacén
---
Se va a generar un análisis completo del Almacén empleando los parámetros definidos en el sistema y que el usuario puede seleccionar según estime conveniente. 

<img src="img_inventario/invent-58.png" alt="texto alternativo" > 

<img src="img_inventario/invent-59.png" alt="texto alternativo" > 

<img src="img_inventario/invent-60.png" alt="texto alternativo" > 


Informe de Inventario
----

Al seleccionar la funcionalidad Informe de Inventario se mostrará una lista con Productos.   

<img src="img_inventario/invent-61.png" alt="texto alternativo" > 

Al seleccionar la opción *Crear* va a permitir introducir un nuevo elemento en la última línea de la vista del listado de productos. 

<img src="img_inventario/invent-62.png" alt="texto alternativo" >  

Inventario previsto
----

Al seleccionar la funcionalidad Inventario previsto, se va a mostrar un reporte de todos los inventarios previstos, marcando la fecha actual. 

<img src="img_inventario/invent-63.png" alt="texto alternativo" >  

En la parte superior se van a mostrar unas flechas que permitirán mostrar el contenido siguiente o anterior, en dependencia de las opciones Semana y Mes. 


Valoración del inventario
--

Al seleccionar la funcionalidad Valoración del inventario se va a mostrar una lista de los productos agrupados por la fecha que se ha introducido, automáticamente el sistema genera los productos de la fecha seleccionada por el usuario.

<img src="img_inventario/invent-64.png" alt="texto alternativo" > 

Al seleccionar el botón *Inventario a fecha*, se va a permitir introducir una fecha nueva y realizar el inventario por la fecha nueva según estime el usuario. 


Movimiento de stock 
----

Al seleccionar la funcionalidad Movimiento de stock se va a mostrar una lista de los movimientos generados hasta el momento. El cual se puede exportar en formato xlsx.

<img src="img_inventario/invent-65.png" alt="texto alternativo" > 

Movimiento de productos 
----

Al seleccionar la funcionalidad Movimiento de productos se va a mostrar una lista de los movimientos generados hasta el momento. El cual se puede exportar en formato xlsx.

<img src="img_inventario/invent-66.png" alt="texto alternativo" > 

Los datos son similares al del informe de Movimiento de stock.


# Configuración

Ajustes
---

Al seleccionar la opción Ajustes , se van a mostrar las opciones necesarias para configurar los datos que se van a generar en las funcionalidades del módulo Inventario. 

En la parte lateral izquierda se muestran otros módulos que se encuentran instalados en el sistema, y que por medio de esta funcionalidad se podrán configurar en caso que sea interés del usuario. 

Ajuste de Inventario
---

Se van a mostrar opciones de selección de como funcionaria el sistema a partir de los elementos: 
**Operaciones**

<img src="img_inventario/invent-67.png" alt="texto alternativo" > 

**Envío**

<img src="img_inventario/invent-68.png" alt="texto alternativo" > 

**Productos**

<img src="img_inventario/invent-69.png" alt="texto alternativo" >  

**Trazabilidad**

<img src="img_inventario/invent-70.png" alt="texto alternativo" >  

**Valoración**

<img src="img_inventario/invent-71.png" alt="texto alternativo" >   

**Almacén**

<img src="img_inventario/invent-72.png" alt="texto alternativo" >  

Luego de seleccionar las opciones para el funcionamiento del módulo, se va a seleccionar el botón Guardar y estos cambios quedaran registrados en el sistema. 

Gestión de almacenes
----

Almacenes
----

Al seleccionar la funcionalidad Almacenes, se va a mostrar una vista lista donde se van a mostrar los Almacenes que están definidos en el sistema. Permite generar un documento en formato xlsx con la información registrada en el sistema de los almacenes. 

<img src="img_inventario/invent-73.png" alt="texto alternativo" >   

Tipos de operaciones
----

Al seleccionar la funcionalidad Tipos de operaciones se va a mostrar una lista de los tipos de operaciones registradas en el sistema. 

<img src="img_inventario/invent-74.png" alt="texto alternativo" >  

Al seleccionar el botón crear se va a mostrar una interfaz con los campos necesarios para crear un nuevo Tipo de operación. 

<img src="img_inventario/invent-75.png" alt="texto alternativo" >  

*Tipo de operación*: Nombre con el que se va a identificar el Tipo de operación.

*Secuencia de referencia*: Se va a seleccionar del listado de secuencias de referencias registradas en el sistema. 

*Oxe360 picking Sequence*: Se va a seleccionar del listado de elementos registrados en el sistema.

*Código*: Se va a introducir el código del Tipo de operación.

*Tipo de operación*: En este campo se debe de seleccionar entre las opciones *Recepción*, *Entrega* y *Transferencia interna*.

Según el tipo de operación que se seleccione se adicionarán otros campos que se deben de introducir.

*Recepción*

<img src="img_inventario/invent-76.png" alt="texto alternativo" >  

<img src="img_inventario/invent-77.png" alt="texto alternativo" >  

*Entrega*
<img src="img_inventario/invent-78.png" alt="texto alternativo" >  

*Transferencia interna*

<img src="img_inventario/invent-79.png" alt="texto alternativo" >   

*Compañía*: El sistema muestra por defecto la compañía del usuario que está registrado en el sistema.

*Mostrar operaciones detalladas*: Se debe de definir si va a mostrar las operaciones detalladas. 

Al guardar nuevo Tipo de operación se va a mostrar en la vista lista de la funcionalidad y se mostrara como una nueva operación al acceder a la vista de la funcionalidad Resumen de inventario.

<img src="img_inventario/invent-80.png" alt="texto alternativo" >  

<img src="img_inventario/invent-81.png" alt="texto alternativo" >  

Productos
--

Categoría de productos
--

Al acceder a la funcionalidad Categoría de productos se van a listar todas las Categorías de producto registradas en el sistema hasta el momento.

<img src="img_inventario/invent-82.png" alt="texto alternativo" >  

Al seleccionar el botón *Crear* se van a mostrar los campos necesarios para crear una nueva categoría.

<img src="img_inventario/invent-83.png" alt="texto alternativo" >  

*Nombre de categoría*: Introducir el nombre con el que se identificara la categoría.

*Categoría padre*: Se va a mostrar el listado de las categorías creadas y se va a seleccionar  en caso que tenga categoría padre de quien es hijo.

*Forzar estrategia de retirada*: Se debe de seleccionar en caso  que se defina entre las opciones First In First Out (FIFO) y Last In First Out (LIFO).

*Método de coste*: Se selecciona entre las opciones Primera entrada primera salida, Coste promedio y Precio estándar.

<img src="img_inventario/invent-84.png" alt="texto alternativo" >  

En la parte superior se muestra una pestaña con nombre **Producto**. Al acceder a ella permite mostrar y crear los Productos que se agruparán en esta categoría.

<img src="img_inventario/invent-85.png" alt="texto alternativo" >  

Al seleccionar el botón Guardar se va a registrar la nueva Categoría del producto.

Atributos
---

Al acceder a esta funcionalidad se van a mostrar todos los atributos registrados en el sistema.

<img src="img_inventario/invent-86.png" alt="texto alternativo" >  

Al seleccionar la opción crear se van a mostrar los campos necesarios para crear un nuevo elemento.

<img src="img_inventario/invent-87.png" alt="texto alternativo" >  

En la pestaña **Valores de atributos** se van a introducir los valores que se van a mostrar a partir del Atributo que se cree.

<img src="img_inventario/invent-88.png" alt="texto alternativo" >  

Al seleccionar el botón *Guardar* se va a registrar el nuevo elemento, mostrándose en la vista lista.

Nomenclatura del código de barra
----

Al acceder a esta funcionalidad se van a mostrar las nomenclaturas de los códigos de barra registrados en el sistema.

<img src="img_inventario/invent-89.png" alt="texto alternativo" >   

Al seleccionar el botón Crear va a mostrar los campos necesarios para crear una nueva nomenclatura.

<img src="img_inventario/invent-90.png" alt="texto alternativo" >  

En la tabla de reglas , al seleccionar la opción *Adicionar línea*, se va a mostrar una vista con los campos definidos para crear una Regla los que son: *Nombre*, *Codificación* , *Patrón del código de barra* , *Tipo* y *Secuencia*.

En el caso de la *Codificación* están definidos.

EA13 y UPC que se emplean para los productos que se venden por unidades y al por menor. UPC con 12 dígitos y EA13 con 13.

EAN8 para productos pequeños con 8 dígitos. 

<img src="img_inventario/invent-91.png" alt="texto alternativo" >  

<img src="img_inventario/invent-92.png" alt="texto alternativo" >  




