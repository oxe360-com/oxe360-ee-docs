
# Módulo: l10n_pe_pos

Luego de instalada la app **l10n_pe_pos**, se va  a visualizar el módulo **Punto de venta** en el que se va a mostrar las opciones que pertenecen a la app instalada. 

<img src="img_pos/pos_1.png" alt="texto alternativo" >

Al acceder a este módulo se van a visualizar las pestañas señaladas.

<img src="img_pos/pos_2.png" alt="texto alternativo" >

En la pestaña *Tableros*, se van a visualizar los Puntos de Ventas existentes hasta el momento. 

<img src="img_pos/pos_3.png" alt="texto alternativo" >

Para crear un nuevo *Punto de venta*, se debe de acceder en la pestaña *Configuración*, al menú *Punto de venta*. 

<img src="img_pos/pos_4.png" alt="texto alternativo" >

Se va a mostrar la siguiente vista.

<img src="img_pos/pos_5.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* se va a mostrar la siguiente vista donde se muestran las opciones necesarias para configurar el *Punto de Venta*. 

<img src="img_pos/pos_6.png" alt="texto alternativo" >

*Punto de venta*: Se debe de especificar el nombre con el que se va a identificar el Punto de venta del cliente 

A continuación se va a mostrar la *Compañía* que automáticamente va a tomar el valor de la Compañía del usuario que está registrado en el sistema.

*InterPoS o Interfaz del pedido*: Se va a configurar como se va a mostrar la interfaz del Pedido de Venta. 
En esta opción se recomienda que además de cualquier otra opción que determine el usuario, se marque la opción *Barras de Desplazamiento Largas* para mejorar el acceso a los elementos que se muestren en tablas, por ejemplo el listado de los clientes. 

*Dispositivos conectados*: Se van a especificar los dispositivos que va a tener el cliente conectado para llevar a cabo sus ventas.

<img src="img_pos/pos_7.png" alt="texto alternativo" >

Al seleccionar la opción *Conectar dispositivos*, se va a visualizar el campo para especificar la *Dirección IP* de la impresora. 

En caso de tener la opción *Escáner de código de barra* sale por defecto un tipo, en caso de existir otro, el usuario tiene la opción de crearlo, o de seleccionar otro existente al seleccionar la flecha que se encuentra al lado del elemento seleccionado por defecto. 


<img src="img_pos/pos_8.png" alt="texto alternativo" >

*Impuestos*: Se marca en caso que el cliente  se vaya a considerar alguna posición fiscal.

*Precio*: 

- Se va a seleccionar si los precios van a ir con o sin impuesto. 

- Si se va a realizar algún descuento global a lo que se está vendiendo.

- En el caso del  campo Programa de fidelización es la opción que se puede aplicar para darle alguna recompensa  a los clientes,  según lo definido en el Programa de lealtad (Loyalty Programs ) que se verá posteriormente.

<img src="img_pos/pos_9.png" alt="texto alternativo" >

*Pagos*:
<img src="img_pos/pos_10.png" alt="texto alternativo" >

*Método de pago*: Se van a definir los métodos de pagos que se van a visualizar en el *Punto de Venta*, los cueles se van a tomar del menú Método de pago, que se mostrara posteriormente. 

*Control de efectivo*: Para tener un control del inicio y cierre de las cajas. 

En el campo *Apertura por defecto*, va a permitir al usuario especificar el monto con el que se va a empezar la venta o crearlo en caso de no existir. Para esto al seleccionar la opción *Crear* se va a visualizar la siguiente interfaz. 

<img src="img_pos/pos_11.png" alt="texto alternativo" >

*Facturas y recibos*: Se va a configurar como se van a visualizar las facturas y recibos que se generen mediante el *Punto de Venta* de las compras realizadas.

En el campo *Encabezado y Pie de página* se van a poner exactamente lo que se debe de mostrar en el *Recibo de la compra* en los campos *Encabezado* y *Pie* que se muestran en esta opción. 

*Impresión automática del recibo*: Al marcar esta opción permitirá al usuario que una vez que se valide la compra, automáticamente se imprima el recibo de la misma. 

En el campo *Facturación* se va a especificar el tipo de *Diario* que por defecto va a generar el sistema.  Los *Puntos de Venta* por defecto deberían de tener una *Boleta electrónica*.

<img src="img_pos/pos_12.png" alt="texto alternativo" >

*Inventario*: En esta opción se va a seleccionar el tipo de *Operación* donde se va a registrar la venta realizada en el módulo de *Inventario*.

<img src="img_pos/pos_13.png" alt="texto alternativo" >

*Contabilidad*: En esta opción se va a seleccionar el tipo de *Diario* de venta, para los pedidos del *Punto de Venta*.

<img src="img_pos/pos_14.png" alt="texto alternativo" >

*Informes de venta*: Se va a especificar el Equipo de venta al  que se le van a reportar las ventas. Por defecto está definido el *Punto de Venta*. 

<img src="img_pos/pos_15.png" alt="texto alternativo" >

Pestaña  *Ampliar Configuración*.

En el campo *Diario de Factura*  se va a especificar los tipos de *Diarios* que se van a visualizar en el *Punto de Venta* que se deben de generar a partir de las compras realizadas. 

En caso de existir clientes con RUC, el cliente puede definir además de la anterior que se había definido que fue Boleta Electrónica, puede seleccionar la opción de Factura Electrónica, o cualquier otra que desee que se visualice.

Los *Diarios* que se registren en este campo se van a mostrar en el *Punto de Venta*, cuando se vaya a validar la venta, permitiéndole al usuario especificar el tipo de documento que el cliente desea generar.

En el campo *Cliente predeterminado* se puede seleccionar con que cliente se va a empezar las ventas. 

<img src="img_pos/pos_16.png" alt="texto alternativo" >

Una vez configurado y guardado el *Punto de Venta* este se va a visualizar en la pestaña *Tablero*.

<img src="img_pos/pos_17.png" alt="texto alternativo" >

Para poder comenzar con las ventas se debe de dar *clic* en el botón *Nueva sesión*. O en caso de haberse iniciado anteriormente *Reanudar*.

<img src="img_pos/pos_18.png" alt="texto alternativo" >

Al iniciar la sesión en el *Punto de Venta* se va a mostrar la vista para llevar a cabo la venta de los productos existentes en el punto. 

<img src="img_pos/pos_19.png" alt="texto alternativo" >

En la parte superior se muestra la opción para buscar algún producto específico que el cliente desee comprar. 

<img src="img_pos/pos_20.png" alt="texto alternativo" >

El vendedor va a ir marcando los productos que el cliente desea comprar y estos se van a ir mostrando en la parte superior izquierda. 

Mostrándose la suma general del precio de cada uno de estos productos. 

En caso de que el cliente desee comprar más de un artículo, mediante los botones de los números existentes se puede asignar la cantidad deseada del producto seleccionado.  

En la configuración del *Precio*, se definió que los precios  de los productos se mostraran sin  impuesto, es por eso que el valor de los impuestos no se muestra. 

En caso que se marque la opción de con impuesto se mostraría de la siguiente manera, y se aplicarían según los impuestos que tengan definidos los productos que se seleccionen. 

<img src="img_pos/pos_37.png" alt="texto alternativo" >

Para que un producto tenga un impuesto y este se visualice en la compra, es importante que este, esté definido en el producto seleccionado.  Para estar seguro del tipo de impuesto aplicado, se va a seleccionar en el menú *Productos*, perteneciente a la pestaña *Productos*.

<img src="img_pos/pos_33.png" alt="texto alternativo" >

Se va a buscar el *Producto* al que se le quiere adicionar o verificar la existencia del impuesto.

Para esto se va aponer el nombre en la opción del buscar y se va a dar *ENTER*, visualizándose los productos a los que corresponden el nombre introducido.

<img src="img_pos/pos_34.png" alt="texto alternativo" >

Se va a dar *clic* en el producto, mostrándose las características del mismo.

<img src="img_pos/pos_35.png" alt="texto alternativo" >

En la pestaña *Información General*, en el campo *Impuestos del cliente* , se va a mostrar el impuesto definido.

En caso de no tener ningún impuesto, se debe de seleccionar el botón *Editar* para modificar la información, en caso de querer asignarle un impuesto al producto.

Seleccionándose luego el botón *Guardar*, para guardar la información modificada. 

<img src="img_pos/pos_36.png" alt="texto alternativo" >

Una vez que el producto presente un impuesto definido, en el *Punto de Venta* se debe de mostrar de la siguiente forma. 

<img src="img_pos/pos_37.png" alt="texto alternativo" >

Luego de que se seleccionen todos los productos que se van a comprar, el vendedor debe de seleccionar el *Cliente* al que se le está realizando la venta. 

En la configuración en la pestaña *Ampliar Configuración*, descrita anteriormente, existe un campo que permite definir un *Cliente* por defecto, el cual se visualizaría en esta vista. En caso que no se haya especificado ninguno se debe de acceder al botón *Cliente*. 

<img src="img_pos/pos_22.png" alt="texto alternativo" >

Mostrándose la siguiente vista.

<img src="img_pos/pos_23.png" alt="texto alternativo" >

En la opción 1 se puede poner el nombre del cliente al que se le desea asignar la compra. En caso de no existir al dar *clic* a la opción 2 se podrá adicionar un nuevo cliente mostrándose una nueva vista.

<img src="img_pos/pos_24.png" alt="texto alternativo" >

Al seleccionar la opción 2 para crear un nuevo elemento se va a mostrar la siguiente vista.  

<img src="img_pos/pos_25.png" alt="texto alternativo" >

Al introducir el *RUC* del cliente, y dar *clic* fuera del cuadro, este automáticamente va a cargar el nombre y los datos del cliente al que pertenece el *RUC* introducido. 

También permite al vendedor introducir los datos del nuevo cliente manualmente. 

Una vez que los datos del nuevo usuario sean introducidos, se va a seleccionar el disquete, que es la opción de guardar, que se muestra al lado de  la opción de recargar la página. 

<img src="img_pos/pos_26.png" alt="texto alternativo" >

Una vez que se defina el usuario al que se le va a asignar la compra, se debe de dar *clic* en el botón *Establecer Cliente*.

<img src="img_pos/pos_27.png" alt="texto alternativo" >

Mostrándose en la vista de la venta el cliente cargado.

<img src="img_pos/pos_28.png" alt="texto alternativo" >

Luego de definido el usuario, el vendedor puede seguir adicionando productos a la compra. En caso que ya el cliente tenga todos los productos que desea comprar, se procede a pagar la compra seleccionando el  botón *Pago*.

<img src="img_pos/pos_29.png" alt="texto alternativo" >

Al seleccionar el botón *Pago* se va a mostrar la siguiente vista.

<img src="img_pos/pos_30.png" alt="texto alternativo" >

Como se muestra se van a visualizar los *Métodos de pago* definidos al configurar el *Punto de venta* , así como los *Diarios* definidos en la pestaña *Ampliar Configuración*. 

En caso de seleccionar el *Método de pago* *Efectivo*, el vendedor  va a introducir manualmente el valor del efectivo depositado por el cliente, reportando un *Vuelto* en caso de que el valor sea superior al real.

<img src="img_pos/pos_31.png" alt="texto alternativo" >

En el caso de que el *Método de pago* sea *Banco* el valor que se mostrará va a ser el mismo generado a partir del valor de la compra. Ya que en el pago por el banco se realiza por transferencia y el valor es exacto.

<img src="img_pos/pos_32.png" alt="texto alternativo" >

Luego el usuario debe de definir qué tipo de documento es el que se va a emitir. Para esto va a seleccionar una de las opciones que se muestran a continuación, las cuales se definieron al configurar el *Punto de Venta*, en la pestaña *Ampliar Configuración*, antes nombrada.

<img src="img_pos/pos_38.png" alt="texto alternativo" >


En caso de no seleccionar ninguno por defecto en la configuración del *Punto de Venta* se definió el tipo de *Diario* que se generaría en la pestaña *Facturación*.


<img src="img_pos/pos_39.png" alt="texto alternativo" >

En caso que se seleccione el tipo de documento se generara el documento seleccionado por el usuario.
Luego de seleccionado el tipo de documento (en este caso se seleccionó *Factura electrónica*), se va a emitir un *Recibo de compra*, con el contenido de toda la compra realizada.

<img src="img_pos/pos_41.png" alt="texto alternativo" >

Automáticamente según la configuración que se haya definido en la impresión del *Recibo de venta* el sistema va a generar un PDF el cual va a poder ser impreso en caso de que presente configurada alguna impresora. 

<img src="img_pos/pos_40.png" alt="texto alternativo" >

El documento generado a partir de la compra se registrará en el módulo de *Facturación* como una factura de venta, en este caso como una Factura de cliente. 

Para visualizar esta información se debe de acceder al módulo *Facturación* y en la pestaña *Cliente*, seleccionar el menú *Facturas*.

<img src="img_pos/pos_42.png" alt="texto alternativo" >

<img src="img_pos/pos_43.png" alt="texto alternativo" >

Como se muestra, la *Factura* va a aparecer en estado *Pagado*.

Luego de realizado el Pago y de emitir el documento. El sistema permite seguir realizando compras, para esto se debe de seleccionar el botón *Siguiente pedido*.

<img src="img_pos/pos_44.png" alt="texto alternativo" >

Mostrándose nuevamente la vista para seleccionar los productos que se desean comprar. 

En caso de que no se vaya a realizar más ventas en el sistema, se debe de seleccionar el botón *Close* o *Cerrar*. 

<img src="img_pos/pos_45.png" alt="texto alternativo" >

Y luego seleccionar la opción *Confirmar*.

<img src="img_pos/pos_46.png" alt="texto alternativo" >

Mostrándose los *Puntos de Ventas*, en la pestaña *Tableros*.

<img src="img_pos/pos_47.png" alt="texto alternativo" >

Pestaña *Pedidos*.

Al dar *clic* a la pestaña *Pedidos* se van a mostrar los menús *Pedidos*, *Sesiones*, *Pagos* y *Clientes*.

Menú: *Pedidos*.

Al acceder al menú *Pedidos* se van a visualizar todos los pedidos  generados por las ventas realizadas por todos los *Puntos de Ventas* registrados en el sistema.

<img src="img_pos/pos_51.png" alt="texto alternativo" >

Mediante la opción marcada el usuario podrá imprimir en un Excel la información que se muestra en esta vista.

<img src="img_pos/pos_52.png" alt="texto alternativo" >

Menú: *Sesiones*.

Al acceder al menú *Sesiones* se va a mostrar las sesiones iniciadas en los *Puntos de Ventas* existentes. 

<img src="img_pos/pos_53.png" alt="texto alternativo" >

La fecha del campo *Fecha de cierre* se actualizará una vez que se cierre el *Punto de Venta* que se explica posteriormente. 

Mediante esta opción <img src="img_pos/pos_54.png" alt="texto alternativo" >, el usuario puede imprimir la información que se visualiza en esta vista.

Menú: *Pagos*.

Al acceder al menú *Pagos* se va a mostrar, los tipos de pagos que se aplicaron en las compras. 
Y los detalles de las compras que se llevaron a cabo en estos pagos. 

<img src="img_pos/pos_55.png" alt="texto alternativo" >


<img src="img_pos/pos_56.png" alt="texto alternativo" >

Menú: *Clientes*.

Al acceder al menú *Clientes*, se van a mostrar los clientes que han realizado compras en los *Puntos de Ventas* definidos en el sistema.

<img src="img_pos/pos_57.png" alt="texto alternativo" >

Al seleccionar uno de los clientes definidos, se podrá visualizar las compras que están asociadas al mismo.  Para esto se debe de acceder a la opción *Pedidos de PdV*.

<img src="img_pos/pos_58.png" alt="texto alternativo" >

Listándose los *Pedidos de los Puntos de Ventas. realizados por este cliente en el sistema. 

<img src="img_pos/pos_59.png" alt="texto alternativo" >

Para visualizar el monto total de las ventas realizadas se puede acceder a la opción *Facturado*.

<img src="img_pos/pos_60.png" alt="texto alternativo" >

Pestaña *Productos*.

En esta pestaña se van a visualizar los menús *Productos* y *Variantes de productos*.

Además de estos menús se van a configurar los menús *Tarifas* y *Programas de lealtad* , los cuales para que se muestren se debe de seleccionar e instalar.

Menú: *Productos*.

Al acceder a este menú se van a visualizar los mismos productos definidos en el módulo de *Inventario*.

Estos productos son los que se van a visualizar para ser seleccionados en el *Punto de Venta*.

<img src="img_pos/pos_61.png" alt="texto alternativo" >

Al acceder a uno de los productos se muestra  todas las características del mismo, y en las pestaña *A mano*, se va a mostrar la cantidad en existencia. 

<img src="img_pos/pos_62.png" alt="texto alternativo" >

Al acceder a la pestaña *Trazabilidad* se va a mostrar los movimientos que se han realizado con este producto. 

<img src="img_pos/pos_63.png" alt="texto alternativo" >

Menú:*Variantes de productos*.

Al acceder a este menú se van a visualizar los variantes de productos definidos en el módulo *Inventario*, que estarán disponibles en el *Punto de Venta*.

<img src="img_pos/pos_64.png" alt="texto alternativo" >

Menú: *Tarifas*.

Para que se visualice el menú *Tarifas*, se debe de acceder a la pestaña *Configuración*, y dentro seleccionar el menú *Configuración*.

<img src="img_pos/pos_65.png" alt="texto alternativo" >

Dentro de la vista *Configuración*, se debe de marcar en la pestaña *Precio*, la opción *Tarifas*.

<img src="img_pos/pos_66.png" alt="texto alternativo" >

Una vez marcada esta opción y seleccionado el botón *Guardar*, se va a visualizar el menú *Tarifa* en la pestaña *Productos*.

Al acceder a este menú se va a mostrar la siguiente vista.

<img src="img_pos/pos_67.png" alt="texto alternativo" >

A partir del menú *Tarifa* se van a definir los distintos tarifarios que el usuario quiera aplicar a la venta de sus productos.

Al seleccionar en la tabla *Normas de pecios*, la opción *Añadir línea* se van a introducir los productos a los que se les va a aplicar esta opción. Para esto se definen además la *Variante* del mismo, la *Cantidad mínima* que se debe de comprar , el *Precio* que se le va a aplicar y las *Fechas* en las que se va a aplicar esta tarifa a el producto que se ha seleccionado. 

<img src="img_pos/pos_68.png" alt="texto alternativo" >

Menú: *Programas de lealtad*.

Para que se visualice el menú *Programas de lealtad* , el usuario debe de instalar la siguiente app , mediante el módulo de *Aplicaciones*.

<img src="img_pos/pos_69.png" alt="texto alternativo" >

Una vez que se haya instalado la siguiente app, se va a visualizar en la pestaña *Productos* el menú *Programas de lealtad*.

<img src="img_pos/pos_70.png" alt="texto alternativo" >

El objetivo de este menú es que cada vez que el cliente realiza una compra o compra un producto, se le puede ir adicionando un punto, y cuando presente un grupo de puntos puede tener la opción de adquirir un producto deseado por el mismo cambiando estos puntos por el producto deseado o el que tenga definido el sistema. 

Al acceder a este menú se va a visualizar el botón *Crear* y al dar *clic* se va a mostrar la siguiente vista.

<img src="img_pos/pos_71.png" alt="texto alternativo" >

En esta vista se van a definir los siguientes campos:

* Nombre con el que se va a identificar este *Programa de lealtad*.

* Puntos que va a poder adquirir el usuario. Puede ser por *Moneda empleada*, es decir por cada sol que pague puede adquirir puntos o puede ser por *Pedido* o por *Producto*.

* Reglas, las cuales van a definir cómo se van a ir sumando los puntos.

<img src="img_pos/pos_72.png" alt="texto alternativo" >

* Recompensas, las cuales van a definir las recompensas que se van a obtener, y de que forma se van a obtener. 

<img src="img_pos/pos_73.png" alt="texto alternativo" >

Para esto el usuario debe de especificar el *Tipo de recompensa* y a partir del tipo de recompensa que defina serán los otros campos que se deban de definir. 

En el campo *Costo de recompensa* se debe de definir la cantidad de puntos que debe de tener el cliente para poder adquirir esta recompensa, así como la cantidad de puntos que como mínimo debe de tener. 

Pestaña *Informes*.

En esta pestaña se van a mostrar los menús *Pedidos* y *Detalles de ventas*.

Menú *Pedidos*.

Se va a mostrar una gráfica de los *Pedidos*, y para esto el usuario puede seleccionar los parámetros que están definidos, logrando visualizar la gráfica correspondiente al parámetro seleccionado. 

<img src="img_pos/pos_74.png" alt="texto alternativo" >

Menú: *Detalle de venta*.

Al seleccionar el menú *Detalle de venta* se va a mostrar la siguiente vista, permitiéndole al usuario seleccionar cual *Punto de Venta* es el que desea visualizar. 

<img src="img_pos/pos_75.png" alt="texto alternativo" >

Una vez seleccionado el *Punto de venta* se va a seleccionar el botón *Imprimir*, generándose un PDF, con los *Detalles de las ventas* , del *Punto de Venta* seleccionado. 

<img src="img_pos/pos_76.png" alt="texto alternativo" >

Pestaña *Configuración*.

En esta pestaña se van a mostrar los menús *Configuración*, *Punto de Venta*, *Métodos de pago*, *Categoría de productos*, *Atributos* e *Impuestos*.

Menú: *Configuración*.

Al acceder a este menú, se va a visualizar la siguiente vista.

<img src="img_pos/pos_77.png" alt="texto alternativo" >

* Como se muestra en esta el cliente puede configurar el *Impuesto* que se le va a aplicar a todos los productos que se van a comercializar, en caso que desee que sea global.

* En la pestaña *Precio*, se va a definir si se va a aplicar alguna *Tarifa* y que tipo de tarifa se pueden crear. Este campo es el menú que se visualiza en la pestaña *Productos*. 

* Va a permitir definir si se va a emplear *Multi monedas* para el pago de los productos. 

* Se va a poder definir una *Terminal de pago*.

* En los *Pagos* , va a darle la posibilidad al vendedor de definir *Redondeos de efectivo*. 

Menú *Punto de venta*.

Al acceder a este menú se van a visualizar todos los *Puntos de Ventas* creados.

<img src="img_pos/pos_78.png" alt="texto alternativo" >

Al seleccionar el botón *Crear*, se va a mostrar la vista correspondiente a los datos que le van a permitir al usuario configurar el *Punto de Venta* que está creando.

Esta información es la misma que se describió de una de los *Punto de Venta* inicialmente. 

<img src="img_pos/pos_79.png" alt="texto alternativo" >

Menú: *Método de pago*.

Al acceder a este menú, se va a visualizar el listado de los métodos de pagos definidos en el sistema, los cuales son los que se van a visualizar una vez que se realice la selección de los productos deseados por el cliente y que se pase a la opción de *Pago*. 

<img src="img_pos/pos_80.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* va a  visualizar una vista, mediante la cual se deben de introducir los datos para cree un nuevo *Método de pago*.

<img src="img_pos/pos_81.png" alt="texto alternativo" >

Menú *Categoría de productos*.

Al acceder a este menú, se van a visualizar las *Categorías de productos* en los que se mostrarán separados los *Productos* en el *Punto de Venta* existente.

<img src="img_pos/pos_82.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* se va a mostrar una vista con los datos correspondientes para crear una nueva *Categoría de producto*. 

<img src="img_pos/pos_83.png" alt="texto alternativo" >

Mostrándose de la siguiente forma en el *Punto de Venta*.

<img src="img_pos/pos_84.png" alt="texto alternativo" >

Menú: *Atributos*.

Al acceder a este menú se va a mostrar el listado de los atributos definidos en el sistema que podrán ser aplicados a las *Variantes de productos*.

<img src="img_pos/pos_85.png" alt="texto alternativo" >

Al acceder al botón *Crear* se va a mostrar una vista con los datos necesarios para crear un nuevo *Atributo*.  

<img src="img_pos/pos_86.png" alt="texto alternativo" >

Menú: *Impuestos*.

Se va a visualizar el listado de los *Impuestos* definidos en el sistema, los cuales se generan en el módulo *Facturación*. 

<img src="img_pos/pos_87.png" alt="texto alternativo" >

Mediante el botón *Crear* , el usuario puede crear un nuevo *Impuesto*, el cual luego se le podrá aplicar a los productos que van a ser comprados por los usuarios.

Una vez que el usuario haya terminado de realizar las ventas correspondientes va a pasar a cerrar el *Punto de Venta*. 

Para esto se va a seleccionar en la pestaña *Tablero* el *Punto de Venta* que se desea cerrar. En este caso se va a cerrar el *Punto de Venta* **Shop**.

Se va a dar *clic* en el botón *Cerrar*. 

<img src="img_pos/pos_48.png" alt="texto alternativo" >

Al seleccionar este botón se va a mostrar la siguiente vista.

<img src="img_pos/pos_49.png" alt="texto alternativo" >

Como se muestra se va a mostrar la hora en que se inició el *Punto de Venta*, la cantidad de ventas realizadas y el monto total de las mismas. 

Para saber los detalles de estas ventas se debe de dar *clic* en la pestaña *Pedidos*.

<img src="img_pos/pos_88.png" alt="texto alternativo" >

Para saber los detalles de los pagos realizados se debe de dar *clic* en la pestaña *Pagos*. Mostrándose la siguiente información.

<img src="img_pos/pos_89.png" alt="texto alternativo" >

Una vez que el usuario seleccione el botón *Cerrar* se van a mostrar los siguientes botones.

<img src="img_pos/pos_90.png" alt="texto alternativo" >

Al seleccionar el botón *Continuar vendiendo* va a mostrar la vista para continuar la venta en el *Punto de Venta*. 

Si por el contrario selecciona el botón *Validar cierre y contabilizar asiento*, el estado del *Punto de Venta* va a pasar a *Cerrado y Contabilizado*. 

Mostrándose el campo *Fecha de cierre*, con el valor de la fecha y la hora en que se cerró el *Punto de Venta*.

<img src="img_pos/pos_91.png" alt="texto alternativo" >

De esta forma el *Punto de Venta* queda cerrado, y permitiendo que se pueda iniciar una nueva sesión próximamente.

<img src="img_pos/pos_92.png" alt="texto alternativo" >

