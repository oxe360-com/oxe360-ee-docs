# Modulo : l10n_pe_pos

Para visualizar los nuevos campos creados en este modulo debemos ingresar a modulo "Punto de Venta".

<img src="img_pos/l10n_pe_pos_1.png" alt="texto alternativo" >

Dentro del modulo de "Punto de Venta" nos colocaremos en la opciones del main y luego daremos clic en la opcion "configuración".  

<img src="img_pos/l10n_pe_pos_2.png" alt="texto alternativo" >
 
En configuración nos decenderemos hasta el final de patantalla donde podremos vizualizar la nueva pestaña "Extend Setting"  y sus nuevos campos de configuracion de Punto de Venta.

<img src="img_pos/l10n_pe_pos_3.png" alt="texto alternativo" >

