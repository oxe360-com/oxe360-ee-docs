
# Modulo: L10n_pe_purchase_lander_cost

* Se visualiza la opcion *User Accounting Landed*, en las preferencias del usuario registrado.

Para visualizar esta opción, luego de instalado el módulo **L10n_pe_purchase_lander_cost**, se debe de acceder al módulo **Ajustes**. 

<img src="img/L10n_pe_purchase_lander_cost_1.png" alt="texto alternativo" >

Dentro de este módulo se debe de seleccionar la pestaña  *Usuarios y compañías*. Dentro se debe de seleccionar la funcionalidad *Usuarios*.

<img src="img/L10n_pe_purchase_lander_cost_2.png" alt="texto alternativo" >

Dentro se muestran los usuarios existentes en el sistema, se debe de seleccionar el usuario registrado en el sistema.

<img src="img/L10n_pe_purchase_lander_cost_3.png" alt="texto alternativo" >

Al acceder al mismo se van a visualizar los datos y los permisos de este usuario.

<img src="img/L10n_pe_purchase_lander_cost_4.png" alt="texto alternativo" >

En la pestaña *Permiso de acceso* en el grupo de permisos de *Configuración Técnica*, se va a visualizar la opción *User Accounting Landed*. Se debe de marcar esta opción.

<img src="img/L10n_pe_purchase_lander_cost_5.png" alt="texto alternativo" >

* Se visualiza la pestaña *Costing*, con la opción *Landed Costs*.

Para visualizar esto se debe de acceder en el módulo **Compra**,  en la pestaña Configuración la funcionalidad *Configuración*.

<img src="img/L10n_pe_purchase_lander_cost_11.png" alt="texto alternativo" >

Y dentro de *Costing* se visualiza la opción ya marcada. 

<img src="img/L10n_pe_purchase_lander_cost_12.png" alt="texto alternativo" >

Una vez que se instale el módulo se debe de marcar esta opción en caso que no esté marcado.

Es importante tener en cuenta que luego de instalado el módulo antes de empezar a trabajar con esta funcionalidad se debe de configurar las *Precisiones decimales*. 
Para esto se debe de acceder al módulo **Ajustes**, y en la opción señalada activar el *modo desarrollador*.

<img src="img/L10n_pe_purchase_lander_cost_16.png" alt="texto alternativo" >

Luego de activado el modo desarrollador, se va a visualizar la pestaña *Técnico*. En el agrupador Estructura de base de datos, se debe de acceder al menú *Precisión decimal*. 

<img src="img/L10n_pe_purchase_lander_cost_17.png" alt="texto alternativo" >

Al acceder a este menú se va visualizar un grupo de elementos, para un mejor funcionamiento del sistema se debe de definir la cantidad de dígitos que se van a mostrar de los costos que se visualizaran, en este caso se van a definir para todos 4 dígitos.

<img src="img/L10n_pe_purchase_lander_cost_18.png" alt="texto alternativo" >


* Luego de configurado todo lo descrito anteriormente se va a acceder al 
módulo **Compras**.

<img src="img/L10n_pe_purchase_lander_cost_6.png" alt="texto alternativo" >

Al acceder a este módulo se va a seleccionar en la pestaña *Pedidos*, donde se va a visualizar la funcionalidad *Tipos de costos aterrizados*.

<img src="img/L10n_pe_purchase_lander_cost_7.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se va a visualizar el listado de los tipos costos de aterrizados que se han registrado.

<img src="img/L10n_pe_purchase_lander_cost_8.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* se van a visualizar los campos de esta funcionalidad. 

<img src="img/L10n_pe_purchase_lander_cost_9.png" alt="texto alternativo" >

Como se muestran en los campos marcados, el usuario debe de definir un *Nombre* y un *Costo* para el mismo. 
En el campo *Método dividido* se debe de seleccionar la opción Por costo actual.
En el campo *Tipo de producto*, se debe de seleccionar Servicio.  

Al acceder a la opción *Categoría de producto* se debe de verificar los campos definidos, para esto se acceder a la opción que se muestra a continuación. 
 
<img src="img/L10n_pe_purchase_lander_cost_20.png" alt="texto alternativo" >

Al acceder a esta opción se van a visualizar la siguiente vista.

<img src="img/L10n_pe_purchase_lander_cost_19.png" alt="texto alternativo" >

El usuario debe de verificar que los campos señalados tengan los valores que se muestran.

* Una vez que se configure y esta información y se cree un *Tipo de coste promedio*, se va a acceder al menú *Solicitudes de presupuesto*.
  
Para esto se va a dar *clic* a la pestaña *Pedidos* y dentro se va a seleccionar el menú *Solicitudes de presupuesto*.

<img src="img/L10n_pe_purchase_lander_cost_21.png" alt="texto alternativo" >

Al acceder a este menú se va a visualizar el listado de las solicitudes creadas.

<img src="img/L10n_pe_purchase_lander_cost_22.png" alt="texto alternativo" >


Al acceder a esta funcionalidad y dar *clic* en el botón *Crear* se va a visualizar la siguiente interfaz con los campos que se han adicionado a partir de la instalación de este módulo.

<img src="img/L10n_pe_purchase_lander_cost_14.png" alt="texto alternativo" >

<img src="img/L10n_pe_purchase_lander_cost_23.png" alt="texto alternativo" >

Para registrar una *Solicitud de presupuesto* y aplicarle un *Tipo de costo de aterrizaje* se debe de realizar el siguiente flujo.

Se va a introducir el nombre del *Proveedor* .

En la *Línea de los productos* se va a adicionar el producto o los productos a los que se les van a aplicar el *Tipo de costo de aterrizaje*.

Se debe de verificar en las características del producto que este sea *Almacenable*. 

Para esto se va a seleccionar la opción que se encuentra marcada a continuación.

<img src="img/L10n_pe_purchase_lander_cost_24.png" alt="texto alternativo" >

Al dar *clic* se van a visualizar las características del *Producto*, y en este se debe de verificar que el *Tipo de producto* sea *Almacenable* y la *Categoría de producto* sea de tipo *All*. 

<img src="img/L10n_pe_purchase_lander_cost_26.png" alt="texto alternativo" >

Luego de introducido el producto se va a adicionar un *Coste de aterrizaje*. Para esto se va a dar *clic* en la opción *Agregar línea*. 

<img src="img/L10n_pe_purchase_lander_cost_27.png" alt="texto alternativo" >

Se van a visualizar los *Tipos de costes de aterrizaje* que se han agregado.
En este caso se va a seleccionar el que se agregó anteriormente.

<img src="img/L10n_pe_purchase_lander_cost_28.png" alt="texto alternativo" >

Una vez que se seleccione se va a visualizar los elementos que se les definieron. 
Luego de esto se va a dar *clic* en el botón *Calcular*.

<img src="img/L10n_pe_purchase_lander_cost_29.png" alt="texto alternativo" >

Una vez que se haya realizado esta operación el valor del campo *Subtotal LC* varia, va a tomar de la suma entre el *Total* y el *Costo total de aterrizaje*.

<img src="img/L10n_pe_purchase_lander_cost_30.png" alt="texto alternativo" >

Además se va a llenar la tabla *Ajustes de valoración*, donde se va a mostrar la *Cantidad* definida por productos, el *Costo formado (por unidad)*, el *Costo formado*, que va a ser el valor del campo *Total*, y el *Costo adicional de aterrizaje*. 

<img src="img/L10n_pe_purchase_lander_cost_31.png" alt="texto alternativo" >

Una vez terminado el flujo, se selecciona el botón *Guardar*. 
Luego de guardado se puede *Enviar por correo electrónico*, *Imprimir*, *Cancelar* o *Confirmar*.

<img src="img/L10n_pe_purchase_lander_cost_32.png" alt="texto alternativo" >

Como se visualiza se va a enumerar la *Solicitud de presupuesto* y el estado va a ser *Petición presupuesto*. 

Al seleccionar el botón *Confirmar Pedido*, se va a mostrar el botón *Recibir productos*. La solicitud de presupuesto en la numeración se va a identificar como una *Orden de compra*, pasando el estado a ser *Orden de compra* y se va a registrar una *Recepción*. 

<img src="img/L10n_pe_purchase_lander_cost_33.png" alt="texto alternativo" >

Al dar *clic* en la opción *Recepciones* se va a visualizar la siguiente vista.

<img src="img/L10n_pe_purchase_lander_cost_39.png" alt="texto alternativo" >

Esta *Recepción* también se va a registrar en el módulo de *Inventario*. 

Para visualizar esta información , se debe de acceder este módulo. 

<img src="img/L10n_pe_purchase_lander_cost_34.png" alt="texto alternativo" >

En la pestaña *Información general*, en el tipo de operaciones *Recepciones*. 

<img src="img/L10n_pe_purchase_lander_cost_35.png" alt="texto alternativo" >

Al dar *clic* en la opción señalada se van a visualizar todas las *Recepciones* realizadas.

<img src="img/L10n_pe_purchase_lander_cost_36.png" alt="texto alternativo" >

<img src="img/L10n_pe_purchase_lander_cost_37.png" alt="texto alternativo" >

Al seleccionar el botón *Validar* se va a visualizar el campo *Fecha efectiva*, tomando el valor actual y el estado de la operación va a pasar a *Realizado*. 

<img src="img/L10n_pe_purchase_lander_cost_38.png" alt="texto alternativo" >

De esta misma forma se visualiza mediante la opción *Recepciones* en el módulo de *Compras*, al que anteriormente se había accedido.

<img src="img/L10n_pe_purchase_lander_cost_39.png" alt="texto alternativo" >


