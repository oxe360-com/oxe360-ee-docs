# Modulo: l10n_pe_purchase

* Se modifican los nombres de los botones que se visualizan de la funcionalidad *Solicitudes de presupuesto*. 

Para ver esta información se debe de acceder al módulo **Compra**. 

<img src="img/pe_purchase_1.png" alt="texto alternativo" >

Dentro de este módulo se debe de acceder a la pestaña *Pedidos*, y dentro seleccionar la funcionalidad *Solicitud de presupuesto*.

<img src="img/pe_purchase_2.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se va a visualizar el listado de los elementos introducidos. Al seleccionar el botón *Crear*, se va a mostrar una interfaz con los campos a introducir. 
Se visualiza el botón con nombre *Send RFQ by Email*, y la *Solicitud de compra* va a estar en estado *Petición Presupuesto*.

<img src="img/pe_purchase_3.png" alt="texto alternativo" >

Al dar *clic* en el botón *Guardar* y luego dar clic en el botón *Send RFQ by Email* se va a mostrar la siguiente interfaz con la estructura del correo que se va enviar al Proveedor.

<img src="img/pe_purchase_4.png" alt="texto alternativo" >

Una vez que el usuario da *clic* en el botón *Enviar, se va a visualiza el botón *Re-Send RFQ by Email*, y la *Solicitud de compra* está en estado *Petición de Cotización Enviada*. 

<img src="img/pe_purchase_5.png" alt="texto alternativo" >

Al dar *clic* a este botón se va volver a enviar el correo, que anteriormente se había enviado, como un recordatorio. 

* Se va a visualizar el campo *See Total Pucharse Draft*, en la funcionalidad *Configuración*.

Para visualizar este campo se debe de acceder al módulo **Compras**, en la pestaña *Configuración* en la funcionalidad *Configuración*. 

<img src="img/pe_purchase_6.png" alt="texto alternativo" >

Al acceder a esta funcionalidad, en el encabezado *Pedidos* se van a mostrar los elementos que se visualizan a continuación. Marcando el campo al que se hace referencia en este módulo.

<img src="img/pe_purchase_7.png" alt="texto alternativo" >



