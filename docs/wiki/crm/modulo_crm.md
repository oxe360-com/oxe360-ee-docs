Modulo CRM 
==========

Este módulo nos brinda una interfaz de usuario diseñada para ventas. Permite gestionar la cartera de clientes realizando una gestión comercial de las necesidades existentes en el mercado. Gestiona y clasifica sus equipos de ventas desde las iniciativas y oportunidades. 
Al acceder al módulo en la parte superior derecho se muestra registrado el usuario con el que se ha accedido al sistema y es con el que se van a registrar las operaciones que se realicen en el mismo. 

<img src="img/0.jpg" alt="texto alternativo" >

Se  muestra en la vista Kanban la información existente en el sistema hasta el momento de las oportunidades que se han introducido, divididas en sus diferentes estados. Excepto las que están en estado Perdido.

<img src="img/1.jpg" alt="texto alternativo" >

Al seleccionar el botón crear se nos despliega un grupo de campos que nos permiten crear una nueva oportunidad, con los campos Oportunidad (nombre de la oportunidad), Cliente (nombre del cliente, el cual nos permite seleccionarlo de los existentes en el sistema), Ingreso estimado (que se espera obtener con esta oportunidad) y nos permite añadirle una Prioridad (el símbolo de las estrellas, la cual va a ser determinada por el usuario). 

<img src="img/2.jpg" alt="texto alternativo" >

<img src="img/3.jpg" alt="texto alternativo" >

<img src="img/4.jpg" alt="texto alternativo" >


Una  vez introducido los campos se selecciona el botón añadir y se guardara la Oportunidad como se muestra a continuación.

<img src="img/5.jpg" alt="texto alternativo" >

Otra vista para crear una oportunidad es por la vista Lista, la cual nos muestra todos los datos de cada una de las Oportunidades registradas y modificadas hasta el momento. 

<img src="img/6.jpg" alt="texto alternativo" >

Como se muestra en la parte lateral derecha nos permite seleccionar cuales campos de los registrados en el sistema deseamos que se nos muestre en la interfaz. 

Para crear una nueva Oportunidad en la vista Lista se da clic en el botón Crear y se muestra la siguiente interfaz:

<img src="img/7.jpg" alt="texto alternativo" >

Se introducen los datos correspondiste a la nueva oportunidad, así  como el ingreso estimado que se espera de la probabilidad de que se lleve a cabo la misma. Además de los campos correspondientes para introducir los datos del cliente y del vendedor.

En la parte inferior se encuentran un grupo de pestañas. Una de ellas nos permite guardar notas en el sistema y las otras son las diferentes etapas por las que pasa la negociación de esta nueva oportunidad de venta, Esto nos va a permitir tener un control del estado actual en que se encuentra, así como la documentación y los detalles que ya existen. 

<img src="img/8.jpg" alt="texto alternativo" >

<img src="img/9.jpg" alt="texto alternativo" >

+ Notas internas

<img src="img/10.jpg" alt="texto alternativo" >

+ Seguimiento

<img src="img/11.jpg" alt="texto alternativo" >

+ Desarrollo

<img src="img/12.jpg" alt="texto alternativo" >

+ Propuesta

<img src="img/13.jpg" alt="texto alternativo" >

+ Negociación

<img src="img/14.jpg" alt="texto alternativo" >

+ Cierre

<img src="img/15.jpg" alt="texto alternativo" >

Una vez que se hayan introducido todos los datos y se le dé clic al botón Guardar se van a mostrar la siguiente imagen.

<img src="img/16.jpg" alt="texto alternativo" >

Para cambiar de estado, solo se debe de dar clic encima del estado que se encuentra en la parte superior derecha  y el sistema va a ir cambiando de estado.

<img src="img/18.0.jpg" alt="texto alternativo" >

Como se muestra en la anterior imagen existen dos botones; uno de ellos es Marcado como ganado que es cuando se ha logrado la oportunidad.

<img src="img/18.jpg" alt="texto alternativo" >

El otro botón es cuando no se va a ejecutar la Oportunidad de negocio, Marcar Perdido. Al seleccionar esta opción el sistema muestra una interfaz que va a permitir introducir las causas de porque ya no se va a llevar a cabo la oportunidad. 

<img src="img/19.jpg" alt="texto alternativo" >

<img src="img/20.jpg" alt="texto alternativo" >

Una vez que se introducen los campos y se selecciona el botón Enviar el estado de la oportunidad cambia y el % de probabilidad se convierte en 0. 

<img src="img/21.jpg" alt="texto alternativo" >

Un vez perdida la oportunidad tiene la oportunidad de Restaurarla si se desea seleccionando el botón Restaurar, esto permitirá volver a mostrar la oportunidad en el estado Propuesta, con el % de probabilidad que presente por defecto el sistema.  





