## l10n_pe_account_boe

***views/res_config_settings_views***

Se adicionaron las siguientes elementos a la funcionalidad de **Ajustes**
Para visualizar los cambios debemos ingresar a Facturación.

<img src="img_boe/Letras1.jpg" alt="texto alternativo" >

Dar click en **Configuración/ajustes**.

<img src="img_boe/Letras2.jpg" alt="texto alternativo" >

Deslizar hacia abajo y dar clic en **Activar Ubicación de Letras**.

<img src="img_boe/Letras3.jpg" alt="texto alternativo" >

Luego de activar la ubicacion de letras tambien podemos a su configuracion dando click en el boton. 
**Configurar las Ubicaciones de Letras**

<img src="img_boe/Letras3-1.jpg" alt="texto alternativo" >

***views/account_boe_location_views***

Luego de dar click mostra la vasta con ubicacion cargadas por defecto y la opcion de crear mas ubicaciones de ser requerido.

<img src="img_boe/Letras4.jpg" alt="texto alternativo" >

-----

***views/menu***

-Para visualizar la opcion de letras en el sistema debe ingresar a facturación.

<img src="img_boe/Letras1.jpg" alt="texto alternativo" >

Luego dar click en la opcion clientes y se visualizara en la lista de opciones las nuevas adiciones de letras.

<img src="img_boe/Letras5.png" alt="texto alternativo" >

-------------
***views/account_view***

-Se Agrego un nuevo campo para la creacion de diarios.
Para visualizar el campo de ingresar a Facturacion o Contabilidad.

<img src="img_boe/Letras1.jpg" alt="texto alternativo" >

-Dar click en Configuracion/Diarios

<img src="img_boe/Letras6.png" alt="texto alternativo" >

-Para mostrar el nuevo campo debe elegir en Tipo la opcion **Varios** y se mostrara el nuevo checkbox **Is Bill Of Exchange**.

<img src="img_boe/Letras7.jpg" alt="texto alternativo" >

----------
**data/paper_format_boe_data**

-Se agrego un nuevo formato de imprecion para letras. 

Para visualizar el nuevo formato de impresion debe ingresar a **Configuracion**.

<img src="img_boe/Letras8.jpg" alt="texto alternativo" >

-Activar el modo desarrollador.

<img src="img_boe/Letras9.jpg" alt="texto alternativo" >

-Ingresar nuevamente a la opcion de configuracion y selecionar Tecnico/Informes/formato de Papel .

<img src="img_boe/Letras10.jpg" alt="texto alternativo" >

-una vez dentro podemos visualizar el registro del nuevo formato.

<img src="img_boe/Letras11.jpg" alt="texto alternativo" >

-El cuenta con las siguientes especificaciones.

<img src="img_boe/Letras12.jpg" alt="texto alternativo" >

-----------
***views/account_payment_view***

-Se agrego campos nuevos en la vista de pagos.

Para visualizar los nuevos campos debes ingresar a Facturacion o Contabilidad.

<img src="img_boe/Letras1.jpg" alt="texto alternativo" >

- Seleccionar la opcion de Clientes/Pagos.

<img src="img_boe/Letras13.jpg" alt="texto alternativo" >

- Dar click en el boton Crear .

<img src="img_boe/Letras14.jpg" alt="texto alternativo" >

- Se monstraran los campos agredados "Es letra de cambio" , "Cuenta de destino"(se muestra al elegir un cliente).
Una vez finalizado el ingreso de datos, seleccionamos **Confirmar** para terminar el proceso.

<img src="img_boe/Letras15.png" alt="texto alternativo" >

-En caso se tenga que editar la operacion podemos dar click en "**Cambiar movimiento**"

<img src="img_boe/Letras16.jpg" alt="texto alternativo" >

-Luego de dar click nos mostrara una ventana en la cual podemos editar la fecha y el diario segun se necesite, Una vez terminada la edicion correspondiente debemos dar click en cambiar movimiento para finalizar el proceso.

<img src="img_boe/Letras17.jpg" alt="texto alternativo" >

------------------

Se ha agregado una nueva para Letras de Cambio.

Para visualizar esta vista de ingresar a Facturacion o Contabilidad.

<img src="img_boe/Letras1.jpg" alt="texto alternativo" >

Seleccionar clientes/Letras de Cambio.

<img src="img_boe/Letras18.jpg" alt="texto alternativo" >

Luego de dar click nos mostrara la nueva vista con los nuevos campos creados. 

<img src="img_boe/Letras19.jpg" alt="texto alternativo" >

-------------------
Se ha agregado una nueva para Planilla de Letras.

Para visualizar esta vista de ingresar a Facturacion o Contabilidad.

<img src="img_boe/Letras1.jpg" alt="texto alternativo" >

Seleccionar Clientes/Planilla de Letras.

<img src="img_boe/Letras20.png" alt="texto alternativo" >

Luego de dar click nos mostrara la nueva vista con los nuevos campos creados 

<img src="img_boe/Letras21.png" alt="texto alternativo" >
