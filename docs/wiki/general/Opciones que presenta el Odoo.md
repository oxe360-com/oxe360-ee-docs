# Opciones que presenta Odoo
_________


Los módulos de Odoo , van a estar constituidos por varios elementos , uno de ellos son las *Vistas*. 

Estas se encuentran en la parte superior derecha al acceder a las funcionalidades de los módulos.

<img src="img_general/5-doc.png" alt="texto alternativo" >

Las vistas más comunes son la vista **Kanban** y la vista **Lista**.

La vista **Kanban** va a mostrar los elementos registrados en el sistema en forma de tarjetas, mostrando de forma más visible para el usuario los datos específicos de los elementos que se muestran registrados. 

<img src="img_general/6-doc.png" alt="texto alternativo" >

La vista **Lista** va a mostrar la información en forma de lista, permitiendo que el usuario pueda ver más elementos listados , pero para visualizar la información detallada de cada uno de ellos debe de acceder al mismo dando clic encima del elementos.

<img src="img_general/7-doc.png" alt="texto alternativo" >

El sistema muestra la posibilidad de seleccionar los campos que el usuario desee mostrar a partir de los definidos en la funcionalidad. Para acceder a ellos debe de dirigirse a la opción con forma de signo de exclamación **!** que se encuentra en la parte superior derecha. 

Al dar clic encima de ella se van a mostrar los campos que podrá visualizar en la vista **Lista**, así como el contenido registrado de los mismos por cada elemento registrado.

<img src="img_general/8-doc.png" alt="texto alternativo" >

Es importante destacar que cada funcionalidad tiene sus propios campos para seleccionar, ya que estos se van a generar a partir de los campos definidos en las funcionalidades. 

Para acceder a alguna de estas vistas solo se debe de dar *clic* encima de la opción deseada y la funcionalidad se va a mostrar de la manera que seleccione el usuario.

Otra de las opciones que existen en las funcionalidades de los módulos es el campo de **Búsqueda**, identificado por una *lupa* en la parte superior derecha  de las funcionalidades.

<img src="img_general/20-doc.png" alt="texto alternativo" >

En esta barra de búsqueda el usuario puede introducir el contenido que desee y el sistema va a filtrar a partir de los elementos que presenten los campos de los elementos registrados en el sistema.

<img src="img_general/21-doc.png" alt="texto alternativo" >

A partir de las opciones que va a mostrar el sistema, el usuario selecciona una y se realizará el filtro. 

<img src="img_general/22-doc.png" alt="texto alternativo" >

Otra de las opciones que se muestran son los **Filtros**, **Agrupar por** y **Favoritos**: 

<img src="img_general/9-doc.png" alt="texto alternativo" >


La opción **Filtros** va a permitirle al usuario agrupar los elementos por el campo seleccionado a partir de un listado de selección que se mostrará en el sistema al seleccionar la flecha que se muestra en la parte derecha del filtro.

<img src="img_general/10-doc.png" alt="texto alternativo" >

Una de las opciones que tienen los **Filtros** es que se puede *Añadir un filtro personalizado*.

<img src="img_general/11-doc.png" alt="texto alternativo" >

Se van a mostrar dos campos, en el primero se va a definir el *nombre del filtro*, y en el segundo se le va a asignar una *condición* definida por el sistema a partir del filtro que se haya definido anteriormente.

<img src="img_general/12-doc.png" alt="texto alternativo" >

En el ejemplo a mostrar se seleccionó el filtro con nombre *Móvil* y se generaron las siguientes condiciones.

<img src="img_general/13-doc.png" alt="texto alternativo" >

Se selecciona una de las opciones y se selecciona el botón Aplicar, generándose el nuevo filtro adicionado por el cliente.

A partir de los elementos registrados en el sistema será el resultado del filtro.

<img src="img_general/14-doc.png" alt="texto alternativo" >

<img src="img_general/15-doc.png" alt="texto alternativo" >

Los filtros creados en el sistema, una vez que se salga de la funcionalidad donde se ha definido no se van a mostrar nuevamente. Para que estos filtros puedan ser empleados nuevamente sin la necesidad de volver a crearlos se deben de guardar en la opción **Favoritos**.

Para esto luego de introducido el filtro seleccionamos la opción *Guardar búsqueda actual* en **Favoritos**, y le asignamos el nombre con el que se va a identificar.

<img src="img_general/23-doc.png" alt="texto alternativo" >

<img src="img_general/24-doc.png" alt="texto alternativo" >

De esta forma este filtro queda registrado en el sistema en la opción **Favoritos**, para su próximo empleo. 

<img src="img_general/25-doc.png" alt="texto alternativo" >

La opción **Agrupar por** va a mostrar todos los elementos agrupados a partir de la opción de agrupación que seleccione el usuario.

Para esto va a acceder a la opción **Agrupar por**, y al dar *clic* en la flecha derecha que presenta la opción **Agrupar por**, se van a deslizar las distintas opciones por las que se pueden agrupar en la funcionalidad en que se esté trabajando.

<img src="img_general/16-doc.png" alt="texto alternativo" >

De igual manera que en el **Filtrar** se puede *Agregar un grupo personalizado* para realizar agrupaciones.

<img src="img_general/17-doc.png" alt="texto alternativo" >

En dependencia al valor seleccionado para *Agrupar* , se realizara la agrupación en el sistema por los valores que presentan el agrupador definido por el usuario.

<img src="img_general/18-doc.png" alt="texto alternativo" >

Se seleccionó el agrupador *Compañía*, y se generó como resultado todas las compañías definidas en el sistema y al seleccionar la flecha del contenido agrupado , se van a desglosar todos los elementos registrados en el sistema que presenten el valor de la *Compañía* en el que están agrupados. 

<img src="img_general/19-doc.png" alt="texto alternativo" >

