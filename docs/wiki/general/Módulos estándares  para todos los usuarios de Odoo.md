# Módulos estándares  para todos los usuarios de Odoo
________________

Al acceder al sistema se van a mostrar para todos los usuarios las opciones:

* **Debate** (o Conversaciones)
* **Calendario**
* **Contactos**

<img src="img_general/0.0-doc.png" alt="texto alternativo" >

El módulo  **Debate**  le permite al usuario registrado en el sistema tener conversaciones entre los otros usuarios  registrados en el sistema. Estas conversaciones pueden ser privadas o por medios de canales o grupos. 

<img src="img_general/0-doc.png" alt="texto alternativo" >

También se puede acceder en la parte superior de la vista, donde se van a mostrar los mensajes de los canales y las charlas que se han entablado. Además se van a mostrar los correos que se han generado a partir de las distintas acciones que se han realizado en el sistema.

<img src="img_general/1-doc.png" alt="texto alternativo" >

<img src="img_general/2-doc.png" alt="texto alternativo" >

Mediante el módulo  **Calendario** el usuario podrá  organizar sus actividades, a partir de una agenda que va a permitir registrar las actividades por días de la semana. 

<img src="img_general/3-doc.png" alt="texto alternativo" >

El módulo **Contactos** va a permitir tener almacenada la información de todos los usuarios y empresas  existentes en el sistema. Va a permitir al usuario registrar nuevas entidades y usuarios en el sistema, los que se van a clasificar en  proveedores y clientes. 

<img src="img_general/4-doc.png" alt="texto alternativo" >

Para un mejor entendimiento de cómo funcionan estos módulos en Odoo el usuario se puede apoyar en la
documentación de Odoo.sh , definida para los usuarios, visitar la siguiente URL:

URL:  https://www.odoo.com/documentation/user/13.0/es/



