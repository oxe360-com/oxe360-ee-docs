# Modulo: l10n_pe_oxe360

En este módulo se introdujeron cambios referentes a carga de datos y nuevos campos.

* Al crear un nuevo Contacto se le adicionaron nuevos campos a partir del Tipo de contacto que se esté creando. 

Para visualizar este contenido de debe de acceder al módulo **Contactos**. 

<img src="img_oxe360/1_pe_oxe360.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* se van a mostrar los campos que se deben de introducir. 

<img src="img_oxe360/1.1_pe_oxe360.png" alt="texto alternativo" >

En caso que el contacto que se vaya a crear sea de Tipo *Individual* se van a mostrar los campos correspondientes al mismo. 

En este módulo se adicionaron los campos, señalados en la siguiente imagen:

<img src="img_oxe360/27_pe_oxe360.png" alt="texto alternativo" >

En caso de que el contacto que se vaya a crear sea de Tipo *Compañía* se adicionaron los campos que están señalados a continuación.

<img src="img_oxe360/28_pe_oxe360.png" alt="texto alternativo" >

En la parte inferior de esta interfaz para ambos tipos de contactos se adiciono además la pestaña *Datos SUNAT*. 
El usuario para poder visualizar los datos referentes a esta pestaña debe de dar *clic* encima de la pestaña Datos SUNAT y se mostrará la siguiente información.

<img src="img_oxe360/29_pe_oxe360.png" alt="texto alternativo" >

* Se adiciono el campo *Tipo de Documento*, en el módulo **Contactos**.  Además se adicionaron el campo "Número de Documento* correspondientes a contactos de tipo *Compañía* e *Individual*. 

<img src="img_oxe360/1.2_pe_oxe360.png" alt="texto alternativo" >

Para seleccionar un *Tipo de Documento* se debe de dar *clic* en la flecha que se encuentra señalada en la siguiente imagen. 

<img src="img_oxe360/1.3_pe_oxe360.png" alt="texto alternativo" >

Se mostrara un listado con los *Tipos de Documento* que están registrados en el sistema.  Para seleccionar uno se dará *clic* encima del que se desea definir. 

<img src="img_oxe360/1.4_pe_oxe360.png" alt="texto alternativo" >

Una vez seleccionado el tipo se introducirá el *Número de Documento*, en el campo que se muestra a continuación. 

<img src="img_oxe360/1.5_pe_oxe360.png" alt="texto alternativo" >

En este módulo se cargaron los *Números de Documento* para los contactos ya existentes en el sistema.
Para visualizar esta información se ira a la vista principal de este módulo que se muestra al acceder al mismo por primera vez ; y se seleccionara uno de los contactos existentes.

<img src="img_oxe360/2_pe_oxe360.png" alt="texto alternativo" >

Al dar *clic* en el contacto seleccionado se va a mostrar la información definida para el mismo, donde se va a visualizar el *Número de Documento* que se le ha definido.

<img src="img_oxe360/3_pe_oxe360.png" alt="texto alternativo" >

* Se adiciono la funcionalidad *Distrito*

Para poder visualizar esta información, el usuario luego de acceder al módulo de **Contactos**, debe de dar *clic* a la pestaña *Configuración*.

<img src="img_oxe360/30_pe_oxe360.png" alt="texto alternativo" >

Al realizar esta operación se va a deslizar un listado de funcionalidades entre las que se muestra *Distritos*.

Al dar *clic* encima de esta funcionalidad se va a mostrar una interfaz listando los elementos y mostrando de ellos los campos que se muestran señalados.

<img src="img_oxe360/31_pe_oxe360.png" alt="texto alternativo" >

Para crear un nuevo *Distrito* , el usuario seleccionará el botón *Crear*, y se mostrara la opción de introducir los campos *Name* y *Code* manualmente.

<img src="img_oxe360/32_pe_oxe360.png" alt="texto alternativo" >

En el caso del campo *City*, este es utilizado para guardar la información de las provincias. Permitirá seleccionar una *Provincia* a partir del listado existente. Para esto se va a dar *clic* en la flecha que se encuentra al final de la línea. De la misma forma,  el campo *State* se utilizará para guardar la información de los *Departamentos* de Perú.  De esta se forma se cubre con la estructura de Ubicaciones Geográficas y códigos *UBIGEO* de Perú. 

<img src="img_oxe360/33_pe_oxe360.png" alt="texto alternativo" >

En caso de que la ciudad que se desee seleccionar no se muestre , se le da *clic* a la opción *Buscar mas...* 
<img src="img_oxe360/34_pe_oxe360.png" alt="texto alternativo" >

Mostrándose todas las ciudades registradas. 

<img src="img_oxe360/35_pe_oxe360.png" alt="texto alternativo" >

Para seleccionar del listado que se muestra, se da *clic* encima de la Provincia que se desea introducir y se cargara automáticamente en el campo *City*. 

<img src="img_oxe360/36_pe_oxe360.png" alt="texto alternativo" >

* Se adiciono la funcionalidad *Tipo de Documento*, donde se encuentran cargados los distintos *Tipos de Documento* definidos en el sistema.

Para acceder a esta funcionalidad el usuario debe de seleccionar en el Módulo **Contactos**, la pestaña *Configuración*. Al dar *clic* encima de está pestaña se va a listar un grupo de funcionalidades y entre ellas se va a mostrar la funcionalidad *Tipo de Documento*. 

<img src="img_oxe360/37_pe_oxe360.png" alt="texto alternativo" >

* Se adiciono la funcionalidad *Ciudades*, donde se encuentran registradas todas las ciudades cargadas en el sistema.
Para que se pueda visualizar el contenido adicionado, el usuario debe de acceder en el Módulo **Contactos**, a la pestaña *Configuración* . Al dar *clic* en *Configuración* se va a listar un grupo de funcionalidades , entre ellas se encuentra *Ciudades*.  

<img src="img_oxe360/38_pe_oxe360.png" alt="texto alternativo" >

Al dar *clic* a la funcionalidad *Ciudades* , se va a mostrar una interfaz con el listado de las ciudades registradas en el sistema. 

<img src="img_oxe360/39_pe_oxe360.png" alt="texto alternativo" >

Al dar *clic* en el botón *Crear* , se va a mostrar la opción de  introducir los campos para una nueva ciudad. 

<img src="img_oxe360/40_pe_oxe360.png" alt="texto alternativo" >

<img src="img_oxe360/41_pe_oxe360.png" alt="texto alternativo" >

Los campos *Nombre* y *Código Postal*, se van a introducir manualmente por el usuario. En el caso de los campos *País* y *Estado*, se debe de dar *clic* en la flecha que se encuentra al final de la barra del contenido, y se va a listar las distintos países y estados registrados en el sistema. 

<img src="img_oxe360/42_pe_oxe360.png" alt="texto alternativo" >

Todas las ciudades registradas hasta el momento en el sistema pertenecen al país Perú.. 

* Se adicionaron en el módulo de **Contabilidad** en la pestaña *Configuración*,  la pestaña *Oxe360*, la cual va a incluir las siguientes funcionalidades:

-*Taxes Group* (Grupo de Impuestos)

-*Bank Type* (Tipo de banco)

-*Bank* (Bancos)

Además se adiciono también la pestaña *Catalog Table*, perteneciente al módulo **Ajustes**.
 
Para visualizar este contenido, el usuario debe acceder en el módulo **Contabilidad**.

<img src="img_oxe360/5_pe_oxe360.png" alt="texto alternativo" >

Acceder a la pestaña *Configuración*.

<img src="img_oxe360/6_pe_oxe360.png" alt="texto alternativo" >

Dentro buscar la pestaña *Oxe360*, en la cual van a estar agrupadas estas nuevas funcionalidades.

<img src="img_oxe360/7_pe_oxe360.png" alt="texto alternativo" >

*Taxes Group* o  *Grupo de Impuestos*

Al dar *clic* a la funcionalidad *Taxes Group* se van a mostrar los distintos tipos de impuestos que se han registrados. 

<img src="img_oxe360/8_pe_oxe360.png" alt="texto alternativo" >

*Bank Type* o *Tipo de Banco*

Al dar *clic* a la funcionalidad *Bank Type* se visualizara el nombre de los *Tipos de bancos* registrados en el sistema. 

<img src="img_oxe360/9_pe_oxe360.png" alt="texto alternativo" >

Al pararnos encima de unos de los *Tipos de bancos* registrados y dar *clic* encima de él, se podrá mostrar una interfaz con el nombre  definido para el mismo. 

<img src="img_oxe360/10_pe_oxe360.png" alt="texto alternativo" >

*Bank* o *Banco*

Al dar clic a la funcionalidad *Bank* se visualizara los bancos registrados en el sistema.

<img src="img_oxe360/12_pe_oxe360.png" alt="texto alternativo" >

<img src="img_oxe360/43_pe_oxe360.png" alt="texto alternativo" >

A esta funcionalidad el usuario también puede acceder mediante el módulo **Contactos**, empleando la ruta *Contactos/Configuración/Cuentas bancarias /Bancos*.

<img src="img_oxe360/11_pe_oxe360.png" alt="texto alternativo" >

Al dar *clic* en el botón *Crear*.

<img src="img_oxe360/13_pe_oxe360.png" alt="texto alternativo" >

Se mostrará una interfaz para crear un nuevo *Banco*.  A esta funcionalidad se le adicionaron los  campos, que se muestran señalados  en la siguiente imagen. 

<img src="img_oxe360/14_pe_oxe360.png" alt="texto alternativo" >

Por cada uno de los Bancos registrados en el sistema se cargaron los datos correspondientes a estos nuevos campos señalados anteriormente. 
Para poder visualizar esta información  se debe de seleccionar el módulo **Contabilidad**. Dentro de este módulo ir a la ruta *Configuración /Oxe 360/Banks*, la cual fue mostrada anteriormente.

Al acceder a esta funcionalidad se va a mostrar una vista Lista con todos los Bancos registrados hasta el momento. 

Se va a seleccionar uno de los Bancos y se le va a dar *clic* encima del mismo.

<img src="img_oxe360/15_pe_oxe360.png" alt="texto alternativo" >

Una vez que se realice esta acción se va a mostrar la información detallada del elemento seleccionado y a la vez el contenido de los datos de los nuevos campos que se cargaron en el sistema.

<img src="img_oxe360/16_pe_oxe360.png" alt="texto alternativo" >

* Se adiciono la pestaña *Tabla de catálogo* la cual para poder ser visualizada el usuario debe de activar el *modo desarrollador*. 

Para esto se va a seleccionar el Módulo **Ajustes**.

<img src="img_oxe360/17_pe_oxe360.png" alt="texto alternativo" >

Al deslizarse hasta la última posición de la interfaz que se muestra , denominada *Herramientas desarrollo*.

<img src="img_oxe360/18_pe_oxe360.png" alt="texto alternativo" >

Damos *clic* encima de cualquiera de las opciones que se muestra. Activándose de manera automática el *modo desarrollador*.

Una vez que el usuario selecione *Activar elmodo desarrollador*, se va a seleccionar en el Módulo **Ajustes**, la pestaña  **+**, que se encuentra en la parte superior.

<img src="img_oxe360/19_pe_oxe360.png" alt="texto alternativo" >

Al realizar esta acción se van a mostrar un listado de funcionalidades. Se selecciona en la pestaña *Técnico*, la pestaña *Estructura de base de datos* y dentro se mostrará la funcionalidad *Catalog Table* o *Tabla de catálogos*.  

<img src="img_oxe360/20_pe_oxe360.png" alt="texto alternativo" >

Al dar *clic* encima de esta funcionalidad se va a mostrar el listado de los catálogos existentes en el sistema.

<img src="img_oxe360/21_pe_oxe360.png" alt="texto alternativo" >

La lista de catálogos se va a visualizar mediante los campos que encabezan la tabla, señalados en la siguiente imagen.

<img src="img_oxe360/22_pe_oxe360.png" alt="texto alternativo" >

*Parent Table*: Se refiere a la clase padre.

*Code*: Código del catálogo.

*Name*: Nombre del catálogo.

*Table Type*:  Tipo de la tabla , puede ser View o Normal.

*Company*: Compañía

Cada uno de los catálogos definidos presenta la descripción de los mismos. 

Para poder visualizar los datos de cada uno de estos catálogos, el usuario debe de dar *clic* encima del catálogo que desee visualizar y se va a mostrar la siguiente interfaz con los siguientes datos.

<img src="img_oxe360/23_pe_oxe360.png" alt="texto alternativo" >

Se van a mostrar los mismos datos que se describiendo anteriormente y se adiciona el campo Active, el cual va a aparecer marcado en caso de que el catalogo esté activado.

En caso que el *Table Type* sea de tipo *View* , el encabezado del contenido de la tabla va a ser  *Child Tables* , en caso que sea *Normal* se va a identificar con la pestaña *Table Elements*.

<img src="img_oxe360/24_pe_oxe360.png" alt="texto alternativo" >

Se visualiza también la pestaña *Descripción*, donde se va a dar una breve descripción de las características de la *Tabla  del catálogo*. 

En caso que el tipo sea *Normal*, se va a mostrar la *Tabla de elementos*, con los siguientes campos.

<img src="img_oxe360/25_pe_oxe360.png" alt="texto alternativo" >

Al acceder a uno de los elementos existentes en esta *Tabla de elementos* se va a mostrar la siguiente interfaz detallando los campos que se muestran a continuación.  Para esto el usuario debe de dar clic encima del elemento que desee visualizar, mostrándose de esta forma.

<img src="img_oxe360/26_pe_oxe360.png" alt="texto alternativo" >

A las tablas antes nombradas se les cargo la información necesaria que se visualizará al acceder a ellas.
En el caso de la *Tabla de elementos*, se cargó la información referente a SUNAT. 

* Se adicionaron campos a la pestaña *Venta y Compra*.

Para visualizar esta información el usuario debe de ir al módulo **Contactos**, y seleccionar el botón *Crear*. Una vez que se muestren los datos correspondientes para crear un nuevo contacto, en la parte inferior se van a mostrar varias pestañas, entre las que se encuentra  *Compra y Venta*.

<img src="img_oxe360/45_pe_oxe360.png" alt="texto alternativo" >

Al dar *clic* encima de esta pestaña se visualizarán los nuevos campos que se introdujeron. 

<img src="img_oxe360/44_pe_oxe360.png" alt="texto alternativo" >


* Se adiciona un nuevo campo al crear una compañía de tipo *Individual*. Este campo va a ser la Empresa a la que está asociado este nuevo contacto.

Para visualizar esta información  el usuario debe de seleccionar en el módulo **Contactos**, el botón *Crear*.
Se van a visualizar las opciones del tipo de contacto que se desea crear. 

Se va a dar clic en el tipo *Individual*.

<img src="img_oxe360/46_pe_oxe360.png" alt="texto alternativo" >

Luego de realizar esta operación se va a visualizar debajo del campo *Nombre* una línea  con una flecha al final.

<img src="img_oxe360/47_pe_oxe360.png" alt="texto alternativo" >

Al dar *clic* encima de esta flecha se va a deslizar un listado de selección donde se van a mostrar todos los contactos creados hasta el momento.

<img src="img_oxe360/48_pe_oxe360.png" alt="texto alternativo" >

Para seleccionar la empresa con la que tiene relación el nuevo contacto,  se va a dar *clic* encima de uno de los elementos mostrados, cargándose automáticamente en el campo, y con este los datos pertenecientes al mismo, como es el caso de la dirección. 

<img src="img_oxe360/49_pe_oxe360.png" alt="texto alternativo" >

* Se activó en **Localización Fiscal** el paquete *Peru - PCGE 2019 Modificado (desde 01/01/2020)*

Para visualizar esta información el usuario debe de acceder al módulo **Contabilidad**. En la pestaña *Configuración* seleccionar la funcionalidad *Ajustes*.

<img src="img_oxe360/51_pe_oxe360.png" alt="texto alternativo" >

Al dar *clic* en *Ajustes* se va a mostrar un grupo de funcionalidades. La primera que se muestra se llama *Localización Fiscal* . 

Como se muestra se encuentra cargado el paquete  *Peru - PCGE 2019 Modificado (desde 01/01/2020)*.

<img src="img_oxe360/50_pe_oxe360.png" alt="texto alternativo" >

* Se le asignaron códigos a Banco (bank), Efectivo (cash) y  Transferencia liquida (Liquidity Transfer ), limitando que la cantidad de caracteres por códigos sea 7.

Para visualizar los cambios, se debe de acceder al módulo **Contabilidad**, y en la pestaña *Configuración* , se leccionar la funcionalidad *Plan contable*.

<img src="img_oxe360/52_pe_oxe360.png" alt="texto alternativo" >

Se podrá visualizar los diferentes nombres de los planes contables con los códigos que se le han definido.

<img src="img_oxe360/53_pe_oxe360.png" alt="texto alternativo" >

<img src="img_oxe360/54_pe_oxe360.png" alt="texto alternativo" >

<img src="img_oxe360/55_pe_oxe360.png" alt="texto alternativo" >

* Se activó la moneda PEN, para las operaciones en el sistema.

Para visualizar esta opción el usuario accede al módulo **Contabilidad**, y selecciona la pestaña *Configuración*. 
Dentro de esta pestaña se selecciona la funcionalidad *Ajustes*.

Se va a deslizar las opciones que se muestran y en la denominada *Monedas* , se va a visualizar marcada la moneda PEN.

<img src="img_oxe360/56.1_pe_oxe360.png" alt="texto alternativo" >

Se va a visualizar en esta misma ruta la opción *Multi-Monedas*.

<img src="img_oxe360/56_pe_oxe360.png" alt="texto alternativo" >

Al dar clic encima de la opción de multi-monedas se va a visualizar la siguiente imagen.

<img src="img_oxe360/57_pe_oxe360.png" alt="texto alternativo" >

Al seleccionar la opción *Activar otras monedas* se va a mostrar una interfaz con las monedas activas hasta el momento y la posibilidad de activar las monedas que desea el usuario.

<img src="img_oxe360/58_pe_oxe360.png" alt="texto alternativo" >

Como se muestra la moneda PEN esta activa.

* Se adiciono la opción *Localización peruana*.

Para visualizar esta opción el usuario accede al módulo **Contabilidad**, y selecciona la pestaña *Configuración*. Dentro de esta pestaña se selecciona la funcionalidad *Ajustes*.

Se va a deslizar hasta llegar a la opción *Pagos de proveedor*, y debajo de esta opción se ha adicionado la opción *Localización peruana*.

<img src="img_oxe360/59_pe_oxe360.png" alt="texto alternativo" >

Dentro se muestra la opción *Cuenta de detracción*, la cual para seleccionar se debe de dar clic en el cuadro que aparece.   

