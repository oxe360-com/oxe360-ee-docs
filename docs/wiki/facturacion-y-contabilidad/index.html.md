---
layout: handbook-page-toc
title: "Facturación y Contabilidad"
---

-
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Configuración
Los primeros pasos para utilizar la facturación

[Configuración de Facturación](docs/wiki/facturacion-y-contabilidad/configuracion-facturacion/index.html.md) 

## Cuentas Contables

## Diarios

## Impuestos

## Libros Electrónicos Contables
Toda la información relacionada a los libros electrónicos contables que existen en la localización peruana con Oxe360

[Libros Electrónicos Contables](docs/wiki/facturacion-y-contabilidad/libros-contables/index.htm.md)

## Reportes de Contabilidad 

Los reportes de Contabilidad válidos para Perú se encuentran descritos aquí

[Reportes de Contabilidad](docs/wiki/facturacion-y-contabilidad/index.html.md)
