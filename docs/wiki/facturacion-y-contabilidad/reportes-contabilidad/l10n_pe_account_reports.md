# Módulo: l10n_pe_account_reports

Se modificó el nombre de la funcionalidad  *Estado de Situación* , mostrándose el nombre *Balance de Comprobación*. 

Se adicionaron además a la tabla que se visualiza en esta funcionalidad los campos *Situación* y *Resultado*.

Para visualizar los cambios se debe de acceder al módulo **Contabilidad**. 

<img src="img_report/l10n_pe_account_reports_0.png" alt="texto alternativo" >

Dentro se va a acceder a la pestaña *Informes*, dentro del agrupador *Informes de auditoría* se va a mostrar la funcionalidad *Balance de Comprobación*.

<img src="img_report/l10n_pe_account_reports_1.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se va a visualizar una interfaz, donde se adicionan los campos *Situación* y *Resultado*. 

<img src="img_report/l10n_pe_account_reports_2.png" alt="texto alternativo" >



