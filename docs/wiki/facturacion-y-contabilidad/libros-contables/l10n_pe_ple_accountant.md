# Módulo: l10n_pe_ple_accountant
--------

Al acceder al sistema se debe de instalar el módulo **Peru-Electronic-Books Program**.
 
<img src="img_ple/pe_ple_accountant.png" alt="texto alternativo" >

Una vez instalado se debe de seleccionar en **Ajustes** , en *Herramientas desarrollo* , la opción *Activar modo desarrollador*.

<img src="img_ple/pe_ple_accountant-0.png" alt="texto alternativo" >


Una vez activado se van a mostrar todas las funcionalidades y pestañas definidas en el sistema.

Al seleccionar el módulo **Ajustes** se va a mostrar una lista desplegable de las funcionalidades, ahí se debe de seleccionar la opción *Tabla de Catalogo*.

<img src="img_ple/pe_ple_accountant-1.png" alt="texto alternativo" >

Dentro de los datos registrados en el sistema se va a seleccionar el code *PE.Sunat.PLE_BOOKS*. 

<img src="img_ple/pe_ple_accountant-2.png" alt="texto alternativo" >

Al visualizar el contenido del mismo se va a mostrar la siguiente interfaz. 

<img src="img_ple/pe_ple_accountant-3.png" alt="texto alternativo" >

En el contenido de la misma se adicionaron las cuentas *5.1*, *5.3* y *6.1*.

* Libro Diario
* Libro Diario-Detalle del Plan Contable Utilizado
* Libro Mayor.  

<img src="img_ple/pe_ple_accountant-4.png" alt="texto alternativo" >

Para visualizar en el sistema estos libros se debe de acceder a **Ajustes**.

<img src="img_ple/pe_ple_accountant-5.png" alt="texto alternativo" >

Dentro se debe de acceder a la pestaña *Usuarios y compañías* y seleccionar el usuario que está registrado en el sistema.

<img src="img_ple/pe_ple_accountant-6.png" alt="texto alternativo" >

En la pestaña *Permisos de accesos* se debe de marcar la opción *Programa de Libros Electrónicos*.

<img src="img_ple/pe_ple_accountant-7.png" alt="texto alternativo" >

Una vez que se marque esta opción se va a seleccionar el módulo **Facturación**.

<img src="img_ple/pe_ple_accountant-8.png" alt="texto alternativo" >

Al dar clic dentro de este se va a mostrar la pestaña *SUNAT* con los libros que se han adicionado.

<img src="img_ple/pe_ple_accountant-9.png" alt="texto alternativo" >

Al acceder al menú *Configuración PLE*, se van a mostrar los libros que están configurados y al dar *clic* en cada uno de ellos se va a mostrar la configuración que se ha definido para cada uno de ellos. A partir de la información que tienen configurados será la información que se visualizará en los menús correspondientes  a cada uno de los libros.

<img src="img_ple/pe_ple_accountant-10.png" alt="texto alternativo" >

<img src="img_ple/pe_ple_accountant-11.png" alt="texto alternativo" >

<img src="img_ple/pe_ple_accountant-12.png" alt="texto alternativo" >

<img src="img_ple/pe_ple_accountant-13.png" alt="texto alternativo" >

Para visualizar la vista de cada uno de los libros se debe de dar *clic* a cada uno de ellos para que se muestre la información de los mismos. Para esto se debe de acceder a la pestaña *SUNAT* y dar *clic* al menú *5.1 Libro Diario*.

<img src="img_ple/pe_ple_accountant-14.png" alt="texto alternativo" >

Como se muestra se listan los libros ya generados.

Al dar *clic* al botón *Crear* se va a mostrar una vista con la siguiente información. En la cual se debe de definir el intervalo de las fechas de la información que se desea visualizar a partir de las facturas con los asientos contables registrados en el sistema hasta el momento.

<img src="img_ple/pe_ple_accountant-16.png" alt="texto alternativo" >

Una vez definida las fechas se debe dar *clic* al botón *Recargar*, y si existe alguna información registrada dentro del intervalo de las fechas, en la tabla *Detalles* se van a mostrar los datos registrados.

<img src="img_ple/pe_ple_accountant-15.png" alt="texto alternativo" >

Una vez que exista contenido listado en la tabla *Detalles*, se debe de dar *clic* al botón *Archivo de salida* para guardar la información.  Para esto se va a mostrar la siguiente vista con un fichero.TXT, al dar *clic* encima del mismo va a permitir al usuario guardar la información en la PC.

<img src="img_ple/pe_ple_accountant-17.png" alt="texto alternativo" >

Para confirmar la información registrada y cerrar el libro, se debe de acceder al botón *Confirmar*. 
Al dar *clic* encima del mismo se va a mostrar el siguiente mensaje para confirmar la acción a realizar.

<img src="img_ple/pe_ple_accountant-18.png" alt="texto alternativo" >

Al dar *clic* al botón *Aceptar*, el estado del libro cambia a *Confirmado* y no se puede actualizar la información del libro nuevamente.

<img src="img_ple/pe_ple_accountant-19.png" alt="texto alternativo" >

El mismo flujo se debe de realizar en los libros *5.3 Libro Diario-Detalle del Plan Contable Utilizado* y  *6.1 Libro Mayor*. 

A partir de la información que tengan configurada en  el menú *Configuración PLE*, será la información que se muestre en la tabla *Detalles* al seleccionar el botón *Recargar*. 


