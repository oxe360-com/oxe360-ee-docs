# Módulo: l10n_pe_ple_stock

* Se adicionaron nuevos campos a la funcionalidad *Tipos de operaciones*.

Para visualizar esta información, se debe de acceder la app de **Inventario**.

<img src="img_ple/pe_ple_stock_4.png" alt="texto alternativo" >


Seleccionar en la pestaña *Configuración*, en el agrupador *Gestión de almacenes*, la funcionalidad *Tipos de operaciones*.

<img src="img_ple/pe_ple_stock_5.png" alt="texto alternativo" >

Dentro de esta funcionalidad al seleccionar el botón *Crear*, se van a visualizar los campos nuevos.

<img src="img_ple/pe_ple_stock_34.png" alt="texto alternativo" >

* Se adiciono un nuevo campo en la funcionalidad *UdM* , Unidad de medida.

Para visualizar los cambios, se debe de acceder al módulo **Inventario**. Dentro se va a seleccionar en la pestaña *Configuración*, en el agrupador Unidades de medidas, la funcionalidad *UdM*. Es importante tener en cuenta que para que se muestre esta opción debe de estar activado en la pestaña Ajustes , la opción de Unidad de medidas.

<img src="img_ple/pe_ple_stock_46.png" alt="texto alternativo" >


<img src="img_ple/pe_ple_stock_29.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* , se va a visualizar una interfaz , donde se va a mostrar el nuevo campo.

<img src="img_ple/pe_ple_stock_35.png" alt="texto alternativo" >

* Se adiciono un nuevo campo a la funcionalidad *Almacenes*.

Para visualizar el cambio, se debe de acceder al módulo **Inventario**. En la pestaña *Configuración*, en el agrupador *Gestión de almacén*, se selecciona la funcionalidad *Almacenes*.

<img src="img_ple/pe_ple_stock_9.png" alt="texto alternativo" >

Al acceder a esta funcionalidad, se va a visualizar un listado de los almacenes definidos. Se debe de dar *clic* encima de uno de los almacenes y se va a visualizar la siguiente interfaz, donde se va a mostrar el nuevo campo.

Al seleccionar el botón *Editar*, va a permitir introducirle contenido a este campo. 

<img src="img_ple/pe_ple_stock_36.png" alt="texto alternativo" >

* Se adiciono el campo *SUNAT* en la funcionalidad *Producto*.

Para visualizar este campo se debe de acceder al módulo **Inventario**. 

Dentro de este módulo se debe de seleccionar la  pestaña *Datos principales*, y dentro seleccionar la funcionalidad *Productos*.

<img src="img_ple/pe_ple_stock_20.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se selecciona el botón *Crear* y se va a visualizar una interfaz con los campos, y dentro de estos el campo *SUNAT*.

<img src="img_ple/pe_ple_stock_45.png" alt="texto alternativo" >

Este mismo campo se puede visualizar, cuando se selecciona la funcionalidad *Variantes de productos*. 

<img src="img_ple/pe_ple_stock_47.png" alt="texto alternativo" >

Se selecciona el botón *Crear*, y se van a mostrar los campos de esta funfuncionalidad.

<img src="img_ple/pe_ple_stock_48.png" alt="texto alternativo" >

También se puede visualizar al acceder al módulo **Inventario**, en la pestaña *Configuración*, agrupador *Productos*, seleccionar la funcionalidad *Categoría de productos*.

<img src="img_ple/pe_ple_stock_25.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se selecciona el botón *Crear*, se va a mostrar una interfaz donde se visualiza el nuevo campo.

<img src="img_ple/pe_ple_stock_49.png" alt="texto alternativo" >


* Al instalar el módulo **l10n_pe_ple_stock** se debe de acceder al módulo **Ajustes**.

<img src="img_ple/pe_ple_stock_30.png" alt="texto alternativo" >

Dentro se debe de acceder a la pestaña *Usuarios y compañías* y dentro seleccionar el menú *Usuarios*.

<img src="img_ple/pe_ple_stock_31.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se va a mostrar el listado de los usuarios existentes en el sistema. Se debe de seleccionar el usuario que está registrado actualmente.

<img src="img_ple/pe_ple_stock_32.png" alt="texto alternativo" >

Se debe de seleccionar el botón *Editar*, para editar los *Permisos de acceso* que presenta el usuario, dentro de la *Configuración Técnica* se debe de marcar la opción **Programa de Libros Electrónicos**. Y se seleccionara el botón *Guardar*.

<img src="img_ple/pe_ple_stock_33.png" alt="texto alternativo" >

Una vez que se realice esta operación, se va a visualizar la pestaña *SUNAT* dentro del módulo **Facturación**. 


* Se visualizan campos al seleccionar el *Tipo de Reporte*: *13.1 Registro del Inventario Permanente Valorizado – Detalle del  Inventario Valorizado*. 

Para visualizar estos campos se debe de acceder al módulo **Facturación**. Seleccionar en la pestaña *SUNAT* la funcionalidad *Configuración PLE*.

<img src="img_ple/pe_ple_stock_41.png" alt="texto alternativo" >

Al acceder a esta funcionalidad, se debe de seleccionar el botón *Crear*, y se mostrará una interfaz con los campos de esta funcionalidad. 

Al introducir en el *Tipo de Reporte* el elemento *13.1 Registro del Inventario Permanente Valorizado – Detalle del Inventario Valorizado*, se van a visualizar los siguientes campos.

<img src="img_ple/pe_ple_stock_42.png" alt="texto alternativo" >


* Se adiciono la funcionalidad *12.1 Registro de Inventario - Unidades Físicas con sus campos*.

Para visualizar los cambios se debe de acceder al módulo **Facturación**, en la pestaña *SUNAT* , dentro del agrupador *Programa de Libros Electrónicos*, se va a visualizar la funcionalidad *12.1 Registro de Inventario - Unidades Físicas*.

<img src="img_ple/pe_ple_stock_13.png" alt="texto alternativo" >

Al acceder a esta funcionalidad si no se ha creado ningún registro se va a visualizar la siguiente interfaz, con el siguiente contenido para guiar al usuario. 

<img src="img_ple/pe_ple_stock_43.png" alt="texto alternativo" >

Al darle *clic* en el botón *Crear* se va a mostrar una interfaz con los campos y botones.

<img src="img_ple/pe_ple_stock_50.png" alt="texto alternativo" >

Al introducir los datos de los campos , se selecciona el botón *Guardar* , y al seleccionar el botón *Recargar*.

<img src="img_ple/pe_ple_stock_57.png" alt="texto alternativo" >

El estado del registro pasa a Validado y se va a llenar la tabla con los datos de los detalles de las cuentas, de las fechas que se han introducido.

<img src="img_ple/pe_ple_stock_59.png" alt="texto alternativo" >

Al seleccionar el botón *Guardar y nuevo* se va a visualizar la siguiente vista , con un fichero que se puede descargar para la pc.

<img src="img_ple/pe_ple_stock_58.png" alt="texto alternativo" >

Al seleccionar el botón *Confirmar* se va a mostrar el siguiente mensaje para que el usuario confirme que desea confirmar la operación.

<img src="img_ple/pe_ple_stock_60.png" alt="texto alternativo" >

Una vez que el usuario de clic en el botón *Aceptar* de este mensaje , el estado del registro pasara para *Confirmado*

<img src="img_ple/pe_ple_stock_61.png" alt="texto alternativo" >

* Se adiciono la funcionalidad *13.1 Registro de Inventario – Valorizado*, con sus campos.

Para visualizar los cambios se debe de acceder a al módulo **Facturación**, en la pestaña *SUNAT*, dentro del agrupador *Programa de Libros Electrónicos*, se va a visualizar la funcionalidad *13.1 Registro de Inventario – Valorizado*.

<img src="img_ple/pe_ple_stock_14.png" alt="texto alternativo" >

Al acceder a esta funcionalidad si no existe ningún elemento registrado se va a mostrar el siguiente mensaje, guiando al usuario en la funcionalidad.

<img src="img_ple/pe_ple_stock_44.png" alt="texto alternativo" >

Al seleccionar el botón *Crear* se van a visualizar los siguientes campos y botones.

<img src="img_ple/pe_ple_stock_51.png" alt="texto alternativo" >

Al introducir los datos de los campos , se selecciona el botón Guardar , y al seleccionar el botón *Recargar*.

<img src="img_ple/pe_ple_stock_62.png" alt="texto alternativo" >

El estado del registro va a pasar a Validado y se va a llenar la tabla con los datos de los detalles de las cuentas, de las fechas que se han introducido.

<img src="img_ple/pe_ple_stock_53.png" alt="texto alternativo" >

Al seleccionar el botón *Guardar y nuevo* se va a visualizar la siguiente vista , con un fichero que se puede descargar para la pc.

<img src="img_ple/pe_ple_stock_54.png" alt="texto alternativo" >

Al seleccionar el botón *Confirmar* se va a mostrar el siguiente mensaje para que el usuario confirme que desea confirmar la operación.

<img src="img_ple/pe_ple_stock_55.png" alt="texto alternativo" >

Una vez que el usuario de clic en el botón *Aceptar* de este mensaje , el estado del registro pasara para *Confirmado*

<img src="img_ple/pe_ple_stock_56.png" alt="texto alternativo" >

