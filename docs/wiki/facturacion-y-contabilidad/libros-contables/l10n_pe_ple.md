# Módulo: pe_ple

* Se realizó una carga de datos en la *Tabla de catálogos*.

Para visualizar esta información se debe de acceder al módulo **Ajustes**.

<img src="img_ple/pe_ple_2.png" alt="texto alternativo" >

Dentro seleccionar en la pestaña *Herramientas desarrollo*, una de las opciones que existen para activar el modo desarrollador. 

<img src="img_ple/pe_ple_3.png" alt="texto alternativo" >

Una vez activado el modo desarrollador, se debe dar *clic* en el mismo módulo **Ajustes**, al **+**  que se visualiza, mostrando el resto de las funcionalidades. Dentro se va a seleccionar en la pestaña *Técnico*, en el agrupador *Estructura de base de datos*, la funcionalidad *Tabla de catálogos*.

<img src="img_ple/pe_ple_1.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se visualiza un listado de tablas con los catálogos. Se debe dar clic en la tabla con código PE_PLE. 

<img src="img_ple/pe_ple_5.png" alt="texto alternativo" >

Al dar *clic* dentro de esta tabla en la pestaña *Sub tablas*, se va a visualizar el contenido de las tablas que se han adicionado.

<img src="img_ple/pe_ple_4.png" alt="texto alternativo" >

* Se adiciono contenido a las tablas con código *PE.SUNAT.PLE.BOOKS*, *PE_SUNAT_FILENAME 2829*, *PE_SUNAT_FILENAME 3030*, *PE_SUNAT_FILENAME 3131*, *PE_SUNAT_FILENAME 3232*, y a todas las tablas *PE_SUNAT_PLE_TABLE* desde la *01* hasta la *35*.

Para visualizar esta información se debe de acceder al módulo **Ajustes**. Dentro se va a seleccionar en la pestaña *Técnico*, en el agrupador *Estructura de base de datos*, la funcionalidad *Tabla de catálogos*.

<img src="img_ple/pe_ple_1.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se va a visualizar el listado de las tablas existentes en el sistema. 

<img src="img_ple/pe_ple_8.png" alt="texto alternativo" >

Del listado de tablas existentes se le cargo contenido a las tablas antes nombradas y que están marcadas a continuación.

<img src="img_ple/pe_ple_7.png" alt="texto alternativo" >

<img src="img_ple/pe_ple_6.png" alt="texto alternativo" >

Para visualizar el contenido adicionado, de debe de dar *clic* encima de cada una de las tablas marcadas y se mostrará la interfaz correspondiente a la tabla. 

<img src="img_ple/pe_ple_9.png" alt="texto alternativo" >

Como se muestra anteriormente el contenido introducido se muestra en la pestaña *Elementos de tablas*.

<img src="img_ple/pe_ple_10.png" alt="texto alternativo" >

Esta misma operación se debe de realizar con las tablas 
 
* PE.SUNAT.PLE_FILENAME 2829
* PE.SUNAT.PLE _FILENAME 3030
* PE.SUNAT.PLE _FILENAME 3131
* PE.SUNAT.PLE _FILENAME 3232
* PE.SUNAT.PLE _TABLE_01
* PE.SUNAT.PLE _TABLE_02
* PE.SUNAT.PLE _TABLE_03
* PE.SUNAT.PLE _TABLE_04
* PE.SUNAT.PLE _TABLE_05
* PE.SUNAT.PLE _TABLE_06
* PE.SUNAT.PLE _TABLE_10
* PE.SUNAT.PLE _TABLE_11
* PE.SUNAT.PLE _TABLE_12
* PE.SUNAT.PLE _TABLE_13
* PE.SUNAT.PLE _TABLE_14
* PE.SUNAT.PLE _TABLE_15
* PE.SUNAT.PLE _TABLE_16
* PE.SUNAT.PLE _TABLE_17
* PE.SUNAT.PLE _TABLE_18
* PE.SUNAT.PLE _TABLE_19
* PE.SUNAT.PLE _TABLE_20
* PE.SUNAT.PLE _TABLE_21
* PE.SUNAT.PLE _TABLE_22
* PE.SUNAT.PLE _TABLE_25
* PE.SUNAT.PLE _TABLE_27
* PE.SUNAT.PLE _TABLE_28
* PE.SUNAT.PLE _TABLE_30
* PE.SUNAT.PLE _TABLE_31
* PE.SUNAT.PLE _TABLE_32
* PE.SUNAT.PLE _TABLE_33
* PE.SUNAT.PLE _TABLE_34
* PE.SUNAT.PLE _TABLE_35

* Se cargaron nuevos impuestos en la funcionalidad *Impuestos*.

Para visualizar este contenido, se debe de acceder al módulo **Contabilidad**.

<img src="img_ple/pe_ple_12.png" alt="texto alternativo" >

Dentro de este módulo, en la pestaña  *Configuración*, en el agrupador *Facturación/Contabilidad, se debe de seleccionar la funcionalidad *Impuestos*.

<img src="img_ple/pe_ple_13.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se visualizan los impuestos que se han adicionado con sus detalles.

<img src="img_ple/pe_ple_11.png" alt="texto alternativo" >

Para saber los detalles de cada uno de estos impuestos se debe de dar *clic* encima de ellos, y se visualizará la información de los mismos.

* Se adiciono un elemento a la funcionalidad *Grupos*, denominado  *Programa de Libros Electrónicos*.

Para visualizar este contenido se debe de acceder al módulo **Ajustes**, en la pestaña *Usuarios y compañías*, seleccionar la funcionalidad *Grupos*.

<img src="img_ple/pe_ple_15.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se visualiza un listado de grupos creados, dentro se encuentra marcado el nuevo elemento.

<img src="img_ple/pe_ple_14.png" alt="texto alternativo" >

Al dar *clic* en este elemento se va a visualizar los datos específicos del mismo.

<img src="img_ple/pe_ple_16.png" alt="texto alternativo" >

* Se adicionaron nuevos permisos.

Se adiciono en la vista de usuarios la opción *Programa de Libros Electrónicos*. 
Para visualizar esta opción se debe de acceder al módulo **Ajustes**. En la pestaña *Usuarios y Compañías*, la funcionalidad *Usuarios*.

<img src="img_ple/pe_ple_18.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se debe de seleccionar el usuario con el que se está trabajando actualmente en el sistema.

<img src="img_ple/pe_ple_19.png" alt="texto alternativo" >

Al dar *clic* a este usuario se va a mostrar una interfaz con todos los permisos del mismo.

En el encabezado *Contabilidad* se debe de seleccionar la opción *Facturación*.
En el encabezado *Configuración Técnica* se debe de seleccionar la opción *Programa de Libros Electrónicos*.

<img src="img_ple/pe_ple_17.png" alt="texto alternativo" >

Una vez que se haya seleccionado esta opción se va a visualizar el módulo **Facturación/Contabilidad**.

<img src="img_ple/pe_ple_20.png" alt="texto alternativo" >

* Se adiciono la funcionalidad *SUNAT PLE Configuration* , con sus campos.

Para visualizar esta funcionalidad se debe de acceder al módulo **Facturación /Contabilidad**.

Seleccionar la pestaña *SUNAT*, dentro se visualiza la funcionalidad *SUNAT PLE Configuration*.

<img src="img_ple/pe_ple_21.png" alt="texto alternativo" >

Al acceder a la funcionalidad, se va a visualizar el listado de los elementos registrados.
Al seleccionar el botón *Crear*, se va a visualizar una interfaz con los campos correspondientes a esta funcionalidad.

<img src="img_ple/pe_ple_22.png" alt="texto alternativo" >

En el campo *Tipo de reporte*, al seleccionar los libros con numeración *5.1, 5.3, 6.1, 12.1, 13.1*,no se van a visualizar las tablas de *Diarios* y *Cuentas* , al seleccionar los libros con numeración *8.1,8.2,14.1* , se va a visualizar la tabla de los *Diarios* , pero no la de las *Cuentas*.

<img src="img_ple/pe_ple_23.png" alt="texto alternativo" >
<img src="img_ple/pe_ple_24.png" alt="texto alternativo" >

