# Módulo : l10n_pe_ple_invoicing

* Se incorporaron los siguientes tipos de reportes en el menú *Configuración Ple*:

-*8.1 - Registro de Compras*.

-*8.2 - Registro de Compras - Información de Operaciones Con Sujetos No Domiciliados*.

-*14.1 - Registro de Ventas E Ingresos*.

A cada uno de estos reportes se le definieron los elementos que los componen.

Para visualizar esta información se debe de activar la opción *Programa de Libros Electrónicos*.

Para esto se debe de acceder al módulo **Ajustes** y dentro del mismo seleccionar en la pestaña *Usuarios y compañías*, en el menú *Usuarios*, el usuario que está registrado en el sistema.

Al acceder a las características del mismo en la pestaña *Permisos de accesos* se va a visualizar la opción *Programa de Libros Electrónicos*. Este se va a marcar y se le dará *clic* al botón *Guardar*. 

Una vez que se actualice el sistema se debe de acceder al módulo **Facturación**.

<img src="img_ple/pe_ple_invoicing_1.png" alt="texto alternativo" >


Dentro de este módulo se debe dar *clic* a la pestaña *SUNAT* y dentro se va a mostrar el menú *Configuración PLE*.

<img src="img_ple/pe_ple_invoicing_2.png" alt="texto alternativo" >

Al acceder a este menú se van a visualizar los nuevos registros.

<img src="img_ple/pe_ple_invoicing_3.png" alt="texto alternativo" >

Al dar *clic* a cada uno de estos registros, se van a visualizar los datos que se les definieron.

**8.1 - Registro de Compras**

<img src="img_ple/pe_ple_invoicing_4.png" alt="texto alternativo" >

**8.2 - Registro de Compras - Información de Operaciones Con Sujetos No Domiciliados**

<img src="img_ple/pe_ple_invoicing_5.png" alt="texto alternativo" >

**14.1 - Registro de Ventas E Ingresos**

<img src="img_ple/pe_ple_invoicing_6.png" alt="texto alternativo" >

* Por cada uno de los registros definidos se va a visualizar un menú, en el cual se va a mostrar la información correspondiente al mismo a partir de los datos cargados en el menú *Configuración PLE*. 

Para visualizar esta información, dentro de la pestaña *SUNAT* en el agrupador  *Programas de Libros Electrónicos*, se van a mostrar los menús de estos reportes.

<img src="img_ple/pe_ple_invoicing_7.png" alt="texto alternativo" >

Al acceder al menú *8.1 Compras* , se va a mostrar los libros con los registros cargados hasta el momento.

<img src="img_ple/pe_ple_invoicing_8.png" alt="texto alternativo" >

Al acceder al botón *Crear* se va a mostrar una vista con los campos a introducir. 

<img src="img_ple/pe_ple_invoicing_9.png" alt="texto alternativo" >

Una vez que el usuario introduzca un intervalo de *fecha*, se le dará *clic* al botón *Guardar*. 

Cuando esté guardada la información, al seleccionar el botón *Recargar*, la tabla *Detalles* tomará valores a partir de la información de los *Asientos contables* registrados en el sistema.

Al mostrarse el listado de los elementos, se puede dar clic al botón *Guardar el archivo*, el cual va a visualizar una vista con la siguiente información:

<img src="img_ple/pe_ple_invoicing_10.png" alt="texto alternativo" >

Permitiendo de esta forma al usuario descargar la información que se ha generado.

Al seleccionar el botón *Cerrar* se va a cerrar la vista, mostrada.

En caso que no se haya cargado ninguna información en la tabla *Detalles*, y se le dé *clic*  al botón *Guardar* el archivo, se va a mostrar el siguiente mensaje.

<img src="img_ple/pe_ple_invoicing_16.png" alt="texto alternativo" >

Al seleccionar el botón *Confirmar* se va a mostrar el siguiente mensaje para confirmar la operación.

<img src="img_ple/pe_ple_invoicing_11.png" alt="texto alternativo" >

Al seleccionar el botón *Aceptar* el registro pasa a estado *Confirmado*.

<img src="img_ple/pe_ple_invoicing_12.png" alt="texto alternativo" >

Lo registros  *8.2 - Registro de Compras - Información de Operaciones Con Sujetos No Domiciliados* y *14.1 - Registro de Ventas E Ingresos* funcionan de la misma manera. Lo que los diferencia son los *Diarios* que se les definieron en la *Configuración PLE*. 

A partir de los *Tipos de diarios* que se les definieron, será la información que se visualizará al acceder al botón *Recargar* en cada uno de ellos.

* Se le adiciona el campo *Código SUNAT* al crear un nuevo país.

Para visualizar esta información, se debe de acceder al módulo **Contacto**.

<img src="img_ple/pe_ple_invoicing_13.png" alt="texto alternativo" >

Dentro de este módulo, en la pestaña *Configuración*, dentro del agrupador *Localización*, se va a visualizar el menú *Países*.

<img src="img_ple/pe_ple_invoicing_14.png" alt="texto alternativo" >

Al acceder a este menú, se va a seleccionar el botón *Crear* y se va a visualizar la siguiente vista, donde se va a mostrar el campo que se le ha adicionado a esta funcionalidad.

<img src="img_ple/pe_ple_invoicing_15.png" alt="texto alternativo" >

