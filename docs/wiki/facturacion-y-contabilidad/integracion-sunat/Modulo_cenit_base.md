## Módulo Cenit Integration

El módulo Cenit es una plataforma social como servicio para la integración de datos y negocios. Permite la automatización del proceso operativo, conectando aplicaciones heredadas y servicios de Internet.
Al instalar por primera vez este módulo se debe de buscar en la opción Aplicaciones de la pantalla de inicio y buscar en la pestaña superior el módulo con el nombre Cenit IO Integrations Client. 

<img src="img_cenit/Cenit-inicio.jpg" alt="texto alternativo" >

Luego de seleccionar el botón instalar, se nos mostrara una interfaz con los campos correspondientes para configurar una cuenta en Cenit.
<img src="img_cenit/Cenit-inicio-1.jpg" alt="texto alternativo" >


Una vez que nos autenticamos recibiremos un correo a la dirección de correo con la que nos hemos autenticado y se instalara el modulo satisfactoriamente. 

<img src="img_cenit/Cenit-1.png" alt="texto alternativo" >


Al acceder a este módulo en la parte superior se muestran las pestañas Collections, Data, API Connectors , Workflows y Settings
Se nos muestra como página de inicio, donde se identificaran los espacios de los nombres o Namespaces como está definido en el sistema. Como se muestra se pueden Crear nuevos espacios, Importar para el sistema y Exportar los ya existentes, guardándolos en un documento de tipo excel.

<img src="img_cenit/Cenit-2.png" alt="texto alternativo" >

Para acceder a esta funcionalidad también lo podemos hacer por medio de la ruta Collections/Namespaces

Para crear un nuevo Namespaces, selecciono la opción Crear y se muestra una interfaz, con los campos Nombre y Slug o identificador, pertenecientes al nuevo espacio que se desea introducir. Además se mostraran  los botones Guardar o Descartar. 

<img src="img_cenit/Cenit-4.png" alt="texto alternativo" >

Una vez que el usuario decida seleccionar la opción Guardar se va a mostrar una vista previa de los campos introducidos, con dos opciones Editar y Crear, permitiendo editar el que se acaba de crear o crear otro nuevo espacio, lo mismo ocurre en todas las funcionalidades existentes en este módulo de Odoo.
Los datos introducidos se mostraran en la vista Lista de la funcionalidad. 

<img src="img_cenit/Cenit-5.png" alt="texto alternativo" >
### Data
En la parte superior al seleccionar la opción Data, se muestran las funcionalidades Data Type, Mapping e Import Mapping.  

* Data Type
------------

Al seleccionar la funcionalidad Data Type , o Tipo de dato en español , se mostrara la siguiente interfaz donde se almacenara la lista de los datos ya introducidos en el sistema.

<img src="img_cenit/Cenit-6.png" alt="texto alternativo" >

Al seleccionar la opción Crear permitirá crear un nuevo Tipo de dato, con los campos definidos para el mismo. Al seleccionar el campo Namespaces me permite deslizar una lista de elementos ya definidos en el sistema o crear un nuevo elemento. En el campo Schema o esquema se introducirá la estructura y las restricciones que estén estrechamente relacionados con el Tipo de dato que se va a definir. 

<img src="img_cenit/Cenit-7.png" alt="texto alternativo" >

* Mapping
---------

Al seleccionar la opción Mapping o mapeo nos mostrara una interfaz con los elementos introducidos en el sistema, de estos se mostraran el Namespaces, el Nombre y el Model o modelo. 

<img src="img_cenit/Cenit-8.png" alt="texto alternativo" >
Al seleccionar la opción Crear se mostrara una interfaz con los campos definidos para esta funcionalidad. Como se muestra muchos de los campos son de selección lo cual nos permitirá seleccionar un elemento ya definido anteriormente en el sistema o crear uno nuevo.

<img src="img_cenit/Cenit-9.png" alt="texto alternativo" >

Entre los campos se encuentra el campo Model que como se muestra a continuación permitirá seleccionar algunos de los modelos ya definidos en el sistema y el campo Enable que permitirá activar, el cual por defecto viene seleccionado. 

<img src="img_cenit/Cenit-10.png" alt="texto alternativo" >

Esta funcionalidad presenta un grupo de pestañas que nos permitirán agrupar los datos necesarios en el sistema.

-Mapping 

<img src="img_cenit/Cenit-11.png" alt="texto alternativo" >

Uno de los requerimientos que presentan los campos a introducir en esta pestaña es que el campo Typo en dependencia del tipo de elemento que se le seleccione se activaran el resto de los otros campos para poder seleccionar o introducir el contenido.
Si se selecciona Field se activara el campo Primary.
Si se selecciona Model se activan los campos Reference y Cardinality. 
Si se selecciona Reference se activa el campo Cardinality.
Si se selecciona Default se desactivan todos y solo se queda activado Inlined, el cual siempre está activado por defecto. Lo mismo ocurre cuando se selecciona Python code.

<img src="img_cenit/Cenit-12.png" alt="texto alternativo" >

-Triggers

En la pestaña Triggers o disparador en español se agruparan los datos de los diferentes Eventos. 

<img src="img_cenit/Cenit-13.png" alt="texto alternativo" >

Para activar el resto de los campos se debe de seleccionar en el campo Eventos el evento On interval permitiendo introducirle los datos al resto de los campos. 

<img src="img_cenit/Cenit-14.png" alt="texto alternativo" >

<img src="img_cenit/Cenit-15.png" alt="texto alternativo" >

-Conditions

<img src="img_cenit/Cenit-16.png" alt="texto alternativo" >

En la pestaña Conditions o condiciones, se muestran los campos definidos para Field que no es más que el espacio  definido para el almacenamiento de un dato en particular. 
Para esto se define la Condición y el Valor del mismo. 

<img src="img_cenit/Cenit-17.png" alt="texto alternativo" >

* Import Mapping 
-------------------

Esta funcionalidad nos permite guardar el Mapping que se ha realizado hasta el momento y Exportar uno existente. 
Al seleccionar el botón Guardar permitirá Editar y Crear una nueva
Al seleccionar el botón Exportar permitirá seleccionar uno existe. 

<img src="img_cenit/Cenit-18.png" alt="texto alternativo" >

### API Connector

En la parte superior al seleccionar la opción API Connector, se muestran las funcionalidades Connection, Connections Roles, Webhooks, Operations y Resources

<img src="img_cenit/Cenit-19.png" alt="texto alternativo" >

* Connection 
---------------
Esta funcionalidad nos permitirá Crear, Importar y Exportar la información. 

<img src="img_cenit/Cenit-20.png" alt="texto alternativo" >

Para importar un archivo se selecciona el botón Importar, mostrándonos una nueva interfaz con el botón Cargar Fichero el cual nos va a permitir seleccionar de la PC el documento que deseamos cargar en el sistema. 

<img src="img_cenit/Cenit-21.png" alt="texto alternativo" >

Para crear una nueva conexión, seleccionamos el botón Crear y se nos mostrara una interfaz con los campos necesarios, además de varias pestañas que permitirán agrupar los datos relacionados con las mismas. En el campo URL se debe de poner la url correspondiente al nuevo elemento que estamos creando.

<img src="img_cenit/Cenit-22.png" alt="texto alternativo" >

-Parameters 

Se introducirá una key (llave) y un valor para la misma. 

<img src="img_cenit/Cenit-23.png" alt="texto alternativo" >

-Header parameters

<img src="img_cenit/Cenit-24.png" alt="texto alternativo" >

-Template parameters

<img src="img_cenit/Cenit-25.png" alt="texto alternativo" >

* Connections Roles
------------------

En esta funcionalidad se van a crear los roles, para esto se deben de introducir los campos que se muestran en la interfaz que se genera al seleccionar esta funcionalidad, la cual nos va a permitir Crear , Importar y Exportar un rol ya existente. 

<img src="img_cenit/Cenit-26.png" alt="texto alternativo" >

Se muestran las siguientes pestañas que agruparan los datos necesarios para crear un nuevo rol.

-Connections

Se va seleccionar la opción Agregar línea y se mostrara una interfaz con las url que ya están definidas en el sistema se selecciona y se cargaran la información automáticamente.  

<img src="img_cenit/Cenit-27.png" alt="texto alternativo" >

<img src="img_cenit/Cenit-27.1.png" alt="texto alternativo" >

-Webhooks

Aquí se registra la información deseada que luego se enviara. 

<img src="img_cenit/Cenit-28.png" alt="texto alternativo" >

* Webhooks
-----------------

<img src="img_cenit/Cenit-29.png" alt="texto alternativo" >

Para crear un Webhooks se van a introducir los datos necesarios para definir el funcionamiento de un Namespaces;  además se puede Importar la información y exportar el contenido. Se va a seleccionar el Namespaces y el Method que por defecto se muestra de tipo Post. 

<img src="img_cenit/Cenit-30.png" alt="texto alternativo" >

De igual forma se van a mostrar las pestañas Parameters,  Header parameters y Template parameters para las cuales se van a introducir los campos Key y Value.

* Operations 
---------------

<img src="img_cenit/Cenit-31.png" alt="texto alternativo" >

Para crear una nueva Operation se debe de seleccionar el botón Crear y se mostraran los campos correspondientes para esto. 

<img src="img_cenit/Cenit-32.png" alt="texto alternativo" >

En el campo Resource se introducirá el nombre del recurso que se definirá en la funcionalidad Resource (API Connector/ Resource).
El campo Purpose cargara un valor en dependencia del que se haya seleccionado en el campo Method.

<img src="img_cenit/Cenit-33.png" alt="texto alternativo" >

* Resources
--------------

<img src="img_cenit/Cenit-34.png" alt="texto alternativo" >

Como se muestra permite Crear, Importar un recurso ya existente y Exportar los recursos registrados en el sistema. Para realizar cualquiera de estas opciones se selecciona el botón correspondiente para la acción que se desea realizar.
Para crear un nuevo Resources se selecciona la opción Crear y se mostraran los campos pertenecientes a esta funcionalidad 

<img src="img_cenit/Cenit-35.png" alt="texto alternativo" >

Como se muestra en la imagen existen campos que permiten seleccionar contenido ya existente en el sistema o crear contenido nuevo como es el caso de Namespaces y Operations.
Se muestran las pestañas Parameters,  Header parameters y Template parameters para las cuales se van a introducir los campos Key y Value.

### Workflows

En la parte superior al seleccionar la opción Workflows  , se muestran las funcionalidades Flows , Events y Translators
 
* Flows  o flujo 
------------------

<img src="img_cenit/Cenit-36.png" alt="texto alternativo" >

Para crear un nuevo Flujo, se selecciona la opción Crear y salen los campos que se deben de registrar para crear el flujo. Además de los campos que son para seleccionar un dato ya existente, están Enabled y Discard events que nos permiten seleccionar si se va a activar y si va a descartar el evento o no. 

<img src="img_cenit/Cenit-37.png" alt="texto alternativo" >

<img src="img_cenit/Cenit-38.png" alt="texto alternativo" >

<img src="img_cenit/Cenit-39.png" alt="texto alternativo" >

* Events
-------------
Para acceder a esta funcionalidad se debe seleccionar la ruta Workflows/ Events 

<img src="img_cenit/Cenit-40.png" alt="texto alternativo" >

Para crear un nuevo Evento, se selecciona la opción Crear y salen los campos que se deben registrar para crear el Evento. Se introduce el nombre del Evento en el campo Name y se selecciona el  contenido de cada uno de los otros campos puesto que son de selección y se me muestran datos que ya se han registrados en el sistema.

<img src="img_cenit/Cenit-41.png" alt="texto alternativo" >

<img src="img_cenit/Cenit-42.png" alt="texto alternativo" >

* Translators
--------------


<img src="img_cenit/Cenit-43.png" alt="texto alternativo" >

En esta funcionalidad me permite exportar los datos registrados en el sistema hasta el momento en un archivo de tipo excel. En este se muestran todos los datos almacenados en el sistema y se muestran los campos con los valores que se muestran en la interfaz

<img src="img_cenit/Cenit-44.png" alt="texto alternativo" >

<img src="img_cenit/Cenit-45.png" alt="texto alternativo" >

### Settings

En la parte superior al seleccionar la opción Settings o Configuración,se va a permitir realizar configuraciones de las opciones del sistema. Para esto tiene definida la funcionalidad Hub Settings o Configuraciones de hub 

* Hub Settings

<img src="img_cenit/Cenit-46.png" alt="texto alternativo" >

Como se muestra nos va a permitir configurar la conexión. Para esto se debe de introducir la url que el usuario tiene definida del Cenit, la User key  o llave del usuario y el User token o identificador del usuario. 

Una vez introducidos estos datos se debe de seleccionar con cuáles de todas las API que se muestran, va a estar integrada. 

<img src="img_cenit/Cenit-47.png" alt="texto alternativo" >



