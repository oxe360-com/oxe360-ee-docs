# Módulo: Docunexo_Sunat

Para poder verificar el funcionamiento de esta aplicación deben de estar instaladas las siguientes aplicaciones en el orden que se muestra a continuación:

* Peru Account Invoicing  ( l10n_pe_account_invoicing )

<img src="img_sunat/docunexo_1.png" alt="texto alternativo" >

* Cenit IO Integrations Client ( cenit_base )

<img src="img_sunat/docunexo_2.png" alt="texto alternativo" >

* SUNAT Integration ( docunexo_sunat )

<img src="img_sunat/docunexo_3.png" alt="texto alternativo" >

Para poder instalar estas aplicaciones se debe de acceder a **Aplicaciones** y buscar estas aplicaciones e instalarlas en el mismo orden en que se mostraron anteriormente.

Al intentar instalar la app **Cenit IO Integrations Client**, se le va a pedir al usuario los datos  correspondientes al usuario del *Cenit* que emplean. 

<img src="img_sunat/docunexo_4.png" alt="texto alternativo" >

Para esto se debe de acceder a la dirección *www.cenit.io*. El usuario debe de estar registrado en este sistema. 

Una vez que se registre, en la pestaña correspondiente a los *Tenants*, se va a seleccionar en la que se va a estar trabajando y se va a mostrar la siguiente vista. 

<img src="img_sunat/docunexo_5.png" alt="texto alternativo" >

Se van a mostrar los campos *Name*, *Key* y *Token* , el valor que se muestren en estos campos son los que se deben de introducir al instalar la app de **Cenit**. 

<img src="img_sunat/docunexo_6.png" alt="texto alternativo" >

Para introducir estos datos en la vista que se muestra al intentar instalar la app **Cenit** se debe de seleccionar el botón *Skip*.

<img src="img_sunat/docunexo_7.png" alt="texto alternativo" >

Se va a mostrar la siguiente vista, donde se van a introducir los valores que se obtuvieron  anteriormente en el sistema *Cenit*. 

<img src="img_sunat/docunexo_8.png" alt="texto alternativo" >

Una vez que el usuario introduzca los valores correspondientes, se va a seleccionar el botón *APPLY* y  se va a instar la aplicación satisfactoriamente. 

Es importante destacar que para que esto no lance ningún error la app de **Letra de cambio**, no debe de estar instalado anteriormente. 

Luego de instalar esta app, el usuario pasara a instalar la app **SUNAT Integration**.

Una vez que se hayan instalado todas las app, el usuario debe de seleccionar **Ajustes**.

<img src="img_sunat/docunexo_9.png" alt="texto alternativo" >

En la pestaña *Usuarios y compañías*, se va a seleccionar el menú *Usuarios*. 

<img src="img_sunat/docunexo_10.png" alt="texto alternativo" >

En este se va a seleccionar el usuario registrado en el sistema y se seleccionará el botón *Editar*. 

En la pestaña *Permisos de acceso* se va a seleccionar la opción *Mostrar características de contabilidad completas*.

<img src="img_sunat/docunexo_11.png" alt="texto alternativo" >

Una vez que se seleccione esta opción en las facturas que se creen por el usuario se va a visualizar el campo *Diario*, permitiendo que el usuario pueda seleccionar las facturas de tipo electrónicas. 

Para verificar el funcionamiento de la app **SUNAT Integration**, se va a seleccionar la app **Facturación**, y dentro del mismo en las pestañas *Cliente* o *Proveedor* se van a crear facturas de tipo electrónicas.  Para mostrar el funcionamiento en la pestaña *Cliente* se va a seleccionar el menú *Factura*. 

<img src="img_sunat/docunexo_14.png" alt="texto alternativo" >

Al acceder a este menú se va a crear una nueva factura. Se debe de tener en cuenta que en el campo *Cliente*, el tipo de documento  del cliente que se va introducir debe de ser *RUC*; y en el campo *Diario* debe de ser una factura de tipo electrónica.

<img src="img_sunat/docunexo_12.png" alt="texto alternativo" >

Al *Guardar* y *Publicar* la factura, el campo *Estado PSE*, toma el estado  de *Enviando*.

Al acceder a la plataforma de *Cenit*, mediante la dirección *www.cenit.io* se va a visualizar en la pestaña de *Notificaciones*, los registros que se generan cuando es enviada la factura por *Docunexo* hacia la *SUNAT*.

<img src="img_sunat/docunexo_16.png" alt="texto alternativo" >

En la vista de la factura que se ha creado, se puede visualizar en la parte inferior los distintos estados por los que pasa la factura.

<img src="img_sunat/docunexo_15.png" alt="texto alternativo" >

Una vez que la factura es enviada el campo *Estado PSE*, toma el valor de *Enviado*. 

<img src="img_sunat/docunexo_17.png" alt="texto alternativo" >

