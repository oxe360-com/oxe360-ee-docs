# Modulo Contabilidad

Este módulo está completamente integrado en la gestión de la empresa permitiendo automatizar sus procesos y logrando  ahorrar mucho trabajo administrativo. Va a agrupar todas las operaciones contables que se realizan en las empresas, estableciendo un control y una adecuada gestión de los mismos. 

Al acceder al sistema se muestra una interfaz rápida y moderna que nos permite mediante graficas tener una idea de cómo se encuentra la empresa hasta el momento. Nos permite crear Facturas del cliente, Facturas del proveedor, Varias operaciones, Realizar conciliaciones con el banco y Nuevas transacciones.

En la parte superior se muestran las pestañas Información general, Clientes, Proveedores, Contabilidad, Informes y Configuración.

<img src="img/cont-1.png" alt="texto alternativo" >

Al seleccionar la pestaña Información general se muestra un Resumen Contable de todo lo referido a la contabilidad de la empresa. 

Se muestran en la interfaz  un tablero con elementos correspondientes a Customer invoices (Ventas); Vendor Bills (Compras); Miscellaneous Operations (Varios); Bank (Banco); Cash (Efectivo) y Point of Sale (Ventas). Por cada uno de estos elementos se mostrara un botón que nos va a permitir realizar diferentes operaciones con los mismos, como es el caso de Crear facturas, Cargar facturas, Nuevas transacciones , Nuevas entradas, todas están en dependencia de la operación contable que se desee registrar. Estas operaciones se encuentran detalladas en las diferentes pestañas que se muestran en el sistema, en dependencia del tipo de información que se desee registrar.

## Cliente 
-----------
Al seleccionar la opción Cliente se va a desglosar las diferentes funcionalidades relacionadas con los mismos, las cuales son Facturas, Facturas rectificativas, Recepciones, Pagos, Informes de seguimiento, Productos y Clientes.  Cada una de estas funcionalidades presentan las opciones Crear, Importar o Cargar y Exportar. 

<img src="img/flecha exportar.jpg" alt="texto alternativo" >


En la parte inferior de las interfaces se va mostrando los registros de las acciones que se van realizando en el sistema.  Una vez que se seleccione el nombre del Cliente este se convierte en seguidor de la factura a la que está asociado. 

Al seleccionar el botón Crear se va a mostrar una interfaz con los datos necesarios para poder registrar la operación. 

Al seleccionar la opción Importar se nos mostrara una interfaz con dos botones Cargar fichero y Cancelar. Al dar clic al botón Cargar fichero permitirá seleccionar el fichero desde el ordenador, en caso de seleccionar la opción Cancelar se cancela la acción. 

El botón Cargar es similar al Importar, solo que al seleccionarlo se me muestra automáticamente la opción de buscar desde el ordenador.

Al seleccionar la opción Exportar, se generar un documento.xlsx, es decir de tipo excel, donde se van a mostrar todos los datos registrados hasta el momento en la funcionalidad donde se está operando. 

Se van a mostrar también en la vista Lista de las funcionalidades las opciones Imprimir y Acción, que a su vez engloban distintas opciones que se pueden realizar en dependencia de la operación o documentación que se esté registrando. 

<img src="img/cont-14.png" alt="texto alternativo" >

<img src="img/cont-16.png" alt="texto alternativo" >

* Facturas

Una factura es un documento que justifica la transacción de un servicio o producto entre una empresa y un consumidor o entre dos empresas. Se trata de un justificante fiscal que debe incluir la información sobre la compraventa y es requisito imprescindible para la posible deducción del Impuesto sobre el Valor Añadido (IVA).

Al acceder a esta funcionalidad se muestra una interfaz con el listado de las Facturas registradas por parte del cliente.

<img src="img/cont-2.png" alt="texto alternativo" >

Al seleccionar la opción Crear se mostrara una interfaz con los datos necesarios para registrar una Factura perteneciente al cliente.

<img src="img/cont-3.png" alt="texto alternativo" >

Existen campos que son de selección, permitiendole al usuario seleccionar uno ya registrado en el sistema o crear uno nuevo, como es el caso de los campos Cliente, Plazo de pago y Diario.

En el campo de Fecha factura se va a mostrar un calendario para poder seleccionar la fecha en que se está realizando la Factura. Se recomienda que la fecha que se seleccione sea la actual, pero en caso de ser otra no puede ser posterior a la fecha en que se está realizando la operación. 

En caso del campo Compañía  el sistema va a cargar de forma automática la compañía registrada actualmente en el sistema, y con el que se autentica el usuario que está registrado. Esto puede ser modificado manualmente. 

Además de estos campos se muestran unas pestañas que van a englobar un grupo de datos necesarios para detallar la Factura, como son Líneas de factura, Apuntes contables y  Otra información.

-Líneas de factura

Presenta las opciones de Agregar un registro, Agregar una sección y Agregar una nota. 
En caso de Agregar un registro se deben de introducir los campos que se muestran,con los que se va a detallar el producto:

Producto: Se va a seleccionar un producto ya registrado en el sistema o crear uno nuevo.

Descripción: Se va a cargar la descripción definida para el producto que se seleccione para facturar al cliente. 

Cuenta: Se debe de introducir la cuenta a la que se va a cargar el producto, en este caso se va a generar una vez que se seleccione agregar un registro, tomando la cuenta del cliente al que se le está realizando la factura. 

Cantidad: Por defecto carga 1, este valor se va a modificar en dependencia de la cantidad de productos que se van a facturar. 

Precio: Este campo va a tomar el valor ya definido para el producto que se va a facturar, es decir una vez que se seleccione el producto este campo va a tomar valor automáticamente. 

Impuesto: Este campo es el impuesto que se le va a aplicar a la factura, coge por defecto el 18%,  que es el que está configurado en el sistema, pero se puede modificar, seleccionar o crear un nuevo impuesto. 

Subtotal: Es el valor de la multiplicación de la cantidad registrada del producto que se desea facturar y el valor del mismo.

Agregar una sección me va a permitir introducir el nombre de la sección que yo desee que salga en la factura, al igual que con la opción Agregar nota.

<img src="img/cont-4.png" alt="texto alternativo" >

-Apuntes contables

En este acápite se van a cargar desglosados los datos del producto con su valor y el impuesto que se le va a aplicar. Como se muestra el campo Debe y Haber la suma del valor de ambos debe de de ser igual para que cuadre la información que se ha introducido. 

En caso de seleccionar la opción Agregar línea, se debe de verificar que la información que se introduzca tenga la cuenta requerida con los datos correspondientes al Debe y en otra línea el valor de la cuenta Haber, las cuales igual al sumarse deben de cuadrar, en caso contrario el sistema lanzara un mensaje de error, impidiendo que se pueda guardar la factura. 

<img src="img/cont-5.png" alt="texto alternativo" >

-Otra información

En esta interfaz se van a generar los datos de la Factura, Pagos y Contabilidad.  Como se muestra en el campo Vendedor se va a cargar automáticamente el nombre del usuario que está registrando la factura, así como la Cuenta bancaria del mismo, como referencia del pago. El campo Incoterm se puede seleccionar de los ya existentes en el sistema. 

<img src="img/cont-6.png" alt="texto alternativo" >

Al introducir los datos en la parte superior, seleccionamos la opción Guardar, para guardar la factura. Hasta ese momento la factura está en estado Borrador.

En la parte inferior a los botones Guardar y Descartar se muestran los botones Publicar y Previsualizar. Al seleccionar la opción Publicar la factura cambia para el estado Publicado y se muestran los botones Enviar e imprimir, Registrar pago, Previsualizar, Agregar factura rectificativa y Cambiar a borrador. 

<img src="img/cont-7.png" alt="texto alternativo" >

En la parte inferior, según los cambios  que se vayan realizando, se va a ir mostrando un registro de los distintos estados por los que pasa la factura.

Al seleccionar la opción Registrar pago, se va a mostrar una interfaz con los datos necesarios para efectuar esta operación. Para eso se muestran los campos:

Diario: Se muestra un valor por defecto del tipo de moneda con el que se va a pagar la factura

Importe: El monto a pagar, ya definido a partir de los datos que se han introducido al confeccionar la factura.

Fecha: Ese valor toma la fecha en que se va a registrar la factura.

Circular: Va a ser el numero con el que se va a identificar la factura.

<img src="img/cont-8.png" alt="texto alternativo" >

Al seleccionar Validar la factura pasa al estado de Pagado.

<img src="img/cont-8.png" alt="texto alternativo" >

Al seleccionar el botón Cambiar a borrador, la factura volvería nuevamente al estado Borrador.

Al seleccionar el botón Previsualizar, se muestra cómo quedaría la factura hasta el momento.

<img src="img/cont-12.0.png" alt="texto alternativo" >

<img src="img/cont-12.png" alt="texto alternativo" >

Al seleccionar Agregar factura rectificativa el sistema muestra una interfaz donde se introducirán los datos necesarios para agregarla la factura a factura rectificativa.

<img src="img/cont-18.png" alt="texto alternativo" >

Al seleccionar Enviar e imprimir el sistema permite enviar un correo al usuario especificando el estado de la factura y los datos necesarios de la misma, y a su vez permite imprimir la factura que se está registrando mostrando un documento.pdf.  

<img src="img/cont-19.png" alt="texto alternativo" >

* Facturas rectificativas 

Una factura rectificativa es un documento que detalla alguna corrección en la factura ordinaria o bien la devolución de la mercancía.

Al seleccionar esta opción se muestra en una vista Lista con los datos registrados hasta el momento. 

Permite Crear nuevas facturas rectificativas, Importar o Cargar una ya existente y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" >  (siempre que existan datos registrados para esta operación en el sistema).

<img src="img/cont-10.png" alt="texto alternativo" >

Al seleccionar la opción Crear se muestra una interfaz con los datos necesarios para registrar esta operación, los cueles son similares a cuando se va a crear una nueva factura.

<img src="img/cont-11.png" alt="texto alternativo" >

Los datos creados se pueden ver en la vista Lista, así como el estado en que se encuentran actualmente las facturas registradas.


* Recepciones

El informe de recepción es un formato elaborado con la finalidad de especificar la cantidad y clase de materiales recibidos. Este formato sirve para crear un registro de las entradas de materiales provenientes de proveedores.

Al seleccionar esta opción se muestra en una vista Lista con los datos registrados hasta el momento. 

Permite Crear nuevas recepciones, Importar o Cargar algún recibo existente y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > .

Al seleccionar la opción Crear se muestra una interfaz con los datos necesarios para registrar un Recibo de venta. Como se muestra, los datos son similares a los de la funcionalidad Factura, manteniendo las pestañas Líneas de factura y Apuntes contables. 

<img src="img/cont-20.png" alt="texto alternativo" >

Una vez que se guarda el Recibo de venta, se puede seleccionar la opción Publicar y mostrando una interfaz con los botones para realizar las operaciones de Registrar pago y Cambiar a borrador, según lo que desee hacer el usuario.

<img src="img/cont-20.png" alt="texto alternativo" >

* Pagos

Es la transacción (monetaria o no) por la que se extingue una deuda.

Al seleccionar esta opción se muestra en una vista Lista con los datos registrados hasta el momento. 

Permite Crear nuevos pagos, Importar un archivo ya existente y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > .

<img src="img/cont-22.png" alt="texto alternativo" >

El estado de las facturas que se muestran en esta interfaz y a las que se les va a realizar el pago, están en estado Validado. 

Al seleccionar la opción Crear se muestra una interfaz con los datos necesarios para realizar esta operación. Para esto se debe de especificar el Tipo de pago. Según el dato que se seleccione serán los datos que se deban de introducir. Por defecto en el sistema sale marcada la opción de Recibir dinero, en caso que se seleccione la opción Enviar dinero se mostrara un nuevo campo llamado Método de pago, de tipo selección donde se debe de seleccionar si va a ser Manual o Cheques, en caso de que sea Cheque se mostrara un campo para introducir el Número de cheque. Si se marca Transferencia interna se visualizara un campo denominado Transferir a, donde se seleccionara si es Bank o Cash (Banco o efectivo), y los campos Tipo de empresa y Socio se pondrán invisibles. 

Además se debe de introducir el Importe, la Fecha en la que se va realizar el pago, tomando por defecto la fecha actual,  y  el Circular que es el número que se va a mostrar para identificar el documento de pago. 

<img src="img/cont-23.png" alt="texto alternativo" >

Al seleccionar Confirmar, el usuario muestra una interfaz con los datos registrados y pasa automáticamente al estado Validado. Además se muestra la opción Restablecer a borrador, lo que nos permitirá modificar los datos que sean necesarios antes de estar seguros de querer seguir con la operación que se está registrando.

En la parte superior se muestra además la opción Apuntes contables y Asignación de pagos, según la operación que se registró. Al dar clic en la opción Apuntes contables se va a mostrar los apuntes contables pertenecientes a la compañía que se introdujo en los datos, y que corresponden a la fecha registrada, pertitiendo exportar estos datos a un fichero de tipo excel. 

<img src="img/cont-25.png" alt="texto alternativo" >

Al registrar los datos se muestran las opciones Imprimir cheque y Restablecer a borrador. Al seleccionar la opción Imprimir cheque se muestra una interfaz donde se introducirá el campo No del próximo cheque y se mandara a Imprimir o Cancelar según desee el usuario. 


Al seleccionar la opción Imprimir se muestra el modelo del pago que se registró y el sistema pasa a estado Enviado.

* Informes de seguimiento

En esta interfaz se mostraran los documentos que tienen seguimiento hasta el momento. Los cuales se pondrán exportar a un documento en formato de tipo Excel. 

<img src="img/cont-25.2.png " alt="texto alternativo" >

* Productos

En esta funcionalidad se van a registrar todos los productos que se ofertan hasta el momento. Para lo cual se puede Crear productos nuevos, Importar o Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" >. 

<img src="img/cont-26.png" alt="texto alternativo" >

Al seleccionar la opción Crear se muestran los datos necesarios para que se registre un Producto en el sistema. 

<img src="img/cont-27.png" alt="texto alternativo" >

En la parte superior derecha se muestra una opción Trazabilidad, que va a mostrar la trazabilidad completa de las operaciones de inventario del producto en que se está trabajando.   

Para la descripción detallada del Producto se muestran las pestañas Información General, Variantes, Ventas, Punto de Venta, Compra, Inventario y Contabilidad.

-Información general

<img src="img/cont-28.png" alt="texto alternativo" >

En el caso del campo Tipo de producto, si se selecciona de tipo Almacenable se mostraran en la parte superior opciones para mostrar e introducir los detalles de  la Trazabilidad, A mano, Previsto, Reglas de abastecimiento, y Lote de número de serie. Si se selecciona Servicio no saldrá ninguna opción y si se elige Consumible solo se mostrara la Trazabilidad del mismo.  

-Variantes

En dependencia del Atributo que se seleccione serán los valores que se mostraran. 

<img src="img/cont-29.png" alt="texto alternativo" >

-Ventas

<img src="img/cont-30.png" alt="texto alternativo" >

-Punto de Venta

<img src="img/cont-31.png" alt="texto alternativo" >

-Compra 

<img src="img/cont-32.png" alt="texto alternativo" >

-Inventario

<img src="img/cont-33.png" alt="texto alternativo" >

-Contabilidad 

<img src="img/cont-34.png" alt="texto alternativo" >

Según el Tipo de producto que se seleccionó se mostraran los botones Configurar variantes, Actualizar la cantidad y Reabastecer. 

* Clientes

En esta funcionalidad se van a mostrar los Clientes existentes hasta el momento. Permite Crear nuevos e Importar uno ya existente.

<img src="img/cont-35.png" alt="texto alternativo" >

Al seleccionar la opción Crear se muestran los datos necesarios para registrar un cliente nuevo.

<img src="img/cont-36.png" alt="texto alternativo" >

En la parte superior se muestra los datos propios del cliente como es el caso de las Oportunidades, las Reuniones que tienen planificadas, lo Facturado hasta el momento y el Libro mayor de empresa. 

Además se van a mostrar las pestañas Contactos y direcciones, Ventas y comprar, Contabilidad y Notas internas, con el objetivo de detallar otros datos del nuevo cliente. 

-Contactos y direcciones

En esta pestaña se muestra la opción Añadir, que al darle clic se muestra una interfaz con los datos necesarios. 

<img src="img/cont-37.png" alt="texto alternativo" >

-Venta y compra 

<img src="img/cont-38.png" alt="texto alternativo" >

-Contabilidad

<img src="img/cont-39.png" alt="texto alternativo" >

-Notas Internas

Aquí se pueden describir las notas que se deseen. 

## Proveedor 
-----------

Al seleccionar la opción Proveedor  se va a desglosar las diferentes funcionalidades relacionadas con los mismos, las cuales son Facturas, Facturas rectificativas, Recepciones, Pagos, Productos y Proveedores.

Cada una de estas funcionalidades presentan las opciones Crear, Importar o Cargar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > .

Son similares a las de los Clientes antes descritas, solo que estas están enfocadas a los Proveedores. 

En la parte inferior de las interfaces se va mostrando los registros de las acciones que se van realizando en el sistema.  Una vez que se seleccione el nombre del Proveedor este se convierte en seguidor de la factura a la que está asociado. 

Al seleccionar el botón Crear se va a mostrar una interfaz con los datos necesarios para poder registrar la operación. 

Al seleccionar la opción Importar se muestra una interfaz con dos botones Cargar fichero y Cancelar. Al dar clic al botón Cargar Fichero permitirá seleccionar el fichero desde el ordenador, en caso de seleccionar la opción Cancelar se cancela la acción. 

Al dar clic sobre la opción de Exportar, se me va a generar un documento.xlsx, es decir de tipo Excel, donde se van a mostrar todos los datos registrados hasta el momento en la funcionalidad donde se está operando. 

Se van a mostrar también en la vista Lista de las funcionalidades las opciones Imprimir y Acción, que a su vez engloban distintas opciones que se pueden realizar en dependencia de la operación o documentación que se está registrando. 

* Factura

Crear factura

<img src="img/cont-40-FacturaProve.png" alt="texto alternativo" >

En el caso de la facturas para el proveedor, en la pestaña Otra información el contenido va enfocado al proveedor.

<img src="img/cont-41.png" alt="texto alternativo" >

<img src="img/cont-42.png" alt="texto alternativo" >

Una vez creada la Factura del proveedor se muestran los botones que me permiten realizar diferentes operaciones:

-Registrar Pago

<img src="img/cont-43.png" alt="texto alternativo" >

<img src="img/cont-44.png" alt="texto alternativo" >


* Factura rectificativa

Crear Factura rectificativa

<img src="img/factura-rectificativa.png" alt="texto alternativo" >

Luego de selecionar la opción Previsualizar y volver al menu, se muestra el botón Cancelar asiento.

<img src="img/facRec-2.png" alt="texto alternativo" >

* Recepciones

Al seleccionar la opción Crear se muestra una interfaz con los datos necesarios para registrar un Recibo de venta.

<img src="img/cont-45.2.png" alt="texto alternativo" >

Luego de introducidos los datos me permite Registrar pago o Cambiar a borrador. Al seleccionar la opción Registrar pago, la factura se muestra como Pagada.

<img src="img/cont45.3.png" alt="texto alternativo" >

* Pagos

Al acceder a la interfaz se muestra una vista Lista con los datos , y al seleccionar la opción Crear se muestra una interfaz con los datos correspondientes a esta operación relacionada con el Proveedor.

<img src="img/cont-47.png" alt="texto alternativo" >

<img src="img/cont-48.png" alt="texto alternativo" >

* Productos

Al acceder a la interfaz se muestra una vista Lista con los datos, y al seleccionar la opción Crear se muestra una interfaz con los datos correspondientes a esta operación relacionada con el Proveedor.

<img src="img/cont-49.png" alt="texto alternativo" >

<img src="img/cont-50.png" alt="texto alternativo" >

* Proveedor

En esta funcionalidad se van a mostrar los Proveedores existentes hasta el momento. Permite Crear nuevos e Importar  uno ya existente.

<img src="img/cont-51.png" alt="texto alternativo" >

Los datos son los mismos que los de Crear un Cliente , antes descrito.

## Contabilidad 
-----------

Al seleccionar la opción Contabilidad  se van a desglosar las diferentes operaciones asociadas a ella, las cuales son Asientos contables, ventas, Compras, Banco y caja, Varios, Libro mayor, Libro mayor de empresa, Transferencias automáticas, Ingresos diferidos, Gastos diferidos, Conciliación y Fechas de bloque. Al acceder a cada una de ellas se muestran las opciones Crear e Importar 

Varios
-----

* Asientos contables

Al acceder a esta funcionalidad se muestra una vista listando los elementos registrados en el sistema. Al seleccionar la opción Crear genera una interfaz para introducir los datos para registrar un Asiento contable.

Al seleccionar la opción Importar se muestra una interfaz con dos botones Cargar Fichero y Cancelar. Al dar clic al botón Cargar Fichero permitirá seleccionar el fichero desde el ordenador, en caso de seleccionar la opción Cancelar se cancela la acción. 

<img src="img/cont-52.png" alt="texto alternativo" >

El sistema por defecto en el campo Fecha contable, toma la fecha en que se está registrando la operación, toma como moneda a emplear USD y carga la compañía a la que pertenece el usuario que está registrado en el sistema, de igual forma estos campos pueden ser modificados manualmente por el usuario. 

Además muestra las pestañas Apuntes contables y Otra información.

-Apuntes contables

Al seleccionar la opción Agregar línea se pueden introducir los datos de las cuentas a las que se les va a realizar el Asiento contable. Se puede cargar del sistema datos ya existentes como la Cuenta que se va a aplicar que pueden ser Bancaria, Transferencia liquida, entre otras y el Socio o compañía al que se le va a asignar. 

En el campo Cuadrículas de impuestos se puede también seleccionar un dato existente o crear uno nuevo, para el cual le introduciríamos los datos Nombre de etiqueta con el que se va a representar y Aplicación, que puede ser Cuenta e Impuesto. Al seleccionar Impuesto se debe de especificar si va a tener Saldo fiscal negativo, el País , y se le debe de asignar alguna Línea de informe de impuestos.

<img src="img/cont-54.png" alt="texto alternativo" >

Es importante que una vez que se le asignen los datos de Debe y Haber estos cuadren, es decir se deben de introducir 2 líneas una con el valor del Haber y otra con él del Debe, y estas a su vez la suma final de cada uno de estos datos debe de ser el mismo. 

<img src="img/cont-53.png" alt="texto alternativo" >

-Otra información

En esta pestaña se mostrara el nombre de la Compañía y permitirá seleccionar si se va a publicar y si se va a revisar. 

<img src="img/cont-55.png" alt="texto alternativo" >

Los Asientos contables van a pasar por dos estados Borrador y Publicado. El estado Borrador lo toma desde el momento inicial en que se le introducen los datos y se guardan. Al seleccionar el botón Guardar se muestran las opciones de Publicar, Duplicar o Cancelar asiento. Al seleccionar la opción Publicar el asiento pasa a estado Publicado y se me muestra la otra opción de Asiento de reversión. 

<img src="img/cont-56.png" alt="texto alternativo" >

<img src="img/cont-57.png" alt="texto alternativo" >


Diarios
------

* Ventas

En esta interfaz se van a listar las Ventas realizadas hasta el momento. Esta información puede ser guardada en un documento de tipo Excel al seleccionar la opción de Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > . 

<img src="img/cont-58.png" alt="texto alternativo" >

* Compras

En esta interfaz se van a listar las Compras realizadas hasta el momento. Esta información puede ser guardada en un documento de tipo Excel al seleccionar la opción de Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" >. 

<img src="img/cont-59.png" alt="texto alternativo" >

* Banco y Caja

En esta interfaz se van a listar las operaciones de tipo Banco y Caja realizados hasta el momento. Esta información puede ser guardada en un documento de tipo Excel al seleccionar la opción de Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" >. 

<img src="img/cont-60.png" alt="texto alternativo" >

* Varios

En esta interfaz se van a listar las operaciones de tipo Varios realizados hasta el momento. Esta información puede ser guardada en un documento de tipo Excel al seleccionar la opción de Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" >. 

<img src="img/cont-61.png" alt="texto alternativo" >

Libros mayores
-------

* Libro mayor

En esta interfaz se van a mostrar agrupadas todas las operaciones; por el tipo de cuenta que se le registro al crear el Asiento contable; realizadas en el sistema, generando un valor total del impuesto registrado tanto por el haber como por él debe. Esta información puede ser guardada en un documento de tipo Excel al seleccionar la opción de Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" >. 

<img src="img/cont-62.png" alt="texto alternativo" >


* Libro mayor de empresa

En esta interfaz se van a mostrar agrupadas todas las operaciones; por el Socio al que se le asigno, al crear el Asiento Contable; realizadas en el sistema, generando un valor total del impuesto registrado tanto por el haber como por él debe. Esta información puede ser guardada en un documento de tipo Excel al seleccionar la opción de Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" >. 

<img src="img/cont-63.png" alt="texto alternativo" >

Administración
--------

* Transferencias automáticas

Al acceder a esta funcionalidad se muestra una vista listando los elementos registrados en el sistema. Al seleccionar la opción Crear genera una interfaz para introducir los datos para registrar una Transferencia automática.

Al seleccionar la opción Importar se muestra una interfaz con dos botones Cargar fichero y Cancelar. Al dar clic al botón Cargar fichero permitirá seleccionar el fichero desde el ordenador, en caso de seleccionar la opción Cancelar se cancela la acción.

Al dar clic sobre la opción de exportar, se me va a generar un documento.xlsx, es decir de tipo Excel, donde se van a mostrar todos los datos registrados hasta el momento en la funcionalidad donde se está operando. 

<img src="img/cont-64.png" alt="texto alternativo" >

Para registrar la Transferencia automática se va a definir el Periodo de cuando a cuando se va a realizar y la frecuencia con la que se va  verificar. Así como los datos de la Cuenta de origen, el % y el tipo de cuenta de destino. 

<img src="img/cont-65.png" alt="texto alternativo" >

Una vez que se guardan los datos se muestra un botón Activar, para activar la Transferencia automática que se ha registrado, permitiendo cambiar el estado de la misma de Deshabilitado a En proceso. 

Una vez que se selecciona la opción Activar va a permitir Calcular la Transferencia y al darle clic al botón Calcular transferencia se va a registrar la Transferencia realizada en la opción Transferencia que se muestra en la parte superior derecha. Al darle clic se va a mostrar una interfaz listando los datos de la transferencia activada.  

<img src="img/cont-66.png" alt="texto alternativo" >

<img src="img/cont-67.png" alt="texto alternativo" >

* Activos

Al acceder a esta funcionalidad se muestra una vista listando los elementos registrados en el sistema. Al seleccionar la opción Crear genera una interfaz para introducir los datos para registrar un Activo.

Al seleccionar la opción Importar se muestra una interfaz con dos botones Cargar fichero y Cancelar. Al dar clic al botón Cargar fichero permitirá seleccionar el fichero desde el ordenador, en caso de seleccionar la opción Cancelar se cancela la acción.

Al dar clic sobre la opción de Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" >, se me va a generar un documento.xlsx, es decir de tipo Excel, donde se van a mostrar todos los datos registrados hasta el momento en la funcionalidad donde se está operando. 

Entre los datos que se deben de introducir para crear un Activo está el campo Método, en dependencia al tipo de método que se seleccione serán los detalles que se deberán de especificar. 

<img src="img/cont-68.png" alt="texto alternativo" >

<img src="img/cont-69.png" alt="texto alternativo" >


Al seleccionar la opción Confirmar, el Activo pasara para el estado En proceso y se van a generar los botones que se muestran  a continuación.

<img src="img/cont-70.png" alt="texto alternativo" >

Al seleccionar el botón Vendido o disponible se muestra una interfaz especificando la Acción, que puede ser Vender o Disponible, y la Factura del cliente.

<img src="img/cont-71.png" alt="texto alternativo" >

<img src="img/cont-72.png" alt="texto alternativo" >

Al seleccionar la opción Vender el activo pasa al estado Cerrado, y se me muestran las opciones de Establecer en ejecución y Guardar modelo. 

<img src="img/cont-73.png" alt="texto alternativo" >


Al seleccionar el botón Guardar modelo se genera una interfaz con los datos ya definidos para este Activo, pero con el campo Nombre del modelo para registrarlo en el sistema. 

<img src="img/cont-74.png" alt="texto alternativo" >

Luego de registrados los datos del Activo y guardarlos si selecciono el botón Calcular amortizaciones, se me va a generar una tabla de amortizaciones, según la cantidad de años que haya definido en el campo duración.

<img src="img/cont-75.png" alt="texto alternativo" >

<img src="img/cont-76.png" alt="texto alternativo" >

Luego de realizada la Amortización y validar el Activo se me van a mostrar las siguientes opciones a realizar.

<img src="img/cont-77.png" alt="texto alternativo" >

* Ingresos diferidos

Al acceder a esta funcionalidad se muestra una vista listando los elementos registrados en el sistema. Al seleccionar la opción Crear genera una interfaz para introducir los datos para registrar un Ingreso diferido.

Al seleccionar la opción Importar se muestra una interfaz con dos botones Cargar fichero y Cancelar. Al dar clic al botón Cargar fichero permitirá seleccionar el fichero desde el ordenador, en caso de seleccionar la opción Cancelar se cancela la acción. 

Si en la vista Lista existen datos ya registrados se va a mostrar una opción Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > , que me va a generar un documento.xlsx, es decir de tipo Excel, donde se van a mostrar todos los datos registrados hasta el momento en la funcionalidad donde se está operando. 

<img src="img/cont-78.png" alt="texto alternativo" >

El campo Primera fecha de reconocimiento toma el valor de la fecha actual en que se está registrando el ingreso. Luego de introducidos todos los datos se puede Confirmar la operación o  Calcular el ingreso. Al seleccionar la opción Calcular ingreso el sistema va a mostrar un Tablero de ingresos a partir de los números de reconocimientos definidos y la frecuencia.

<img src="img/cont-79.png" alt="texto alternativo" >

Al seleccionar la opción Confirmar el ingreso pasa al estado En proceso y se me muestran otras opciones a realizar.

<img src="img/cont-80.png" alt="texto alternativo" >

* Gastos diferidos

Al acceder a esta funcionalidad se muestra una vista listando los elementos registrados en el sistema. Al seleccionar la opción Crear genera una interfaz para introducir los datos para registrar un Gasto diferido.

Al seleccionar la opción Importar se muestra una interfaz con dos botones Cargar fichero y Cancelar. Al dar clic al botón Cargar fichero permitirá seleccionar el fichero desde el ordenador, en caso de seleccionar la opción Cancelar se cancela la acción. 

Si en la vista Lista existen datos ya registrados se va a mostrar una opción exportar, que me va a generar un documento.xlsx, es decir de tipo excel, donde se van a mostrar todos los datos registrados hasta el momento en la funcionalidad donde se está operando. 

Para crear un Gasto diferido se va a introducir los datos correspondientes al mismo.

<img src="img/cont-81.png" alt="texto alternativo" >

En la parte superior se muestran las opciones Confirmar y Calcular diferido. Al seleccionar la opción Calcular diferido se muestra un Tablero de gastos desglosando el valor original.

<img src="img/cont-82.png" alt="texto alternativo" >

Al seleccionar la opción Confirmar el ingreso pasa a estado En proceso y se muestran las opciones Establecer a borrador, Modificar gastos y Guardar modelo, los cuales son similares a otras funcionalidades que se han descrito anteriormente. 

<img src="img/cont-83.png" alt="texto alternativo" >

Al seleccionar la opción Modificar gastos se va a mostrar una interfaz con los campos que se desean cambiar y el Motivo de la modificación. 

<img src="img/cont-84.png" alt="texto alternativo" >

Acciones
-------

* Conciliación

Esta funcionalidad corresponde a una Acción contable del sistema y deben de mostrar las conciliaciones pendientes que se han generado al realizar las distintas operaciones en el sistema.

<img src="img/cont-84.1.png" alt="texto alternativo" >

En caso de que no se muestre ninguna pendiente, porque los saldos están cuadrados, se puede acceder a la opción Validar que se describe en el contenido que se muestra en el sistema y nos va a mostrar distintas operaciones para verificar.

Al seleccionar la opción Validar se nos va a mostrar las distintas operaciones bancarias que se han registrado y la posibilidad de validarlas. De igual forma nos permite verificar las operaciones Facturas sin pago, Facturas de proveedores que no se han pagado aun y los Apuntes existentes sin conciliar.

<img src="img/cont-85.png" alt="texto alternativo" >

<img src="img/cont-86-factura.png" alt="texto alternativo" >

* Fechas de bloqueo

Esta funcionalidad corresponde a una Acción contable del sistema y me va a mostrar una interfaz que me permitirá introducir los datos necesarios para Bloquear el periodo fiscal. Para esto se definen las fechas y se selecciona la opción  Guardar. 

<img src="img/cont-87.png" alt="texto alternativo" >

## Informes 
-----------

En esta funcionalidad se van a agrupar todos los informes necesarios que se generan a partir del módulo Contabilidad de una empresa. 

Para esto se van a agrupar en Declaraciones genéricas, Informes de empresas, Informes de auditoría y Administración. 

Declaraciones genéricas
---------

* Ganancias y pérdidas 

Se va a mostrar una vista detallada de los Ingresos y los Gastos generados en la empresa hasta el momento. Va a permitirle al usuario Visualizar la información en formato pdf mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información. 

<img src="img/cont-88.png" alt="texto alternativo" >

Al seleccionar la opción Guardar se va a mostrar una interfaz donde se va a especificar el formato en que se desea guardar la información en el sistema, puede ser PDF o XLSX.

<img src="img/cont-89.png" alt="texto alternativo" >

Luego de esto se va a guardar en una interfaz denominada Documentos generados.

* Balance de situación

Se va a mostrar una vista detallada de los Activos y los Pasivos generados en la empresa hasta el momento. Va a permitirle al usuario Visualizar la información en formato pdf , mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información. 

<img src="img/cont-90.png" alt="texto alternativo" >

* Resumen ejecutivo

Se va a mostrar un Resumen ejecutivo que incluye los gastos y ganancias referentes a los elementos Efectivo,Rentabilidad, Balance de situación, Rendimiento y Cargo, generados en la empresa hasta el momento. Va a permitirle al usuario visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información. 

<img src="img/cont-91.png" alt="texto alternativo" >

* Estado de flujo de efectivo

Se va a mostrar la información detallada de los flujos de efectivo generados en la empresa hasta el momento. Va a permitirle al usuario visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información. 

<img src="img/cont-92.png" alt="texto alternativo" >

* Check register 

Se va a mostrar un registro de los distintos tipos de cheques, reflejando donde y cuando se han depositados los cheques generados en la empresa hasta el momento. Va a permitirle al usuario visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información. 

<img src="img/cont-93.png" alt="texto alternativo" >

Informes de empresa
-------

* Libro mayor de empresa

Se va a mostrar un registro en el que están registrados cada una de las cuentas contables generados en la empresa hasta el momento. Va a permitirle al usuario visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información. 

<img src="img/cont-94.png" alt="texto alternativo" >

* Vencida por cobrar

Se va a mostrar un registro en el que están registrados cada una de las cuentas contables vencidas por cobrar generados en la empresa hasta el momento. Va a permitirle al usuario visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información.

<img src="img/cont-95.png" alt="texto alternativo" >

* Vencida por pagar

Se va a mostrar un registro en el que están registrados cada una de las cuentas contables vencidas por pagar generados en la empresa hasta el momento. Va a permitirle al usuario Visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información.

Informes de auditoría 
---------

* Libro mayor

Se va a mostrar el registro de todas las operaciones económicas registradas en las distintas cuentas contables de la empresa de manera cronológica, generados en la empresa hasta el momento. Va a permitirle al usuario visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información.

<img src="img/cont-96.png" alt="texto alternativo" >

* Balance de comprobación

Se va a visualizar la lista del total de los débitos y de los créditos de las cuentas, junto al saldo de cada una de ellas (ya sea deudor o acreedor). De esta forma, permite establecer un resumen básico del estado financiero generado en la empresa hasta el momento. Va a permitirle al usuario Visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información.

<img src="img/cont-97.png" alt="texto alternativo" >

* Diarios consolidados

Se muestra la información agrupada por cuentas contables, los hechos económicos de una empresa, en el que se registran todas las transacciones realizadas en la empresa hasta el momento. Va a permitirle al usuario visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel y Guardar la información.

<img src="img/cont-98.png" alt="texto alternativo" >


* Reportes impuestos

Se muestran los impuestos generados en la empresa hasta el momento. Va a permitirle al usuario visualizar la información en formato pdf, mediante una Vista previa de impresión, Exportar a un documento de tipo excel, Guardar la información y Closing Journal Entry (Entrada de Diario de Cierres). 

<img src="img/cont-99.png" alt="texto alternativo" >

Al seleccionar la opción Closing Journal Entry se muestra una interfaz detallando el impuesto que estaba listado, permitiendo Publicarlo, Duplicar o Cancela asiento. 

<img src="img/cont-100.png" alt="texto alternativo" >

* Auditorías de libros

Al seleccionar esta opción se muestra una interfaz que va a permitir realizarle una Auditoria a los libros existentes en la empresa. El sistema va a cargar por defecto todos los diarios registrados hasta el momento. Al darle Imprimir luego de verificada o introducida la información se va a generar un documento en formato pdf de la Auditoria de libros. 

Administración
-------

* Facturas 

Al seleccionar esta funcionalidad se va a mostrar una gráfica del estado de las facturas a partir del parámetro seleccionado en la pestaña Medidas. 

<img src="img/cont-101.png" alt="texto alternativo" >

* Informe de activos

Al seleccionar esta opción se muestra una interfaz con los activos existentes en la empresa. El sistema va a cargar por defecto todos los diarios registrados hasta el momento. Al darle Imprimir luego de verificada o introducida la información se va a generar un documento en formato pdf de la Auditoria de libros. 

<img src="img/cont-102.png" alt="texto alternativo" >


## Configuración 
-----------

Esta funcionalidad permite definir los Ajustes necesarios de este módulo, y configurar las diferentes operaciones contables como son Facturación, Pago y Contabilidad.

* Ajustes

Al seleccionar esta opción se muestra la pestaña Ajustes que va a permitir configurar los aspectos importantes para que el modulo funcione como se tiene definido en la empresa. Para esto se van a definir y seleccionar:

-Impuestos, se marcara el tipo de impuesto que se va a aplicar.

-Moneda, se definirá el tipo de moneda a emplear.

-Facturas del cliente, se marcaran las características de cómo se van a mostrar las facturas y los elementos que se van visualizar y a cargar al confeccionarlas. Así como las acciones que se pueden llevar a cabo con ellas. 

-Pagos de clientes, se definirá los tipos de pagos que se pueden realizar y como. 

-Facturas de proveedor, se van a definir las acciones que se pueden llevar a cabo al confeccionar las facturas.

-Pagos de proveedor, se definirá como y que se realizara para llevar a cabo el pago de  proveedor. 

-Banco y Caja, se definirán las condiciones para realizar los pagos y los tipos de importaciones bancarias.

-Periodos fiscales, se definirán las características de los periodos fiscales.

-Analítica, se seleccionaran las acciones a realizar.

-Informes, permite definir características de como saldrán los informes que se generan.

Luego de seleccionar los elementos que el usuario determine que deba de mostrar el sistema se selecciona el botón Guardar, guardando los cambios realizados. 

Facturación
----

* Plazos de pago

En esta interfaz se van listar los Plazos de pago creados en el sistema. Me va a permitir Crear, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un Plazo de pago.

<img src="img/cont-104.png" alt="texto alternativo" >

Es importante tener en cuenta que la compañía que se debe de seleccionar al introducir el nuevo Pago debe de ser con la que está registrado en el sistema, de lo contrario se generara un error. 

Entre los campos a introducir está el campo Término, para el cual se debe de Agregar una línea, y se mostrara una interfaz para crear un nuevo Término de pago.

<img src="img/cont-105.png" alt="texto alternativo" >


Luego de introducidos los datos se selecciona el botón Guardar y cerrar y se mostrara en la tabla el nuevo Término que se ha adicionado. 

* Impuestos

En esta interfaz se van a listar los Impuestos creados en el sistema. 

<img src="img/cont-106.png" alt="texto alternativo" >

Permite Crear, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un Impuesto. Además se muestran las pestañas Definición y Opciones avanzadas donde se definen las características del Impuesto creado.

<img src="img/cont-107.png" alt="texto alternativo" >

<img src="img/cont-108.png" alt="texto alternativo" >

* Posiciones fiscales

En esta interfaz se van listar las Posiciones fiscales creadas en el sistema. 

<img src="img/cont-109.png" alt="texto alternativo" >

Permite Crear, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir una Posición fiscal. Además se muestran las pestañas Mapeo de impuesto y Mapeo de cuenta, donde se van a especificar el tipo de impuesto asociado a la Posición fiscal y el tipo de cuenta.

<img src="img/cont-110.png" alt="texto alternativo" >

<img src="img/cont-111.png" alt="texto alternativo" >

* Incoterms

En esta interfaz se van listar los Incoterms creados en el sistema. 

<img src="img/cont-112.png" alt="texto alternativo" >

Permite Crear, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear me va a permitir en la misma vista del listar en la última posición introducir los datos correspondientes al nuevo Incoterms. 

<img src="img/cont-113.png" alt="texto alternativo" >

* Diarios

En esta interfaz se van listar los Diarios creados en el sistema.

<img src="img/cont-114.png" alt="texto alternativo" >

Permite Crear, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un nuevo Diario. 

Además se muestran las pestañas Asientos contables y Configuración avanzada, donde se van a especificar las cuentas empleadas, las Referencias de pago y las características del Control de acceso. En dependencia del tipo de diario que se seleccione, serán los datos que se mostraran para introducir en la pestaña Asientos contables, y es obligatorio introducir un códifo corto del Asiento contable. 

<img src="img/cont-115.png" alt="texto alternativo" >

<img src="img/cont-116.png" alt="texto alternativo" >

* Niveles de seguimiento

En esta interfaz se van listar los Seguimientos de pago.

<img src="img/cont-117.png" alt="texto alternativo" >

Permite Crear un nuevo Seguimiento, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un nuevo Seguimiento de pago. Además se muestran las pestañas Mensaje que va a mostrar un ejemplo de cómo sería el mensaje que se le enviara a modo de recordatorio del importe sin pagar, y la estructura de los datos que presentara el mismo. 

<img src="img/cont-118.png" alt="texto alternativo" >

<img src="img/cont-119.png" alt="texto alternativo" >

<img src="img/cont-120.png" alt="texto alternativo" >

Pagos
-----

* Añada una cuenta bancaria

Al seleccionar esta funcionalidad se van a mostrar todas las cuentas bancarias existentes y que se han cargado hasta el momento. Para adicionar una nueva se selecciona del listado y se da clic a la opción Connect o Cancelar en caso de no querer realizar la acción

<img src="img/cont-121.png" alt="texto alternativo" >

* Cuenta bancaria 

Al acceder a esta funcionalidad se van listar las Cuentas bancarias existentes.

<img src="img/cont-122.png" alt="texto alternativo" >

Permite Crear una nueva cuenta, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir una nueva Cuenta bancaria. 

<img src="img/cont-123.png" alt="texto alternativo" >

* Método de pago 

Al acceder a esta funcionalidad se van listar los Métodos de pago existentes.

Permite Crear e Importar un nuevo método de pago.

<img src="img/cont-124.png" alt="texto alternativo" >

Contabilidad 
----

* Plan contable 

Al acceder a esta funcionalidad se van listar los Planes contables existentes.

<img src="img/cont-125.png" alt="texto alternativo" >

Permite Crear un nuevo Plan contable, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir una nueva Cuenta bancaria. En dependencia del valor del campo Tipo, se va a especificar una acción en el campo Permitir conciliación. 

<img src="img/cont-126.png" alt="texto alternativo" >

* Grupos de diarios

Al acceder a esta funcionalidad se van listar los Grupos de diarios existentes.

Permite Crear un nuevo grupo, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un nuevo Grupo de diario. 

<img src="img/cont-127.png" alt="texto alternativo" >

* Categorías de producto

Al acceder a esta funcionalidad se van listar las Categorías de producto existentes.

<img src="img/cont-127.1.png" alt="texto alternativo" >

Permite Crear una nueva categoría de producto, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente.
Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir una nueva Categoría.
 
<img src="img/cont-128.png" alt="texto alternativo" >

* Modelos de conciliación 

Al acceder a esta funcionalidad se van listar los Modelos de conciliación existentes.

<img src="img/cont-129.png" alt="texto alternativo" >

Permite Crear un nuevo modelo , Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un nuevo Modelo de conciliación.

<img src="img/cont-130.png" alt="texto alternativo" >

<img src="img/cont-131.png" alt="texto alternativo" >

 
* Modelos de activos

Al acceder a esta funcionalidad se van listar los Modelos de activos existentes.

<img src="img/cont-132.png" alt="texto alternativo" >

Permite Crear un nuevo modelos, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un nuevo Modelo de activos.

<img src="img/cont-133.png" alt="texto alternativo" >

* Modelo de ingresos diferidos

Al acceder a esta funcionalidad se van listar los Modelos de ingresos diferidos existentes.134

Permite Crear un nuevo modelos, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un nuevo Modelo de ingresos diferidos. 

<img src="img/cont-134.png" alt="texto alternativo" >

<img src="img/cont-135.png" alt="texto alternativo" >

* Modelo de gastos diferidos 

Al acceder a esta funcionalidad se van listar los Modelos de gastos diferidos existentes.
Permite Crear un nuevo modelos, Importar y Exportar <img src="img/flecha exportar.jpg" alt="texto alternativo" > la documentación existente. Al seleccionar la opción Crear se va a mostrar una interfaz para introducir los datos necesarios para definir un nuevo Modelo de gastos diferidos.

<img src="img/cont-136.png" alt="texto alternativo" >

