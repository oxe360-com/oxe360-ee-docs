# Módulo: l10n_pe_account_invoicing_additional 

Despues de instalar el módulo **l10n_pe_account_invoicing_additional**, se van a mostrar nuevos campos en el sistema en las facturas de las ventas. 

Para visualizar estos campos se debe de acceder al módulo de **Facturación**.

<img src="img_additional/l10n_pe_account_invoicing_additional_1.png" alt="texto alternativo" >

En la pestaña *Clientes* se listan los tipos de facturas de ventas. 

<img src="img_additional/l10n_pe_account_invoicing_additional_2.png" alt="texto alternativo" >

Para verificar el funcionamiento en uno de estos tipos de facturas se va a acceder al menú *Factura de clientes*, y se va a crear una nueva factura.

<img src="img_additional/l10n_pe_account_invoicing_additional_3.png" alt="texto alternativo" >

Luego de seleccionar el botón *Crear*, en la vista que se muestra, en el campo *Tipo de operación* se va a seleccionar la operación *Venta interna – Anticipos*. De esta forma estamos creando un anticipo en el sistema. 

<img src="img_additional/l10n_pe_account_invoicing_additional_4.png" alt="texto alternativo" >

Se van a introducir todos los datos correspondientes a la nueva factura.

<img src="img_additional/l10n_pe_account_invoicing_additional_5.png" alt="texto alternativo" >

Luego de guardada y publicada esta factura con el *Tipo de operación* *Venta interna – Anticipos*, se podrá emplear en otra factura que se cree en el sistema. 

Para verificar el funcionamiento de la misma, se va a crear una nueva factura con el flujo que se describió anteriormente, pero en el campo *Tipo de operación* se va a seleccionar la opción *Venta Interna-Deducción de anticipos*.

<img src="img_additional/l10n_pe_account_invoicing_additional_6.png" alt="texto alternativo" >

Una vez que se seleccione esta opción se va a visualizar en la pestaña *Otra información* el campo *Anticipos*, con el botón *Añadir* que va a permitir cargar los anticipos definidos anteriormente. 

<img src="img_additional/l10n_pe_account_invoicing_additional_7.png" alt="texto alternativo" >

Se va a introducir el contenido de la pestaña *Líneas de facturas*. Para poder aplicar el flujo de forma correcta, el contenido de la *Línea de factura*  que se definió anteriormente con *Tipo de operación* *Venta interna – Anticipos*, debe de tener el valor similar o menor que el valor de la factura a la que se le quiere aplicar el anticipo. Logrando de esta forma que el valor no sea negativo. 

**Factura Anticipo**
<img src="img_additional/l10n_pe_account_invoicing_additional_8.png" alt="texto alternativo" >

<img src="img_additional/l10n_pe_account_invoicing_additional_9.png" alt="texto alternativo" >

Al seleccionar en el campo *Anticipos* el botón *Añadir*, se van a visualizar las facturas que se definieron con el *Tipo de operación* *Venta interna – Anticipos*, que pertenezcan al cliente con el que se creó.

<img src="img_additional/l10n_pe_account_invoicing_additional_10.png" alt="texto alternativo" >

A partir del valor definido en la suma total de los productos cargados en la pestaña *Líneas de facturas*, se va a seleccionar el anticipo más adecuado. 

Una vez seleccionado se va a visualizar el número de la factura que se seleccionó.

<img src="img_additional/l10n_pe_account_invoicing_additional_11.png" alt="texto alternativo" >

En la pestaña *Línea de facturas* se va a visualizar el valor del contenido de la factura de anticipos que se ha adicionado. A partir del valor que presenta el anticipo se realiza una resta con el valor de los productos que se han introducido.

<img src="img_additional/l10n_pe_account_invoicing_additional_12.png" alt="texto alternativo" >

En este ejemplo el valor final es **0.00**. Al *Guardar* y *Publicar* la factura, automáticamente se muestra como *Pagado*.

<img src="img_additional/l10n_pe_account_invoicing_additional_13.png" alt="texto alternativo" >

* A partir de la instalación de este módulo, se adicionaron los campos  *¿Es una guía de remisión?* Y *¿Contiene percepción?*.

Estos campos se visualizan cuando se crea una nueva *Factura de venta* (Pestaña *Cliente*).

Al seleccionar la pestaña *Otra información* se van a visualizar estos nuevos campos.

<img src="img_additional/l10n_pe_account_invoicing_additional_14.png" alt="texto alternativo" >

Al marcar el campo *¿Es una guía de remisión?* Se van a visualizar los siguientes campos para introducir los datos necesarios que debe de registrar el usuario en caso que sea una remisión. Se deben de introducir los valores del *Receptor* y los valores del *Conductor*.

<img src="img_additional/l10n_pe_account_invoicing_additional_15.png" alt="texto alternativo" >

Al marcar el campo *¿Contiene percepción?*, el sistema verifica que se hayan introducido datos en la pestaña de la *Línea de facturas*, y que el valor total no sea *cero*. En caso que el valor no sea *cero* y se pueda marcar este campo se van a visualizar los siguientes campos.

<img src="img_additional/l10n_pe_account_invoicing_additional_16.png" alt="texto alternativo" >

En el campo *Régimen de percepción* se van a visualizar las siguientes opciones.

<img src="img_additional/l10n_pe_account_invoicing_additional_17.png" alt="texto alternativo" >

Al seleccionar la opción *Percepción Venta Interna Tasa 2%*, se le va a aplicar la percepción al valor *Total* de la factura que se visualiza en el campo *Monto base de percepción* , realizando una suma del valor total y de la percepción (campo *Monto de la percepción* ) visualizando el total de la suma en el campo *Monto final + percepción*.

<img src="img_additional/l10n_pe_account_invoicing_additional_19.png" alt="texto alternativo" >

<img src="img_additional/l10n_pe_account_invoicing_additional_18.png" alt="texto alternativo" >

Este valor varía a partir del campo *Régimen de percepción aplicado*. 


