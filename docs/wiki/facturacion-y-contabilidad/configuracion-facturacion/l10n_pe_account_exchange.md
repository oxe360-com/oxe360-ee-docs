
# Módulo : l10n_pe_account_exchange 

Luego de instalado el modulo con sus dependencias, el usuario debe de definir en las pretendencia del usuario registrado, los permisos de accesos. 

Para esto debe de entrar en el módulo de **Ajustes**.

<img src="img_exchange/l10n_pe_account_exchange1.png" alt="texto alternativo" >

En la pestaña *Usuarios y compañías*, en el menú *Usuarios*, debe de seleccionar el usuario registrado en el sistema.

<img src="img_exchange/l10n_pe_account_exchange2.png" alt="texto alternativo" >

<img src="img_exchange/l10n_pe_account_exchange3.png" alt="texto alternativo" >

Dentro de las características de este usuario, debe de marcar en la pestaña *Permisos de accesos*, la opción **Ajuste por diferencia de cambio** y se selecciona el botón *Guardar*.

<img src="img_exchange/l10n_pe_account_exchange4.png" alt="texto alternativo" >

Una vez que el usuario haya marcado esta opción va a acceder al módulo **Contabilidad**.  En la pestaña *Contabilidad*, dentro del agrupador *Generar asientos*, se va a seleccionar el menú *Ajuste por diferencia de cambio*.  

<img src="img_exchange/l10n_pe_account_exchange5.png" alt="texto alternativo" >

**Ajuste por diferencia de cambio**.

Este registro va a consolidar todas las diferencias de cambio de un mes.

Al acceder a este menú se va a visualizar una vista con un botón *Crear* en caso que no existan elementos registrados. 

<img src="img_exchange/l10n_pe_account_exchange6.png" alt="texto alternativo" >

Al dar *clic* en el botón *Crear* se va a visualizar la siguiente vista. 

<img src="img_exchange/l10n_pe_account_exchange7.png" alt="texto alternativo" >

En el campo señalado se va a introducir el nombre con el que se va a identificar el registro, puede ser el mes con el año que se va a visualizar. 

En el campo *Fecha desde*, se debe de introducir el primer día del año vigente, es decir 1ro de enero, ya que el sistema verifica todos los ajustes desde enero que estén pendientes.

En el campo *Fecha hasta*, se va a poner el último día del mes al que se desea visualizar los ajustes.  

En el campo *TC Compras* se debe de introducir el *Tipo de Cambio de Compras*, vigente en ese momento.

En el *TC Ventas*, se va a introducir el *Tipo de Cambio de Ventas* vigente. 

Estos valores son los tipos de cambios del mes al que se le está realizando el registro del ajuste. Para conocer el valor del cambio se puede acceder a la siguiente página *www.sbs.gob.pe*.

Luego de cargados los datos, se va  a dar *clic* en el botón *Procesar*.

<img src="img_exchange/l10n_pe_account_exchange8.png" alt="texto alternativo" >

Luego de seleccionar el botón *Procesar*, la tabla *General* se va a cargar con todos los comprobantes registrados, que la *Moneda* que se introdujo sea diferente a la registrada en el sistema, u que aún no se hayan pagado.  

El valor que se introduce se toma a partir del menú *Todos los comprobantes*, realizando un filtro de todas las facturas que están registradas donde se ha definido otro tipo de moneda.

Como se visualiza se muestra el *Ajuste* y en el *Tipo de cambio* se va a mostrar el que se le aplico si la factura que se visualiza es de tipo *Compra* o *Venta*.

Para mayor entendimiento se muestra una de las facturas que se visualizan en la tabla.

<img src="img_exchange/l10n_pe_account_exchange9.png" alt="texto alternativo" >

