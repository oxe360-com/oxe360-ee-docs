# Módulo: l10n_pe_account_invoicing 

Una vez instalado el módulo y sus dependencias, se visualizara el módulo **Facturación**. 
Para que se muestren todos los menús pertenecientes al mismo, se debe de seleccionar en **Ajustes**, en la pestaña *Usuarios y compañías*, el menú *Usuario*.

<img src="img_invoicing/l10n_pe_account_invoicing_66.png" alt="texto alternativo" >

<img src="img_invoicing/l10n_pe_account_invoicing_67.png" alt="texto alternativo" >

Al seleccionar el usuario que está registrado en el sistema, en la pestaña *Permisos de accesos*, se debe de marcar la opción *Mostrar características de contabilidad completas*.

<img src="img_invoicing/l10n_pe_account_invoicing_68.png" alt="texto alternativo" >

Una vez que se seleccione y se guarden los cambios, al darle actualizar al sistema se van a  visualizar todos los menús del módulo **Facturación**. 
 

* Se adicionaron elementos a la funcionalidad *Diarios*. 

Para visualizar estos cambios se debe de acceder al módulo **Contabilidad**.

<img src="img_invoicing/l10n_pe_account_invoicing_1.png" alt="texto alternativo" >

Se debe de seleccionar en la pestaña *Configuración*, la funcionalidad *Diarios*.

<img src="img_invoicing/l10n_pe_account_invoicing_2.png" alt="texto alternativo" >

Al dar *clic* en esta funcionalidad se va a visualizar la lista de *Diarios* registrados en el sistema.

A esta lista se adicionaron los *Diarios* marcados en la siguiente imagen. 

Estos Diarios se van a emplear en las facturas para identificar el tipo de factura que se estará creando, tanto del Cliente como del Proveedor.

<img src="img_invoicing/l10n_pe_account_invoicing_3.png" alt="texto alternativo" >

<img src="img_invoicing/l10n_pe_account_invoicing_57.png" alt="texto alternativo" >


Al seleccionar el botón *Crear* se van a mostrar los campos necesarios para crear un nuevo *Diario*.


<img src="img_invoicing/l10n_pe_account_invoicing_25.png" alt="texto alternativo" >


Según el dato que se introduzca en el campo *Tipo* serán los campos que se van a visualizar para introducir. 

En el caso de este módulo se ven referir a los Tipos *Venta* y *Compra*. 

<img src="img_invoicing/l10n_pe_account_invoicing_26.png" alt="texto alternativo" >

Al seleccionar el Tipo  *Ventas* o Tipo *Compra* se van a mostrar los siguientes campos *Tipo Prueba de pago SUNAT* y *Annual ID*.

<img src="img_invoicing/l10n_pe_account_invoicing_27.png" alt="texto alternativo" >

Al en el campo *Tipo Prueba de pago SUNAT*, la opción  *01 Factura* o la opción *03 Boleta de venta*, se van a visualizar los campos *Factura rectificativa* y *Nota de débito*.

<img src="img_invoicing/l10n_pe_account_invoicing_28.png" alt="texto alternativo" >

<img src="img_invoicing/l10n_pe_account_invoicing_29.png" alt="texto alternativo" >


* Se adicionaron elementos a la funcionalidad *Plazos de pago*.

Para visualizar estos cambios se debe de acceder al módulo **Contabilidad**.

Dentro de este módulo se debe de acceder en la pestaña *Configuración*, a la funcionalidad *Plazos de pago*.

<img src="img_invoicing/l10n_pe_account_invoicing_56.png" alt="texto alternativo" >

Al dar *clic* en esta funcionalidad se va a visualizar la lista de *Plazos de pagos* registrados en el sistema.

A esta lista se le adicionaron los plazos  *60 días* y *90 días*.

<img src="img_invoicing/l10n_pe_account_invoicing_5.png" alt="texto alternativo" >

Los Plazos de pago que se muestran en esta funcionalidad serán los que se mostraran en el campo *Plazo de pago* al crear las facturas, posteriormente. 

<img src="img_invoicing/l10n_pe_account_invoicing_55.png" alt="texto alternativo" >


* Se adiciono contenido a la funcionalidad *Impuestos*. 

Se adicionaron los campos *Código de impuesto*, *Nombre del código fiscal*, *Código Unice* y *Tipo de afectación fiscal*.

Para visualizar estos nuevos campos se debe de acceder en el módulo **Contabilidad**, en la  pestaña *Configuración*, en el agrupador *Facturación* a la funcionalidad *Impuestos*.

<img src="img_invoicing/l10n_pe_account_invoicing_31.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se va a visualizar una lista de Impuestos existentes hasta el momento, los cuales serán los que se le aplicarán a los productos que se registran en las facturas que se crearan posteriormente. 

<img src="img_invoicing/l10n_pe_account_invoicing_52.png" alt="texto alternativo" > 


Al dar *clic* en el botón *Crear* se va a mostrar una interfaz donde se mostraran los campos necesarios para crear un nuevo Impuesto. 

<img src="img_invoicing/l10n_pe_account_invoicing_53.png" alt="texto alternativo" >

En este caso se creó como ejemplo un Impuesto con nombre Calidad, como se muestra a continuación, el cual se va a visualizar al crear una Factura. 

<img src="img_invoicing/l10n_pe_account_invoicing_54.png" alt="texto alternativo" > 

Entre los impuestos que se listan se encuentran los impuestos de *18 %* del Ámbito de impuesto de Compra y de venta.

<img src="img_invoicing/l10n_pe_account_invoicing_8.png" alt="texto alternativo" >

A estos impuestos en este módulo se le asignaron los valores  referentes a los campos *Código de impuesto*, *Nombre del código*, *Código Unice* y *Tipo de afectación fiscal*, mostrándose de la siguiente forma.

<img src="img_invoicing/l10n_pe_account_invoicing_9.png" alt="texto alternativo" >

<img src="img_invoicing/l10n_pe_account_invoicing_10.png" alt="texto alternativo" >

Estos valores se cargan automáticamente cuando seleccionamos en el campo *Código de impuesto*, la opción  *1000 - IGV Impuesto General a las Ventas*.



* Se definió un nuevo *Formato de papel*.

Para visualizar esto se debe de acceder a **Ajustes**.

<img src="img_invoicing/l10n_pe_account_invoicing_19.png" alt="texto alternativo" >

Dentro seleccionar en la opción *Herramienta de desarrollo*, una de las opciones que se visualizan. 

<img src="img_invoicing/l10n_pe_account_invoicing_20.png" alt="texto alternativo" >

Una vez que se realice esta operación se van a visualizar todas las funcionalidades pertenecientes al módulo **Ajustes** y a otros módulos.

Para visualizar lo que se ha adicionado se debe de seleccionar en **Ajuste**, en la pestaña *Técnico*, en *Informes*, la funcionalidad *Formato de papel*.

<img src="img_invoicing/l10n_pe_account_invoicing_18.png" alt="texto alternativo" >

Como se muestra se listan los Formatos de papel existentes en el sistema, y se adiciono el que se encuentra señalado.

<img src="img_invoicing/l10n_pe_account_invoicing_21.png" alt="texto alternativo" >

A este último se le especificaron los valores siguientes.

<img src="img_invoicing/l10n_pe_account_invoicing_22.png" alt="texto alternativo" >

Estos formatos de papel se van a visualizar y se le aplicaran a las facturas una vez que se acceda al botón *Enviar e imprimir*.  Dentro de las opciones que se muestran marcadas está *Imprimir*,como se muestra a continuación.

<img src="img_invoicing/l10n_pe_account_invoicing_59.png" alt="texto alternativo" >


* Se configuro el *Tipo de cambio desde internet*, a partir del *Tipo de moneda* definida en el sistema.

Para visualizar esto se debe de acceder al módulo **Contabilidad**, en la pestaña de *Configuración* la funcionalidad *Ajustes*.

<img src="img_invoicing/l10n_pe_account_invoicing_12.png" alt="texto alternativo" >

Al dar *clic* en la opción *Multi-Monedas*  se va a visualizar la moneda con la que se va a realizar la diferencia del cambio. 

<img src="img_invoicing/l10n_pe_account_invoicing_13.png" alt="texto alternativo" >

<img src="img_invoicing/l10n_pe_account_invoicing_11.png" alt="texto alternativo" >

Al especificar la opción *Multi-moneda* va a permitir realizar las operaciones en diferentes monedas, no solo en la que tiene definido el sistema por defecto. Es decir, se podrán crear facturas en otras monedas o pagarlas en una moneda diferente a la que está creada. Para esto se visualizará un campo *Moneda* en las Facturas.

<img src="img_invoicing/l10n_pe_account_invoicing_60.png" alt="texto alternativo" >

Y al seleccionar el botón *Registrar pago*, se muestra la opción para pagar la factura en diferentes monedas.

<img src="img_invoicing/l10n_pe_account_invoicing_61.png" alt="texto alternativo" >



* Se adiciono el contenido en las *Tasas monetarias*.

Luego de tener activada la opción *Multi-monedas* se va a visualizar un menú llamado *Monedas*.

Para acceder a este, dentro de la pestaña *Configuración*, en el agrupador *Contabilidad*, se va a mostrar el menú *Monedas*.

<img src="img_invoicing/l10n_pe_account_invoicing_62.png" alt="texto alternativo" >

Al acceder a este menú se va a mostrar el listado de las monedas existentes y las que están activas actualmente.

<img src="img_invoicing/l10n_pe_account_invoicing_63.png" alt="texto alternativo" >

Para saber la *Tasa de cambio* existente de alguna de estas monedas, se debe de dar *clic* encima de alguna de ellas, mostrándose una vista con la pestaña *Tasas*.  

<img src="img_invoicing/l10n_pe_account_invoicing_37.png" alt="texto alternativo" >

Al dar *clic* en esta opción  se va a mostrar los *Tipos de tasa de cambios* definidos en el sistema. 

<img src="img_invoicing/l10n_pe_account_invoicing_38.png" alt="texto alternativo" >


* Se adiciono la opción de *Tasa de cambio automática*.

Para visualizar esta opción se debe de acceder al módulo **Contabilidad**, en la pestaña *Configuración*, seleccionar la funcionalidad *Ajustes*. 

<img src="img_invoicing/l10n_pe_account_invoicing_12.png" alt="texto alternativo" >

Al dar *clic* en la opción *Multi-monedas* se va a mostrar la opción *Tasa de Cambio Automático*, con los datos definidos.

<img src="img_invoicing/l10n_pe_account_invoicing_14.png" alt="texto alternativo" >

Una vez que el usuario acceda a la opción *Actualizar*, el sistema debe de automáticamente actualizar los valores de las tasas de las monedas, asignándole a la moneda que presenta por defecto el sistema el valor 1. 

<img src="img_invoicing/l10n_pe_account_invoicing_65.png" alt="texto alternativo" >


* Se adiciono la opción *Cuenta de detracción*.

Para visualizar esta opción se debe de acceder al módulo **Contabilidad**, en la pestaña *Configuración*, seleccionar la funcionalidad *Ajustes*. 

<img src="img_invoicing/l10n_pe_account_invoicing_12.png" alt="texto alternativo" >

Se debe de seleccionar la opción *Localización peruana*, dentro se muestra la opción *Cuenta de detracción*. 

<img src="img_invoicing/l10n_pe_account_invoicing_15.0.png" alt="texto alternativo" >

Al dar *clic* a la opción se va a visualizar el campo *Detracción del diario* con  los *Diarios* de tipo *Proveedor*.

<img src="img_invoicing/l10n_pe_account_invoicing_15.png" alt="texto alternativo" >

<img src="img_invoicing/l10n_pe_account_invoicing_16.png" alt="texto alternativo" >



* Se adiciono contenido en la funcionalidad *Secuencia*.

Para visualizar esta opción se debe de acceder en **Ajustes**, en la pestaña *Técnico*, dentro del agrupador *Secuencia e identificadora*,  la funcionalidad *Secuencia*.

<img src="img_invoicing/l10n_pe_account_invoicing_17.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se va a mostrar una vista lista. A esta se le adicionaron las secuencias señaladas, con los datos correspondientes a las mismas.

<img src="img_invoicing/l10n_pe_account_invoicing_23.png" alt="texto alternativo" >


* Entre los menús existentes en el módulo de **Facturación** está *Clientes* y *Proveedores*, los  cuales van a mostrar los datos específicos de cada Cliente o Proveedor, según el contacto que se esté creando. Estos datos también se pueden visualizar mediante el módulo **Contactos**, en el menú *Contactos*.

<img src="img_invoicing/l10n_pe_account_invoicing_71.png" alt="texto alternativo" >


<img src="img_invoicing/l10n_pe_account_invoicing_72.png" alt="texto alternativo" >

Al acceder a este menú se puede visualizar la pestaña *Datos SUNAT*, la cual va a registrar automáticamente los datos generados por la *SUNAT*, a partir del *Tipo de documento* *RUC* que se le haya asignado al contacto creado.
<img src="img_invoicing/l10n_pe_account_invoicing_70.png" alt="texto alternativo" >

* En el módulo de **Contactos** se visualiza en la pestaña *Configuración* el menú *Grupo de países*.

<img src="img_invoicing/l10n_pe_account_invoicing_73.png" alt="texto alternativo" >

Al acceder a esta menú se va a mostrar los grupos de países definidos y dentro de estos grupos se van a visualizar los países que lo componen.

<img src="img_invoicing/l10n_pe_account_invoicing_74.png" alt="texto alternativo" >

Esta información se va a visualizar en el módulo de **Facturación**, en el menú *Posición Fiscal*.

<img src="img_invoicing/l10n_pe_account_invoicing_75.png" alt="texto alternativo" >



* Se visualiza la funcionalidad *Ciudades* y se posiciona dentro del agrupador *Localización*.

Para visualizar estos cambios se debe de acceder al módulo **Contactos** y dar *clic* en la pestaña *Configuración*.

<img src="img_invoicing/l10n_pe_account_invoicing_40.png" alt="texto alternativo" >


* Entre los menús del módulo **Facturación** está *Productos*, el cual se visualiza en la pestaña *Cliente* (Productos a vender) y en los *Proveedores* (Productos a comprar).

<img src="img_invoicing/l10n_pe_account_invoicing_42.png" alt="texto alternativo" >

<img src="img_invoicing/l10n_pe_account_invoicing_43.png" alt="texto alternativo" >

Al acceder a estas funcionalidades se selecciona el botón *Crear*, visualizándose la siguiente interfaz.

<img src="img_invoicing/l10n_pe_account_invoicing_44.png" alt="texto alternativo" >

En la pestaña *Información General* se van a visualizar los nuevos campos.

<img src="img_invoicing/l10n_pe_account_invoicing_41.png" alt="texto alternativo" >

Además se visualiza la opción *Ver tabla de código Sunat*, la cual al dar *clic* encima de esta nos va a redireccionar a la página *Cámara de comercio de Bogotá*.

<img src="img_invoicing/l10n_pe_account_invoicing_45.png" alt="texto alternativo" >

<img src="img_invoicing/l10n_pe_account_invoicing_46.png" alt="texto alternativo" >

Estos campos también se visualizan  al acceder a la funcionalidad *Categoría de productos*.

Para visualizarlos se debe de seleccionar en el módulo **Contabilidad**, la pestaña *Configuración*, en el agrupador *Contabilidad*, la funcionalidad *Categoría de productos*.

<img src="img_invoicing/l10n_pe_account_invoicing_47.png" alt="texto alternativo" >

Al acceder a esta funcionalidad se va a visualizar una vista, dentro de esta seleccionamos el botón *Crear*.

<img src="img_invoicing/l10n_pe_account_invoicing_48.png" alt="texto alternativo" >

Una vez realizada esta acción se van a visualizar los campos antes nombrados.

<img src="img_invoicing/l10n_pe_account_invoicing_49.png" alt="texto alternativo" >



* En el módulo **Facturación**, al acceder a la pestaña *Clientes*, se van a mostrar los distintos tipos de facturas de ventas. 

<img src="img_invoicing/l10n_pe_account_invoicing_77.png" alt="texto alternativo" >

Para crear una *Factura de cliente* se debe de acceder al menú *Factura*. 

En la opción *Filtros* se visualiza una lista de los diferentes filtros definidos en el sistema a partir de los tipos de facturas existentes.

<img src="img_invoicing/l10n_pe_account_invoicing_78.png" alt="texto alternativo" >


Al seleccionar el botón *Crear* se van a mostrar los campos definidos para crear una nueva *Factura*.
Generando una nueva factura con un número consecutivo, que va a servir para identificarla en el sistema. 

<img src="img_invoicing/l10n_pe_account_invoicing_79.png" alt="texto alternativo" >

En el campo *Fecha de factura* se va a definir la fecha en que se crea la factura, en caso que el usuario no introduzca ningún dato, al darle *clic* al botón *Guardar* y luego *Publicar* va a tomar la fecha actual automáticamente. 

En el campo *Estado del PSE* se van a visualizar los estados que se muestran en la siguiente imagen. Al seleccionar el estado *Aceptada* o *No aceptada*, luego de *Guardar* y *Publicar* la factura se va a visualizar el botón *Anular Factura*.

En el campo *Diario*, se va especificar el tipo de Diario identificando la factura que se va a crear. 

En el campo *Plazo de pago*, se va a especificar el Plazo de pago en el que se pagara la factura, estos valores se crean en el menú *Plazo de pago*.

En el campo *Tipo de documento* se especifica el tipo de documento que se va a crear, por defecto el sistema carga el valor 01- Factura, ya que se está creando una Factura de cliente.  

En el campo *Tipo de operación* se va a definir la operación a realizar, como es una *Factura de cliente* o *Factura de venta* se carga por defecto  la operación *Venta interna*. 

Luego de selecciona la opción *Multi-Moneda*, descrito anteriormente, se va a mostrar el campo *Moneda*, donde se carga por defecto la moneda definida en el sistema, pero permitiéndole al usuario, seleccionar la moneda en que se va a crear la Factura. 

<img src="img_invoicing/l10n_pe_account_invoicing_80.png" alt="texto alternativo" >

En la pestaña *Líneas de facturas*  se van a cargar los productos que se van a facturar, cargándose automáticamente el resto de los datos de los campos a partir del producto que se ha introducido. 
En el campo *Impuesto* se van a definir los impuestos que se le van a aplicar al producto introducido.
En la parte inferior se va a mostrar la suma del precio de los productos que se han cargado así como el valor de los impuestos aplicados a partir del valor del producto.

  
<img src="img_invoicing/l10n_pe_account_invoicing_81.png" alt="texto alternativo" >

En caso de que la factura presente detracción se va a marcar el campo *Detracción* visualizándose los campos correspondientes a al mismo.

<img src="img_invoicing/l10n_pe_account_invoicing_83.png" alt="texto alternativo" >


En la pestaña *Apuntes contables* se van a desglosar las cuentas aplicadas a esta factura a partir de los productos introducidos y los impuestos aplicados. 

<img src="img_invoicing/l10n_pe_account_invoicing_82.png" alt="texto alternativo" >

En la pestaña *Otra información* se van a mostrar los campos de la Factura, mostrándose el campo *Vendedor*, el cual toma por defecto el usuario registrado en el sistema, los campos de la *Contabilidad* , donde se especifica el *Término* y la *Posición fiscal* . Los datos del *Pago*, y se especifica los datos en caso de que la factura tenga detracción. 

<img src="img_invoicing/l10n_pe_account_invoicing_84.png" alt="texto alternativo" >


Al introducir todos los datos de la factura, se selecciona el botón *Guardar*, en caso que toda la información sea correcta se selecciona el botón *Publicar*.  Una vez que se publica la factura se van a mostrar los siguientes botones. 

<img src="img_invoicing/l10n_pe_account_invoicing_85.png" alt="texto alternativo" >

Al seleccionar el botón *Enviar e Imprimir* se va a mostrar un correo , con el *Nombre del cliente* que se había introducido al crear la factura , el *Número* con el que se identifica la factura, el monto a pagar y la factura en formato pdf para que el usuario tenga una copia.  

<img src="img_invoicing/l10n_pe_account_invoicing_86.png" alt="texto alternativo" >

Al seleccionar el botón *Enviar e imprimir*, se va a mostrar el formato con el que se va a visualizar el pdf de la factura creada.

<img src="img_invoicing/l10n_pe_account_invoicing_87.png" alt="texto alternativo" >

Al seleccionar el botón *Registrar pago* se va a mostrar una vista, donde se cargan los valores para realizar el pago de la factura.  Al tener la opción *Multi-moneda* seleccionada se puede pagar en otra moneda que no sea la definida en el sistema.

<img src="img_invoicing/l10n_pe_account_invoicing_88.png" alt="texto alternativo" >


<img src="img_invoicing/l10n_pe_account_invoicing_90.png" alt="texto alternativo" >

Al seleccionar el botón *Previsualizar* se va a mostrar una vista en el navegador con la factura que se ha creado.

<img src="img_invoicing/l10n_pe_account_invoicing_89.png" alt="texto alternativo" >

Al seleccionar la opción *Back to edit mode*, se vuelve a mostrar el sistema.


* A partir de la factura se puede crear una factura de tipo *Nota de débito* mediante el botón *Crear Nota de débito* o crear una factura de tipo *Nota de crédito* mediante el botón *Agregar factura rectificativa*. 

Al seleccionar el botón *Cambiar a borrador*, cambia de estado para el estado *Borrador* y pasa  a ser una *Factura borrador*. 

Al seleccionar el botón *Anular factura* el valor del campo *Estado del PSE* pasa a *Anulando*. 
 
**Nota de Crédito**

Para crear una *Nota de crédito* en el sistema, se puede hacer mediante, el botón de *Añadir factura rectificativa*, a una factura ya creada, o mediante el menú *Notas de crédito de clientes*, en caso que la factura sea de tipo *Factura de cliente*. 

En caso que la factura sea *Factura de proveedor*, se puede crear una *Nota de crédito de proveedor*. 

Para crear la nueva factura por medio del botón *Añadir factura rectificativa*, se debe de seleccionar en el menú facturas una de las facturas existentes. 

En la vista que se genera se va a visualizar el botón *Agregar factura rectificativa*.

<img src="img_invoicing/l10n_pe_account_invoicing_91.png" alt="texto alternativo" >


Para confirmar que sea correcta la creación de una *Nota de crédito* o *Factura rectificativa* a una factura existente, esta no debe de estar *Pagada*, por lo que el campo *Impuesto de adeudado* no debe de estar en 0 (cero). 


Una vez que se le dé *clic* al botón *Agregar factura rectificativa*, se va a visualizar una vista, donde se va a seleccionar que se va a hacer al crear esa *Nota de crédito*.

<img src="img_invoicing/l10n_pe_account_invoicing_93.png" alt="texto alternativo" >

También se debe de introducir el *Motivo* de la creación de esta *Nota de crédito* y la *Fecha de confección* de la misma. 

En el campo del *Diario* se debe de especificar que el tipo de diario es *Nota de crédito*, en caso de no especificarse el sistema va a asignar por defecto este tipo de diario. 
 
Es importante destacar que la *Fecha de confección* de la *Nota de crédito* debe de ser la misma que la de la factura a la que estará vinculada o superior a esta. 

Luego de introducido los datos y darle *clic* al botón *Revertir*, se va a mostrar la *Nota de crédito* que está vinculada a la factura a la cual se le realizó la misma. 

<img src="img_invoicing/l10n_pe_account_invoicing_94.png" alt="texto alternativo" >

Como el documento que se está emitiendo es una *Nota de crédito*, se le va a mostrar en la pestaña *Otra información* los campos correspondientes a la *Factura que se modifica*.

Al seleccionar el botón *Publicar* la *Nota de crédito* va a tomar un  Número con el que se va a identificar, el cual va a poseer el *id* de la Nota de crédito, el *año* en que se confecciono y un *número* consecutivo. 

<img src="img_invoicing/l10n_pe_account_invoicing_100.png" alt="texto alternativo" >

Esta nota de crédito que se ha creado a partir del botón *Agregar factura rectificativa* se va a visualizar en el menú *Notas de crédito del cliente*. Para visualizar la misma se va a seleccionar en la pestaña *Clientes*, el menú *Notas de crédito del cliente*. 

Al acceder a la misma se va a mostrar el listado de las *Notas de crédito* creadas a partir de las diferentes facturas. 
En la imagen se muestra marcada la *Nota de crédito* que se adiciono anteriormente. 

<img src="img_invoicing/l10n_pe_account_invoicing_101.png" alt="texto alternativo" >


**Nota de débito**

Para crear una *Nota de débito* a una factura existente, se debe de realizar el mismo flujo que el de la *Nota de crédito*.

Al acceder al botón *Crear nota de débito* de una factura creada, se va a visualizar la siguiente vista. Donde se debe de introducir el *Motivo* de la creación de la *Nota de débito*.

<img src="img_invoicing/l10n_pe_account_invoicing_102.png" alt="texto alternativo" >


Al seleccionar el botón *Crear Nota de débito* se va a generar una factura de tipo *Nota de débito*. 

En la pestaña *Otra información* en el contenido de la *Factura que modifica*, en el campo *Factura para la cual esta factura es la nota de crédito / débito*, se va a mostrar el *Número* de la factura a la cual se le realizó la *Nota de débito*.

<img src="img_invoicing/l10n_pe_account_invoicing_117.png" alt="texto alternativo" >

Esta facture de tipo *Nota de débito*, se va a visualizar en el menú *Nota de débito del cliente* o *Nota de débito del proveedor*, según a la factura que se le ha aplicado la *Nota de débito*.
En este caso es una *Nota de débito del cliente*.

<img src="img_invoicing/l10n_pe_account_invoicing_118.png" alt="texto alternativo" >


**Cancelar asiento**

Cuando se crea una nueva factura, y se selecciona el botón *Guardar*, esta se encuentra en estado *Borrador*, visualizándose el botón *Cancelar asiento*. 

<img src="img_invoicing/l10n_pe_account_invoicing_103.png" alt="texto alternativo" >

Al dar *clic* en este botón la factura pasa a estado *Cancelada*, y no se muestra activo ninguno de los botones. 
 
<img src="img_invoicing/l10n_pe_account_invoicing_104.png" alt="texto alternativo" >


**Recepciones**

Este menú se va a visualizar en las pestañas de *Clientes* y en la de *Proveedores*. 

Para accede al menú *Recepciones* se debe de acceder a la pestaña *Clientes* o en *Proveedores*, y dentro se muestra el menú *Recepciones*.

Al acceder a esta funcionalidad se van a listas los *Recibos de Compra* o de *Venta* según corresponda.

**Recibos de Venta**

<img src="img_invoicing/l10n_pe_account_invoicing_115.png" alt="texto alternativo" >


**Recibo de compra**

<img src="img_invoicing/l10n_pe_account_invoicing_116.png" alt="texto alternativo" >


Al seleccionar al botón *Crear* se van a mostrar los campos a introducir correspondientes al *Recibo* que se va a crear. 

En el caso de que sea un *Recibo de venta* se va a mostrar el campo *Cliente*, y en el caso de ser un *Recibo de compra* se va a mostrar el campo *Proveedor*. 
 
<img src="img_invoicing/l10n_pe_account_invoicing_105.png" alt="texto alternativo" >

El flujo para registrar un nuevo *Recibo* es similar a cuando se crea una *Factura*. Se introducen los *Productos* en las *Líneas de factura* y en la pestaña *Apunte contables* se van a mostrar las *Cuentas*, con los valores del *Debe* y del *Haber*.


Una vez que se cree el *Recibo*, este va a tomar el *Número* en dependencia del tipo de Diario que se le introduzca, el año de confección y un número consecutivo. 


<img src="img_invoicing/l10n_pe_account_invoicing_106.png" alt="texto alternativo" >


**Todos los comprobantes**

Este menú se va a mostrar en la pestaña *Clientes* y en la pestaña *Proveedores*, y en él se van a almacenar todos los tipos de facturas que se generen a partir de las *Ventas* y las *Compras*.

<img src="img_invoicing/l10n_pe_account_invoicing_107.png" alt="texto alternativo" >

En este menú se pueden crear las distintas facturas definidas en el sistema. 

Al seleccionar el botón *Crear* se debe especificar en el campo *Diario*, la factura que se va a crear y se realiza el flujo similar al definido anteriormente al crear una *Factura de cliente*. 

<img src="img_invoicing/l10n_pe_account_invoicing_108.png" alt="texto alternativo" >


**Boletas** 

Las *Boletas* es otro tipo de factura que se crea en el sistema.  
Pueden ser de *Cliente* y de *Proveedor*. 

**Boletas de clientes**: 
<img src="img_invoicing/l10n_pe_account_invoicing_109.png" alt="texto alternativo" >

**Boletas de proveedor**:

<img src="img_invoicing/l10n_pe_account_invoicing_111.png" alt="texto alternativo" >


Al crear la factura de tipo *Boleta de cliente*, el campo *Diario*, va a cargar automáticamente el tipo de *Diario* correspondiente a la misma. Lo mismo ocurre cuando es un tipo de factura *Boleta de proveedor*. También existe el campo *Tipo de documento* que especifica el tipo de documento que se está creando si es una *Boleta de venta* o una *Boleta de Compra*. 

**Boletas de clientes**: 
<img src="img_invoicing/l10n_pe_account_invoicing_110.png" alt="texto alternativo" >


**Boletas de proveedor**:
<img src="img_invoicing/l10n_pe_account_invoicing_112.png" alt="texto alternativo" >


**Resumen Diario de Boletas** 

Al crear una *Boleta de cliente*, el usuario le asigna en el campo *Estado del PSE* el valor de *Enviando*, *Enviado* o *Anulado*. Luego de determinar el *Estado del PSE* se *Guarda* y se *Publica*  la *Boleta de cliente*. 

Para visualizar el menú *Resumen Diario de Boletas*  se debe de acceder en la pestaña *Clientes* al menú con este nombre.

<img src="img_invoicing/l10n_pe_account_invoicing_113.png" alt="texto alternativo" >


Al seleccionar el botón *Crear* se va a visualizar la siguiente vista.

<img src="img_invoicing/l10n_pe_account_invoicing_114.png" alt="texto alternativo" >

En la fecha se debe de poner la *Fecha de confección* de las boletas que presenten en el campo *Estado del PSE* los valores *Enviando*, *Enviado* o *Anulado*. Luego se dará *clic* al botón *Guardar* llenando la tabla señalada con el contenido de las boletas que estén registradas en el sistema con estos estados.

<img src="img_invoicing/l10n_pe_account_invoicing_120.png" alt="texto alternativo" >

Una vez que esto ocurra se va a mostrar el botón *Enviar a SUNAT* y *Cancelar resumen*. 

Al dar clic en el botón *Enviar SUNAT* el resumen va a pasar al estado *Enviando* y se van a mostrar los campos marcados. 

<img src="img_invoicing/l10n_pe_account_invoicing_121.png" alt="texto alternativo" >

Al seleccionar el botón *Cancelar resumen*, el estado del resumen pasa a *Cancelado* y el *Estado del SUNAT* también se muestra en estado *Cancelado*. 

<img src="img_invoicing/l10n_pe_account_invoicing_122.png" alt="texto alternativo" >


**Comunicación de baja**

Para acceder a este menú en la pestaña *Clientes*, se va a seleccionar el menú nombre *Comunicación de baja*.

Este menú va a visualizar las *Facturas de cliente* en estado *Anulada*. 

Para conocer el correcto funcionamiento, en el menú *Facturas de cliente* se va a verificar que exista alguna *Factura de cliente* que el *Diario* sea *Factura electrónica*, y que el *Estado del PSE* esté en *Anulando*. 

<img src="img_invoicing/l10n_pe_account_invoicing_123.png" alt="texto alternativo" >

Al acceder al menú *Comunicación de baja*, y seleccionar el botón *Crear* se va a mostrar la siguiente vista.

<img src="img_invoicing/l10n_pe_account_invoicing_124.png" alt="texto alternativo" >

En el campo *Fecha de generación del documento*, se va a introducir la fecha, en que se desee conocer si ha ocurrido alguna baja o factura anulada. 

En este caso se va a introducir la fecha marcada en la factura que se conoce que está en estado *Anulando* y se da *clic* al botón *Guardar*. Mostrándose la información siguiente. 


<img src="img_invoicing/l10n_pe_account_invoicing_126.png" alt="texto alternativo" >

Al seleccionar el botón *Enviar a SUNAT*, se va a mostrar la siguiente información.


<img src="img_invoicing/l10n_pe_account_invoicing_127.png" alt="texto alternativo" >


Al seleccionar el botón *Cancelar comunicación de baja*  el estado pasa a *Cancelado* y el *Estado del SUNAT* también se muestra en *Cancelado*.

