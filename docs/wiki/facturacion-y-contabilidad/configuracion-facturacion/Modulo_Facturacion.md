# Módulo Facturación

La facturación es el proceso de emisión de facturas, se refiere a todos los actos relacionados con la elaboración, registro, envío y cobro de las facturas.

Para acceder al módulo de facturación en Odoo,  se debe de verificar que el mismo esté instalado para esto se debe de mostrar en la pantalla de inicio el siguiente módulo.
 
 <img src="img/Fact-0.1.png" alt="texto alternativo" >


Al acceder al sistema se muestra en la pantalla de inicio la siguiente interfaz, donde se muestra el listado de las facturas registradas hasta el momento en el sistema. 

<img src="img/Fact-1.png" alt="texto alternativo" >

Esta interfaz pertenece a la funcionalidad de Facturas de los clientes. Para acceder a ella también se puede hacer mediante la ruta Clientes/ Facturas. 
Como se muestra en la vista inicial se muestran  las pestañas Clientes, Proveedores, Informes y Configuración, los cuales a su vez engloban un grupo de funcionalidades que se describen a continuación.

Al acceder  a estas funcionalidades se muestran los botones, Crear, Importar, Cargar y Exportar <img src="img/Fact-4.png" alt="texto alternativo" >
El botón Crear permite crear un nuevo elemento en la funcionalidad donde se está trabajando.
Al seleccionar el botón Importar se muestra una nueva interfaz con los botones cargar fichero y Cancelar. Al seleccionar la opción Cargar fichero me permite seleccionar el contenido existente en el ordenador. De igual forma funciona el botón Cargar.
Al seleccionar la opción Exportar <img src="img/Fact-4.png" alt="texto alternativo" >, me va a generar un documento en formato Excel, donde se va a mostrar todos los elementos que se muestran la vista lista de estas funcionalidades. Es importante destacar que esta opción no se mostrara en el sistema si no hay ningún dato registrado. 

Recomendaciones:
Es recomendable que al empezar a trabajar con el módulo se comience en la Pestaña de Configuración, definiendo los elementos que deben de estar definidos en la empresa, en caso que ya no estén definidos, con el objetivo que sea más ameno el trabajo con el mismo. 
<img src="img/Fact-2.png" alt="texto alternativo" >


### Clientes
--------

 Al acceder a la pestaña Clientes se va a deslizar una lista de las funcionalidades que están enfocadas a los Clientes, las cuales son Facturas, Facturas rectificativas, Recepciones, Pagos, Productos  y Clientes.

Factura
--------

La factura es un documento legal que constituye y autentifica que se ha prestado o recibido un servicio o se ha comprado o vendido un producto. En la factura se incluyen todos los datos referentes a la operación y, la emisión de la misma, es de obligado cumplimiento en operaciones mercantiles.

Al acceder  a la vista Lista de la funcionalidad Facturas se muestran distintas opciones que nos permitirá configurar distintas características de este módulo.

<img src="img/Fact-3.png" alt="texto alternativo" >

* Información de compañía: Permitirá introducir todos los datos de la compañía a la que pertenece la factura que se va a introducir.<img src="img/Fact-1.1.png" alt="texto alternativo" >

* Diseño de factura: Se mostrará la estructura que va a presentar el documento con el que se van a emitir las facturas que se generaran desde el sistema. <img src="img/Fact-1.2.png" alt="texto alternativo" >

* Método de pago: Se  va a definir un método de pago con el que se van a realizar las facturas.  En dependencia del tipo de pago que se seleccione serán los datos que se mostrarán en el sistema para introducir por el usuario. <img src="img/Fact-1.3.png" alt="texto alternativo" >
<img src="img/Fact-1.5.png" alt="texto alternativo" >

* Factura ejemplo: Mediante esta opción se envía una factura para probar el portal del cliente. 
Al seleccionar el botón Crear se mostrará una interfaz para introducir los datos correspondientes a la factura que se está creando y los botones Guardar, Descartar, Publicar y Previsualizar. A su vez se muestran los estados Borrador y Publicado por los cuales va a pasar la factura.

<img src="img/Fact-5.png" alt="texto alternativo" >

Para Crear una nueva factura los campos a introducir son:

* Cliente: El nombre del cliente, al que se le está realizando la factura. En este campo el sistema va a mostrar los clientes que ya se han registrado en el sistema. En caso de ser un usuario nuevo permite crearlo e introducir los datos correspondientes al mismo, en la opción crear que se encuentra al final de los datos para seleccionar. 

* Referencia: Es el identificador con el que se va a hacer referencia a la factura.

* Fecha factura: Se va a introducir la fecha en que se confecciona la factura. Es recomendable que se ponga la fecha actual, en caso que no ser esa, la fecha no puede ser mayor a la fecha actual. 

* Plazo de pago: Va a registrar el plazo de pago que se define para el pago de la factura. Para esto el sistema tiene definido en la pestaña Configuración una funcionalidad que va a permitir registrar los Plazos de pagos definidos para el módulo, los cuales se van a cargar como selección para introducir el contenido de este campo. 

* Líneas de factura: En esta pestaña se van a introducir el contenido que se va a facturar al seleccionar la opción Agregar registró. Para esto se introducen los siguientes campos:

  -Producto: Este campo va a mostrar un campo de selección donde se van a mostrar los productos que ya se han definido en el sistema en la funcionalidad Producto que se encuentra en la pestaña Cliente. 

  -Descripción: Al seleccionar el producto que se desea facturar se va a generar la descripción del mismo, la cual ya se ha introducido al definir los Productos en la funcionalidad Producto. 

  -Cantidad: Se define la cantidad que se desea facturar del producto que se seleccionó. 

  -Precio: Este contenido se define automáticamente al introducir el Producto.

  -Impuestos: Este contenido se define en la pestaña Configuración en la funcionalidad Impuesto. Por defecto sale el 18 % ya que en la funcionalidad Ajustes, el usuario tiene la posibilidad de definir que impuesto es el que se va a generar de forma automática. De igual forma se pueden introducir otros valores definidos por el usuario que está registrando la factura. 

  -Subtotal: Este valor es la multiplicación del valor del Producto por la cantidad que se desea adquirir. 

  -Base imponible: es el valor del impuesto que se debe de generar al realizar la factura. 

  -IGV (Impuesto General a las Ventas): es el valor que se le va a adicionar a la factura a partir del impuesto que se definió. 

  -Total: El valor total que se debe de facturar. 

<img src="img/Fact-5.2.png" alt="texto alternativo" >

Al seleccionar la opción Agregar una sección se adicionara una sección que se mostrara en la factura, al igual que al seleccionar la opción Agregar nota, que se adicionara una nota. 

* Otra información: En esta pestaña se van a introducir los datos que estime el usuario que sean necesarios referentes al vendedor. Para esto se van a introducir los siguientes datos:

  -Vendedor: Se va a introducir el nombre del vendedor que está realizando la factura, en este caso el sistema toma los datos del usuario que está autenticado en el sistema.

  -Incoterm: Este contenido se definirá en la pestaña Configuración, en la funcionalidad Incoterm , que corresponde a los términos de comercio internacional que se emplean para aclarar los costes de las transacciones comerciales internacionales. En este campo permite seleccionar el que se va a emplear a partir de los que ya están definidos en el sistema. 

  -Posición fiscal: El contenido de este campo se definirá en la pestaña Configuración en la funcionalidad Posición Fiscal, donde se define un mapeo de impuestos para la empresa. Se muestra un campo de selección donde  se van a mostrar las posiciones fiscales que ya tiene definida la empresa. 

  -Referencia de pago: Es la referencia con la que se va a identificar el pago de la factura.

  -Cuenta bancaria: El contenido de este campo se definirá en la pestaña Configuración en la funcionalidad Cuentas bancarias, donde se define un mapeo de impuestos para la empresa. Se muestra un campo de selección donde  se van a mostrar las Cuentas bancarias que ya tiene definida la empresa.


<img src="img/Fact-5.1.png" alt="texto alternativo" >

<img src="img/Fact-6.png" alt="texto alternativo" >

Luego de introducidos todos los datos y seleccionar el botón Guardar , se mostraran los siguientes botones:

<img src="img/Fact-7.png" alt="texto alternativo" >

* Editar: Para editar la información que se ha introducido.

* Crear: Para crear una nueva factura.

* Publicar: Para publicar la factura que se ha creado.

* Previsualizar: Para visualizar como debe de mostrarse la factura. 

<img src="img/Previsualizarfactura.png" alt="texto alternativo" >

* Cancelar asiento: Para cancelar la factura que se registró. 

Una vez que se seleccione el botón Publicar la factura cambia de estado y pasa al estado Publicado. Una vez publicado se muestran los botones siguientes:

* Enviar e imprimir: Se va a mostrar una interfaz que permitirá seleccionar las opciones Imprimir, Enviar por correo postal y  Correo electrónico. Según la acción que se seleccione para realizar se introducirán los datos necesarios para la misma. 

<img src="img/Enviarfactura.png" alt="texto alternativo" >

Al seleccionar la opción Imprimir se va a generar un documento en formato PDF con el número de identificación de la factura y que se mostrara de la siguiente manera.

<img src="img/Enviarfactura2.png" alt="texto alternativo" >

* Registrar pago: Se va a mostrar una interfaz que me permitirá registrar el pago correspondiente a la factura. Esta interfaz corresponde a la funcionalidad Pago.  Una vez introducidos los datos y seleccionando el botón Validar, la factura se muestra como Pagada.

* Agregar factura rectificativa: Se mostrara una interfaz para introducir los datos de porque se rectifica la factura y los datos correspondientes para realizar esta acción. Si se selecciona el método de crédito Reembolso parcial la factura rectificativa va a ser creada en borrador y puede ser editada antes de expedirse.  Si se selecciona la opción Reembolso completo la factura rectificativa se valida automáticamente y se concilia con la factura. En caso que se seleccione Reembolso completo y nuevo borrador de factura,  la factura rectificativa se valida automáticamente y se concilia con la factura. La factura original se duplica como un nuevo borrador.

<img src="img/Fact-8.1.png" alt="texto alternativo" >

<img src="img/Fact-8.2.png" alt="texto alternativo" >


* Cambiar a borrador: Al seleccionar este botón la factura regresa a estado Borrador y se muestran solo los botones correspondientes a este estado.


Una vez creada la factura se va a mostrar en la vista lista, mostrándose el valor general de los montos de las distintas facturas que se han creado. 

<img src="img/Fact-9.png" alt="texto alternativo" >

Facturas rectificativas
-----

Una factura rectificativa es la factura que se emite para corregir algún error o agregar algún dato que sea necesario. Una factura rectificativa es un documento que detalla alguna corrección en la factura ordinaria o bien la devolución de la mercancía.

Al acceder  a la vista Lista de la funcionalidad Facturas rectificativas se muestran distintas opciones que nos permitirá configurar distintas características de este módulo.

* Información de compañía: Permitirá introducir todos los datos de la compañía a la que pertenece la factura que se va a introducir.

* Diseño de factura: Se mostrará la estructura que va a presentar el documento con el que se van a emitir las facturas que se generaran desde el sistema.

* Método de pago: Se  va a definir un método de pago con el que se van a realizar las facturas.  En dependencia del tipo de pago que se seleccione serán los datos que se mostrarán en el sistema para introducir por el usuario. 

* Factura ejemplo: Mediante esta opción se envía una factura para probar el portal del cliente. 

<img src="img/Fact-10.png" alt="texto alternativo" >

Al seleccionar el botón Crear se mostrará una interfaz para introducir los datos correspondientes a la Facturas rectificativas que se está creando y los botones Guardar, Descartar, Publicar y Previsualizar. A su vez se muestran los estados Borrador y Publicado por los cuales va a pasar la factura.

Para crear una nueva Factura rectificativa los campos a introducir son los mismos que se describieron en la funcionalidad Factura, descrita anteriormente. 

<img src="img/Fact-11.png" alt="texto alternativo" >

Luego de introducidos todos los datos y seleccionar el botón Guardar, se mostraran los siguientes botones:

<img src="img/Fact-7.png" alt="texto alternativo" >

* Editar: Para editar la información que se ha introducido.

* Crear: Para crear una nueva Factura rectificativa.

* Publicar: Para publicar la Factura rectificativa que se ha creado.

* Previsualizar: Para visualizar como debe de mostrarse la factura rectificativa. Una vez visualizada y seleccionar la opción Descargar se mostrara un documento en formato pdf que mostrara la factura. Para volver a la interfaz de donde se introducen los datos se selecciona la opción volver al modo de edición. 

* Cancelar asiento: Para cancelar la Factura rectificativa que se registró. 

<img src="img/Fact-12.1.png" alt="texto alternativo" >

<img src="img/Fact-12.2.png" alt="texto alternativo" >

<img src="img/Fact-12.png" alt="texto alternativo" >

Una vez que se seleccione el botón Publicar la Factura rectificativa cambia de estado y pasa al estado Publicado. 

<img src="img/Fact-13.png" alt="texto alternativo" >

* Enviar e imprimir: Se va a mostrar una interfaz que permitirá seleccionar las opciones Imprimir, Enviar por correo postal y  Correo electrónico. Según la acción que se seleccione para realizar se introducirán los datos necesarios para la misma. Al seleccionar la opción Imprimir se va a generar un documento en formato PDF con el número de identificación de la Factura rectificativa.


* Registrar pago: Se va a mostrar una interfaz que me permitirá registrar el pago correspondiente a la factura rectificativa. Esta interfaz corresponde a la funcionalidad Pago.  Una vez introducidos los datos y seleccionando el botón Validar, la factura se muestra como Pagada. 


<img src="img/Fact-13.png" alt="texto alternativo" >

<img src="img/Fact-14.png" alt="texto alternativo" >

<img src="img/Fact-15.png" alt="texto alternativo" >

Una vez que la factura esta como Pagada y se selecciona la opción Previsualizar se muestra la fecha en que se pagó incluido en la factura en la parte inferior del documento.

<img src="img/Fact-16.png" alt="texto alternativo" >

* Cambiar a borrador: Al seleccionar este botón la factura regresa a estado Borrador y se muestran solo los botones correspondientes a este estado.

Una vez creada la factura rectificativa se va a mostrar en la vista Lista, mostrándose el valor general de los montos de las distintas facturas que se han creado. 

<img src="img/Fact-17.png" alt="texto alternativo" >

Recepciones
---

La Recepción es un informe elaborado con la finalidad de especificar la cantidad y clase de materiales recibidos. Este formato sirve para crear un registro de las entradas de materiales provenientes de proveedores.

Al acceder  a la vista Lista de la funcionalidad Recepciones se muestran distintas opciones que nos permitirá configurar distintas características de este módulo.


* Información de compañía: Permitirá introducir todos los datos de la compañía a la que pertenece la factura que se va a introducir.

* Diseño de factura: Se mostrará la estructura que va a presentar el documento con el que se van a emitir las facturas que se generaran desde el sistema.

* Método de pago: Se  va a definir un método de pago con el que se van a realizar las facturas.  En dependencia del tipo de pago que se seleccione serán los datos que se mostrarán en el sistema para introducir por el usuario. 

* Factura ejemplo: Mediante esta opción se envía una factura para probar el portal del cliente.

<img src="img/Fact-18.png" alt="texto alternativo" >

Al seleccionar el botón Crear se mostrará una interfaz para introducir los datos correspondientes al Recibo de venta que se está creando y los botones Guardar, Descartar y Publicar. A su vez se muestran los estados Borrador y Publicado por los cuales va a pasar el Recibo de venta.

<img src="img/Fact-19.png" alt="texto alternativo" >

Para crear un nuevo Recibo de venta los campos a introducir son:
* Cliente: El nombre del cliente. En este campo el sistema va a mostrar los clientes que ya se han registrado en el sistema En caso de ser un usuario nuevo permite crearlo e introducir los datos correspondientes al mismo, en la opción crear que se encuentra al final de los datos para seleccionar. 

* Referencia: Es el identificador con el que se va a hacer referencia del recibo.

* Fecha factura: Se va a introducir la fecha de la factura. Es recomendable que se ponga la fecha actual, en caso que no ser esa, la fecha no puede ser mayor a la fecha actual. 

* Plazo de pago: Va a registrar el plazo de pago que se define para el pago de la factura. Para esto el sistema tiene definido en la pestaña Configuración una funcionalidad que va a permitir registrar los Plazos de pagos definidos para el módulo, los cuales se van a cargar como selección para introducir el contenido de este campo. 

* Líneas de factura: En esta pestaña se van a introducir el contenido que se va a facturar al seleccionar la opción Agregar registró. Para esto se introducen los siguientes campos:

  -Producto: Este campo va a mostrar un campo de selección donde se van a mostrar los productos que ya se han definido en el sistema en la funcionalidad Producto que se encuentra en la pestaña Cliente. 

  -Descripción: Al seleccionar el producto que se desea facturar se va a generar la descripción del mismo, la cual ya se ha introducido al definir los Productos en la funcionalidad Producto. 

  -Cantidad: Se define la cantidad que se desea facturar del producto que se seleccionó. 

  -Precio: Este contenido se define automáticamente al introducir el Producto.

  -Impuestos: Este contenido se define en la pestaña Configuración en la funcionalidad Impuesto. Por defecto sale el 18 % ya que en la funcionalidad Ajustes, el usuario tiene la posibilidad de definir que impuesto es el que se va a generar de forma automática. De igual forma se pueden introducir otros valores definidos por el usuario. 

  -Subtotal: Este valor es la multiplicación del valor del Producto por la cantidad que se desea adquirir. 
  -Base imponible: es el valor del impuesto que se debe de genera. 

  -IGV (Impuesto General a las Ventas): es el valor que se le va a adicionar a la factura a partir del impuesto que se definió. 

  -Total: El valor total que se debe de facturar. 

<img src="img/Fact-20.png" alt="texto alternativo" >

Luego de introducidos todos los datos y seleccionar el botón Guardar, se mostraran los siguientes botones:

* Editar: Para editar la información que se ha introducido.

* Crear: Para crear un nuevo Recibo de venta.

* Publicar: Para publicar el Recibo de venta que se ha creado.

Una vez que se seleccione el botón Publicar el Recibo de venta cambia de estado y pasa al estado Publicado y se muestran los siguientes botones:

* Registrar pago: Se va a mostrar una interfaz que me permitirá registrar el pago correspondiente al recibo, esta interfaz corresponde a la funcionalidad Pago. Una vez introducidos los datos y seleccionando el botón Validar, el recibo se muestra como Pagado y solo permite Cambiar a borrador el recibo. 

<img src="img/Fact-21.png" alt="texto alternativo" >

* Cambiar a borrador: Al seleccionar este botón el recibo regresa a estado Borrador y se muestran solo los botones correspondientes a este estado.
Una vez creado el  Recibo de venta se va a mostrar en la vista Lista. 

<img src="img/Fact-22.png" alt="texto alternativo" >

Pagos
----

Es la transacción (monetaria o no) por la que se extingue una deuda.
Al seleccionar esta funcionalidad se muestra en una vista Lista con los datos registrados hasta el momento. Permite Crear nuevos pagos, Importar un archivo ya existente y Exportar <img src="img/Fact-4.png" alt="texto alternativo" >.

<img src="img/Fact-23.png" alt="texto alternativo" >

Al seleccionar el botón Crear se mostrará una interfaz para introducir los datos correspondientes al Pago que se está registrando y los botones Guardar, Descartar, Confirmar y Cancelar. A su vez se muestran los estados Borrador, Validado, Conciliado y Cancelado por los cuales va a pasar el Pago.

Al seleccionar la opción Crear se muestra una interfaz con los datos necesarios para realizar esta operación. 

* Tipo de pago: Según el dato que se seleccione serán los datos que se deban de introducir. 
Por defecto en el sistema sale marcada la opción de Recibir dinero, en caso que se seleccione la opción Enviar dinero se mostrara un nuevo campo llamado Método de pago, de tipo selección donde se debe de seleccionar si va a ser Manual o Cheques, en caso de que sea Cheque se mostrara un campo para introducir el Número de cheque. 

Si se marca Transferencia interna se visualizara un campo denominado Transferir a, donde se seleccionara si es Bank o Cash (Banco o efectivo), y los campos Tipo de empresa y Socio se pondrán invisibles, ya que no se debe de introducir datos en los mismos. 

* Importe: Monto que se va a pagar.

* Fecha: Fecha en la que se va realizar el pago, tomando por defecto la fecha actual.

* Circular: Número que se va a mostrar para identificar el documento de pago. 

<img src="img/Fact-24.png" alt="texto alternativo" >

Al seleccionar el botón Confirmar el pago pasa al estado validado y se muestran los botones Imprimir cheque y Restablecer borrador.

<img src="img/Fact-25.png" alt="texto alternativo" >

Al seleccionar la opción Imprimir cheque se muestra una interfaz donde se debe de introducir el Número del cheque y seleccionar el botón Imprimir. 

<img src="img/Fact-26.png" alt="texto alternativo" >

Al seleccionar el botón Restablecer a borrador el Pago cambia hacia el estado Borrador.

Productos
----
En esta funcionalidad se van a registrar todos los productos que se ofertan hasta el momento. 
Al seleccionar esta funcionalidad se muestra en una vista Lista con los datos registrados hasta el momento. Permite Crear nuevos Productos, Importar un archivo ya existente y Exportar <img src="img/Fact-4.png" alt="texto alternativo" >.

<img src="img/Fact-27.png" alt="texto alternativo" >

Para Crear un nuevo Producto se selecciona el botón crear y se va a mostrar una interfaz con los campos necesarios para registrar el nuevo Producto.

<img src="img/Fact-28.png" alt="texto alternativo" >

* Nombre del producto: Nombre con el que se va a identificar el producto que se está creando.

* Opciones Puede ser vendido o Puede ser comprado o los dos.

Pestaña Información general que incluye:
<img src="img/Fact-29.png" alt="texto alternativo" >

* Tipo de producto: Puede ser Consumible o Servicio.

* Categoría de producto: Este campo permite seleccionar la Categoría de producto a partir de las categorías que están registradas en el sistema.

* Referencia interna: Es la referencia que va a tener el producto.

* Código de barra: Código con el que se va a identificar para realizar la compra. 

* Precio de venta: El valor con el que se va a ofertar el Producto.

* Impuesto de cliente: Impuesto que se define para este Producto.

* Costo: Costo del impuesto a aplicar. 

* Notas internas: Notas que se mostraran sobre el producto.

* Pestaña Variantes:
<img src="img/Fact-30.png" alt="texto alternativo" >

  -Atributo: Características del producto, pueden ser Patas, Color o Duración.

  -Valores: Este va a tomar valor a partir del Atributo que se va a describir. 

* Pestaña Compra:
<img src="img/Fact-31.png" alt="texto alternativo" >

  -Impuesto del proveedor: Se va a definir el impuesto que será aplicado por el proveedor. 
Una vez que se introducen todos los datos y seleccionar la opción Guardar se muestra el botón Configurar variantes. Al seleccionar este botón se muestra una vista Lista con las Variantes  que se asignaron en la pestaña variantes, y se muestra la opción Exportar (Imagen 4), posibilitando generar un documento de tipo Excel. 

 Una vez que se haya creado el Producto correctamente se va a mostrar en la vista Lista de la funcionalidad. 

<img src="img/Fact-32.png" alt="texto alternativo" >

Clientes
---
En esta funcionalidad se van a mostrar los Clientes existentes hasta el momento. Permite Crear nuevos e Importar  uno ya existente.

<img src="img/Fact-33.png" alt="texto alternativo" >

Al seleccionar la opción Crear se van a mostrar todos los datos necesarios para registrar un usuario el cual puede ser Individual o Compañía. 

<img src="img/Fact-34.png" alt="texto alternativo" >

<img src="img/Fact-35.png" alt="texto alternativo" >

<img src="img/Fact-36.png" alt="texto alternativo" >

<img src="img/Fact-37.png" alt="texto alternativo" >

Al seleccionar la opción Guardar se registra el Cliente de forma correcta y se muestran los botones Editar y Crear.

<img src="img/Fact-38.png" alt="texto alternativo" >

Se muestra el usuario creado en la vista principal de la funcionalidad. 

### Proveedores

 Al acceder a la pestaña Proveedores  se va a deslizar una lista de las funcionalidades que están enfocadas a ellos, las cuales son Facturas, Facturas rectificativas, Recepciones, Pagos, Productos  y Proveedores.

Factura
---

Al acceder  a la vista Lista de la funcionalidad Facturas se muestran distintas opciones que nos permitirá configurar distintas características de este módulo.

* Información de compañía: Permitirá introducir todos los datos de la compañía a la que pertenece la factura que se va a introducir. 

* Diseño de factura: Se mostrará la estructura que va a presentar el documento con el que se van a emitir las facturas que se generaran desde el sistema.

* Método de pago: Se  va a definir un método de pago con el que se van a realizar las facturas.  En dependencia del tipo de pago que se seleccione serán los datos que se mostrarán en el sistema para introducir por el usuario.

* Factura ejemplo: Mediante esta opción se envía una factura para probar el portal del cliente. 
Al seleccionar el botón Crear se mostrará una interfaz para introducir los datos correspondientes a la factura que se está creando y los botones Guardar, Descartar y Publicar. A su vez se muestran los estados Borrador y Publicado por los cuales va a pasar la factura.

<img src="img/Fact-39.png" alt="texto alternativo" >

Para crear una nueva factura los campos a introducir son:

* Primer número: Este valor se genera automáticamente del sistema. 

* Proveedor: El nombre del Proveedor, al que se le está realizando la factura. En este campo el sistema va a mostrar los clientes que ya se han registrado en el sistema. En caso de ser un usuario nuevo permite crearlo e introducir los datos correspondientes al mismo, en la opción crear que se encuentra al final de los datos para seleccionar. 

* Referencia: Es el identificador con el que se va a hacer referencia a la factura.

* Fecha factura: Se va a introducir la fecha en que se confecciona la factura. Es recomendable que se ponga la fecha actual, en caso que no ser esa, la fecha no puede ser mayor a la fecha actual. 

* Fecha contable: Se va a introducir la fecha actual del sistema, en caso que no ser esa, la fecha no puede ser mayor a la fecha actual.

* Completar automáticamente: En caso de introducir un nombre del proveedor que coincide con alguno ya creado se puede seleccionar el nombre que se va a mostrar en este campo y se cargaran todos los datos relacionados al mismo.

* Plazo de pago: Va a registrar el plazo de pago que se define para el pago de la factura. Para esto el sistema tiene definido en la pestaña Configuración una funcionalidad que va a permitir registrar los Plazos de pagos definidos para el módulo, los cuales se van a cargar como selección para introducir el contenido de este campo. 

* Líneas de factura: En esta pestaña se van a introducir el contenido que se va a facturar al seleccionar la opción Agregar registró. Para esto se introducen los siguientes campos:

  -Producto: Este campo va a mostrar un campo de selección donde se van a mostrar los productos que ya se han definido en el sistema en la funcionalidad Producto que se encuentra en la pestaña Cliente. 

  -Descripción: Al seleccionar el producto que se desea facturar se va a generar la descripción del mismo,  la cual ya se ha introducido al definir los Productos en la funcionalidad Producto. 

  -Cantidad: Se define la cantidad que se desea facturar del producto que se seleccionó. 

  -Precio: Este contenido se define automáticamente al introducir el Producto.

  -Impuestos: Este contenido se define en la pestaña Configuración en la funcionalidad Impuesto. Por defecto sale el 18 % ya que en la funcionalidad Ajustes, el usuario tiene la posibilidad de definir que impuesto es el que se va a generar de forma automática. De igual forma se pueden introducir otros valores definidos por el usuario que está registrando la factura. 

  -Subtotal: Este valor es la multiplicación del valor del Producto por la cantidad que se desea adquirir. 

  -Base imponible: es el valor del impuesto que se debe de generar al realizar la factura. 

  -IGV (Impuesto General a las Ventas): es el valor que se le va a adicionar a la factura a partir del impuesto que se definió. 

  -Total: El valor total que se debe de facturar. 

<img src="img/Fact-40.png" alt="texto alternativo" >

Al seleccionar la opción  Agregar una sección se adicionara una sección que se mostrara en la factura, al igual que al seleccionar la opción Agregar nota, que se adicionara una nota. 

* Otra información: En esta pestaña se van a introducir los datos que estime el usuario que sean necesarios referente a la Contabilidad y a los Pagos.

  -Incoterm: Este contenido se definirá en la pestaña Configuración, en la funcionalidad Incoterm , que corresponde a los términos de comercio internacional que se emplean para aclarar los costes de las transacciones comerciales internacionales. En este campo permite seleccionar el que se va a emplear a partir de los que ya están definidos en el sistema. 

  -Posición fiscal: El contenido de este campo se definirá en la pestaña Configuración en la funcionalidad Posición Fiscal, donde se define un mapeo de impuestos para la empresa. Se muestra un campo de selección donde  se van a mostrar las posiciones fiscales que ya tiene definida la empresa. 

  -Referencia de pago: Es la referencia con la que se va a identificar el pago de la factura.

  -Cuenta bancaria: El contenido de este campo se definirá en la pestaña Configuración en la funcionalidad Cuentas bancarias, donde se define un mapeo de impuestos para la empresa. Se muestra un campo de selección donde  se van a mostrar las Cuentas bancarias que ya tiene definida la empresa.

<img src="img/Fact-41.png" alt="texto alternativo" >

Luego de introducidos todos los datos y seleccionar el botón Guardar, se mostraran los siguientes botones:

* Editar: Para editar la información que se ha introducido.

* Crear: Para crear una nueva factura.

* Publicar: Para publicar la factura que se ha creado.

Una vez que se seleccione el botón Publicar la factura cambia de estado y pasa al estado Publicado. Una vez publicado se muestran los botones siguientes:

* Registrar pago: Se va a mostrar una interfaz que me permitirá registrar el pago correspondiente a la factura. Esta interfaz corresponde a la funcionalidad Pago.  Una vez introducidos los datos y seleccionando el botón Validar, la factura se muestra como Pagada.

* Agregar factura rectificativa: Se mostrara una interfaz para introducir los datos de porque se rectifica la factura y los datos correspondientes para realizar esta acción. Si se selecciona el método de crédito Reembolso parcial la factura rectificativa va a ser creada en borrador y puede ser editada antes de expedirse.  Si se selecciona la opción Reembolso completo la factura rectificativa se valida automáticamente y se concilia con la factura. En caso que se seleccione Reembolso completo y nuevo borrador de factura,  la factura rectificativa se valida automáticamente y se concilia con la factura. La factura original se duplica como un nuevo borrador.

* Cambiar a borrador: Al seleccionar este botón la factura regresa a estado Borrador y se muestran solo los botones correspondientes a este estado.

Luego de agregar una Factura rectificativa de un proveedor, empleando como método de crédito Reembolso parcial, y seleccionar el botón Invertir se muestra la siguiente interfaz. Como se muestra se crea un  borrador de la factura antes creada en estado borrador, siendo está una factura rectificativa y se adiciona el botón Cancelar asiento para cancelar la factura que se registró.

Al seleccionar el botón Cancelar asiento la factura pasa al estado Cancelado. 

<img src="img/Fact-42.png" alt="texto alternativo" >

Una vez creada la factura se va a mostrar en la vista lista, mostrándose el valor general de los montos de las distintas facturas que se han creado. 

<img src="img/Fact-43.png" alt="texto alternativo" >

Facturas rectificativas
-----
Una factura rectificativa es la factura que se emite para corregir algún error o agregar algún dato que sea necesario. Una factura rectificativa es un documento que detalla alguna corrección en la factura ordinaria o bien la devolución de la mercancía.
Al acceder  a la vista Lista de la funcionalidad Facturas rectificativas se muestran distintas opciones que nos permitirá configurar distintas características de este módulo.

* Información de compañía: Permitirá introducir todos los datos de la compañía a la que pertenece la factura que se va a introducir.

* Diseño de factura: Se mostrará la estructura que va a presentar el documento con el que se van a emitir las facturas que se generaran desde el sistema.

* Método de pago: Se  va a definir un método de pago con el que se van a realizar las facturas.  En dependencia del tipo de pago que se seleccione serán los datos que se mostrarán en el sistema para introducir por el usuario. 

* Factura ejemplo: Mediante esta opción se envía una factura para probar el portal del cliente. 

<img src="img/Fact-44.png" alt="texto alternativo" >

Al seleccionar el botón Crear se mostrará una interfaz para introducir los datos correspondientes a la Facturas rectificativas que se está creando y los botones Guardar, Descartary Publicar. A su vez se muestran los estados Borrador y Publicado por los cuales va a pasar la factura.
Para crear una nueva Factura rectificativa los campos a introducir son los mismos que se describieron en la funcionalidad Factura, descrita anteriormente omitiendo el campo  Primer número. 

<img src="img/Fact-45.png" alt="texto alternativo" >

Luego de introducidos todos los datos y seleccionar el botón Guardar, se mostraran los siguientes botones:

<img src="img/Fact-46.png" alt="texto alternativo" >

* Editar: Para editar la información que se ha introducido.

* Crear: Para crear una nueva Factura rectificativa.

* Publicar: Para publicar la Factura rectificativa que se ha creado.

* Cancelar asiento: Para cancelar la Factura rectificativa que se registró. Al seleccionar este botón el estado pasa a Cancelado, y se muestra el botón  Cambiar a borrador.

Cambiar a borrador: Al seleccionar este botón la factura regresa a estado Borrador y se muestran solo los botones correspondientes a este estado.

Una vez que se seleccione el botón Publicar la Factura rectificativa cambia de estado y pasa al estado Publicado. 
<img src="img/Fact-47.png" alt="texto alternativo" >

* Registrar pago: Se va a mostrar una interfaz que me permitirá registrar el pago correspondiente a la factura rectificativa. Esta interfaz corresponde a la funcionalidad Pago.  Una vez introducidos los datos y seleccionando el botón Validar, la factura se muestra como Pagado.

<img src="img/Fact-48.png" alt="texto alternativo" >

Una vez creada la Factura rectificativa se va a mostrar en la vista Lista, mostrándose el valor general de los montos de las distintas facturas que se han creado. 

<img src="img/Fact-49.png" alt="texto alternativo" >

Recepciones
---
La Recepción es un informe elaborado con la finalidad de especificar la cantidad y clase de materiales recibidos. Este formato sirve para crear un registro de las entradas de materiales provenientes de proveedores.

Al acceder  a la vista Lista de la funcionalidad Recepciones se muestran distintas opciones que nos permitirá configurar distintas características de este módulo.

* Información de compañía: Permitirá introducir todos los datos de la compañía a la que pertenece la factura que se va a introducir.

* Diseño de factura: Se mostrará la estructura que va a presentar el documento con el que se van a emitir las facturas que se generaran desde el sistema.

* Método de pago: Se  va a definir un método de pago con el que se van a realizar las facturas.  En dependencia del tipo de pago que se seleccione serán los datos que se mostrarán en el sistema para introducir por el usuario. 

* Factura ejemplo: Mediante esta opción se envía una factura para probar el portal del cliente. 

<img src="img/Fact-50.png" alt="texto alternativo" >

Al seleccionar el botón Crear se mostrará una interfaz para introducir los datos correspondientes al Recibo de compras que se está creando y los botones Guardar, Descartar y Publicar. A su vez se muestran los estados Borrador y Publicado por los cuales va a pasar el Recibo de venta.

<img src="img/Fact-51.png" alt="texto alternativo" >

Para crear un nuevo Recibo de compras los campos a introducir son:

* Proveedor: El nombre del Proveedor. En este campo el sistema va a mostrar los proveedores que ya se han registrado en la funcionalidad En caso de ser un usuario nuevo permite crearlo e introducir los datos correspondientes al mismo, en la opción crear que se encuentra al final de los datos para seleccionar.

* Referencia: Es el identificador con el que se va a hacer referencia del recibo.

* Fecha factura: Se va a introducir la fecha de la factura. Es recomendable que se ponga la fecha actual, en caso que no ser esa, la fecha no puede ser mayor a la fecha actual. 

* Plazo de pago: Va a registrar el plazo de pago que se define para el pago de la factura. Para esto el sistema tiene definido en la pestaña Configuración una funcionalidad que va a permitir registrar los Plazos de pagos definidos para el módulo, los cuales se van a cargar como selección para introducir el contenido de este campo. 

* Líneas de factura: En esta pestaña se van a introducir el contenido que se va a facturar al seleccionar la opción Agregar registró. Para esto se introducen los siguientes campos:

  -Producto: Este campo va a mostrar un campo de selección donde se van a mostrar los productos que ya se han definido en el sistema en la funcionalidad Producto que se encuentra en la pestaña Cliente. 

  -Descripción: Al seleccionar el producto que se desea facturar se va a generar la descripción del mismo, la cual ya se ha introducido al definir los Productos en la funcionalidad Producto. 

  -Cantidad: Se define la cantidad que se desea facturar del producto que se seleccionó. 

  -Precio: Este contenido se define automáticamente al introducir el Producto.

  -Impuestos: Este contenido se define en la pestaña Configuración en la funcionalidad Impuesto. Por defecto sale el 18 % ya que en la funcionalidad Ajustes, el usuario tiene la posibilidad de definir que impuesto es el que se va a generar de forma automática. De igual forma se pueden introducir otros valores definidos por el usuario. 

  -Subtotal: Este valor es la multiplicación del valor del Producto por la cantidad que se desea adquirir.

  -Base imponible: es el valor del impuesto que se debe de genera. 

  -IGV (Impuesto General a las Ventas): es el valor que se le va a adicionar a la factura a partir del impuesto que se definió. 

  -Total: El valor total que se debe de facturar. 

<img src="img/Fact-52.png" alt="texto alternativo" >

Luego de introducidos todos los datos y seleccionar el botón Guardar, se mostraran los siguientes botones:

* Editar: Para editar la información que se ha introducido.

* Crear: Para crear un nuevo Recibo de venta.

* Publicar: Para publicar la Factura rectificativa que se ha creado.

* Cancelar asiento: Para cancelar la Factura rectificativa que se registró. Al seleccionar este botón el estado pasa a Cancelado, y se muestra el botón  Cambiar a borrador.

* Cambiar a borrador: Al seleccionar este botón la factura regresa a estado Borrador y se muestran solo los botones correspondientes a este estado.

<img src="img/Fact-53.png" alt="texto alternativo" >

Una vez que se seleccione el botón Publicar la Factura rectificativa cambia de estado y pasa al estado Publicado.  
<img src="img/Fact-54.png" alt="texto alternativo" > 

* Registrar pago: Se va a mostrar una interfaz que me permitirá registrar el pago correspondiente a la factura rectificativa. Esta interfaz corresponde a la funcionalidad Pago.  Una vez introducidos los datos y seleccionando el botón Validar, la factura se muestra como Pagado.

Una vez creado el  Recibo de compra se va a mostrar en la vista Lista. 

<img src="img/Fact-55.png" alt="texto alternativo" >

Pagos
----
Es la transacción (monetaria o no) por la que se extingue una deuda.
Al seleccionar esta funcionalidad se muestra en una vista Lista con los datos registrados hasta el momento. Permite Crear nuevos pagos, Importar un archivo ya existente y Exportar <img src="img/Fact-4.png" alt="texto alternativo" >.

<img src="img/Fact-56.png" alt="texto alternativo" >

Al seleccionar el botón Crear se mostrará una interfaz para introducir los datos correspondientes al Pago que se está registrando y los botones Guardar, Descartar, Confirmar y Cancelar. A su vez se muestran los estados Borrador, Validado, Conciliado y Cancelado por los cuales va a pasar el Pago.
Al seleccionar la opción Crear se muestra una interfaz con los datos necesarios para realizar esta crear un Pago. 

* Tipo de pago: Según el dato que se seleccione serán los datos que se deban de introducir. 
Por defecto en el sistema sale marcada la opción de Recibir dinero, en caso que se seleccione la opción Enviar dinero se mostrara un nuevo campo llamado Método de pago, de tipo selección donde se debe de seleccionar si va a ser Manual o Cheques, en caso de que sea Cheque se mostrara un campo para introducir el Número de cheque. 
Si se marca Transferencia interna se visualizara un campo denominado Transferir a, donde se seleccionara si es Bank o Cash (Banco o efectivo), y los campos Tipo de empresa y Socio se pondrán invisibles, ya que no se debe de introducir datos en los mismos. 

* Importe: Monto que se va a pagar.

* Fecha: Fecha en la que se va realizar el pago, tomando por defecto la fecha actual.

* Circular: Número que se va a mostrar para identificar el documento de pago. 

* Diario: Es el tipo de moneda en que se realizará el pago. 

<img src="img/Fact-57.png" alt="texto alternativo" >

Al seleccionar el botón Confirmar el pago pasa al estado Validado y se muestran el botón Restablecer a borrador.
<img src="img/Fact-58.png" alt="texto alternativo" >

Al seleccionar el botón Restablecer a borrador el Pago cambia hacia el estado Borrador.
Luego de registrados los datos correctamente se muestran en la vista Lista. 

<img src="img/Fact-59.png" alt="texto alternativo" >

Productos
----
En esta funcionalidad se van a registrar todos los productos que se ofertan hasta el momento. 
Al seleccionar esta funcionalidad se muestra en una vista Lista con los datos registrados hasta el momento. Permite Crear nuevos Productos, Importar un archivo ya existente y Exportar <img src="img/Fact-4.png" alt="texto alternativo" >.
<img src="img/Fact-60.png" alt="texto alternativo" >

Para Crear un nuevo Producto se selecciona el botón crear y se va a mostrar una interfaz con los campos necesarios para registrar el nuevo Producto.

<img src="img/Fact-61.png" alt="texto alternativo" >

* Nombre del producto: Nombre con el que se va a identificar el producto que se está creando.

* Opciones Puede ser vendido o Puede ser comprado o los dos.

* Pestaña Información general que incluye:
  -Tipo de producto: Puede ser Consumible o Servicio.

  -Categoría de producto: Este campo permite seleccionar la Categoría de producto a partir de las categorías que están registradas en el sistema.

  -Referencia interna: Es la referencia que va a tener el producto.

  -Código de barra: Código con el que se va a identificar para realizar la compra. 

  -Precio de venta: El valor con el que se va a ofertar el Producto.

  -Impuesto de cliente: Impuesto que se define para este Producto.

  -Costo: Costo del impuesto a aplicar. 

  -Notas internas: Notas que se mostraran sobre el producto.

* Pestaña Variantes:
  -Atributo: Características del producto, pueden ser Patas, Color o Duración.

  -Valores: Este va a tomar valor a partir del Atributo que se va a describir. 

* Pestaña Compra:
  -Impuesto del proveedor: Se va a definir el impuesto que será aplicado por el proveedor. 

Una vez que se introducen todos los datos y seleccionar la opción Guardar se muestra el botón Configurar variantes. Al seleccionar este botón se muestra una vista Lista con las Variantes  que se asignaron en la pestaña variantes, y se muestra la opción Exportar <img src="img/Fact-4.png" alt="texto alternativo" >, posibilitando generar un documento de tipo Excel. 

Una vez que se haya creado el Producto correctamente se va a mostrar en la vista Lista de la funcionalidad. 

Proveedores
---
En esta funcionalidad se van a mostrar los Proveedores existentes hasta el momento. Permite Crear nuevos e Importar  uno ya existente.

<img src="img/Fact-62.png" alt="texto alternativo" >

Al seleccionar la opción Crear se van a mostrar todos los datos necesarios para registrar un usuario el cual puede ser Individual o Compañía. 

<img src="img/Fact-63.png" alt="texto alternativo" >

Al seleccionar la opción Guardar se registra el Proveedor y se muestran los botones Editar y Crear. 

### Informes

 Al acceder a la pestaña Informes  se va  a mostrar la funcionalidad Administración que va a generar el informe de todas las Facturas registradas hasta el momento.  

Facturas
---

Se va a mostrar una gráfica a partir de la selección de los parámetros que se muestran en la pestaña Medidas. Al seleccionar la fecha que se muestra en ese botón se van a desplegar los parámetros bajos los cuales se muestran las gráficas. Estos son: 

* Cantidad productos
* Importe adecuado
* Número de líneas
* Precio medio
* Total
* Total libre de impuestos 
* Cuenta


<img src="img/Fact-64.png" alt="texto alternativo" >

### Configuración

 Al acceder a la pestaña Configuración  se van a mostrar las funcionalidades correspondientes a la configuración del módulo, las cuales son Ajustes, Facturación (incluye Plazo de pago, Impuestos, Posiciones Fiscales, Incoterms, Diarios), Pagos (Añada una cuenta bancaria, Cuentas bancarias, Método de pago), Contabilidad (Grupos de Diarios, Modelos de conciliación).

 Ajustes
------

Al seleccionar esta opción se muestra la pestaña Ajustes que va a permitir configurar los aspectos importantes para que el modulo funcione como se tiene definido en la empresa. Para esto se van a definir y seleccionar:

* Impuestos

<img src="img/Fact-65.png" alt="texto alternativo" >

Como se muestra se cargarán en las facturas de comprar y venta el impuesto al 18%, esto se puede modificar según se estime. De igual forma se pueden seleccionar las operaciones referentes a los impuestos que se pueden aplicar.

* Moneda

<img src="img/Fact-66.png" alt="texto alternativo" >

Se selecciona el tipo de moneda que se va a cargar por defecto y permite seleccionar si va a ser transacciones en monedas extranjeras, es decir multimonedas.

* Facturas de cliente 

<img src="img/Fact-67.png" alt="texto alternativo" >

Se van a poder definir como saldrían las facturas de los clientes, y se va a poder adicionar el Término y condiciones predeterminados que se va a mostrar en la parte inferior de la factura.

<img src="img/Fact-67.1.png" alt="texto alternativo" >

* Pagos de clientes 

<img src="img/Fact-68.png" alt="texto alternativo" >

Se mostrara las distintas opciones para seleccionar como se desea que se realice el pago de los clientes. 

* Facturas de proveedor 

<img src="img/Fact-69.png" alt="texto alternativo" >

Va a permitir configurar cómo será la factura del proveedor, si se va a poder Digitalizar y de qué forma, así como la seleccionar de una Línea de factura por impuesto.

 * Pagos de proveedor 

<img src="img/Fact-70.png" alt="texto alternativo" >

* Analítica

<img src="img/Fact-71.png" alt="texto alternativo" >

Al terminar de definir los Ajustes, se debe de seleccionar el botón Guardar, y se registrarán los cambios. 

Facturación
-----
 
**Plazos de pago**

Se va a generar una vista lista con todos los Plazos de pagos definidos en el sistema.

<img src="img/Fact-72.png" alt="texto alternativo" >

Al seleccionar la opción Crear se va a mostrar una interfaz con los campos necesarios para definir un Plazo de pago.

<img src="img/Fact-73.png" alt="texto alternativo" >

* Plazo de pago: Va a ser como se va a identificar para el usuario este tipo de plazo de pago.

* Descripción de la factura: Se va a poner una breve descripción donde se explique el Plazo de pago para el cliente. 

* Términos

Aquí se va a incluir el Tipo de vencimiento, donde se va a poder introducir el Tipo de termino y las características del mismo.

<img src="img/Fact-73.1.png" alt="texto alternativo" >

Es obligatorio que el ultimo Tipo de Termino que se introduzca sea de tipo Balance, donde se va a garantizar que se va a asignar la cantidad por completo, en caso contrario el sistema lanzara un error que imposibilitara registra el Plazo de pago. 

<img src="img/Fact-73.2.png" alt="texto alternativo" >

Al introducir todos los datos se selecciona el botón Guardar y se registra el nuevo Plazo de pago en el sistema.

<img src="img/Fact-73.3.png" alt="texto alternativo" >

Para archivar un Plazo de pago, se puede realizar accediendo a la opción Acción que se muestra al seleccionar un Plazo de pago específico, y se selecciona la opción Archivo. Se lanza un mensaje de confirmación, y el Plazo de pago pasa al estado Archivado, y no se mostrara en la vista lista de la funcionalidad.

<img src="img/archivo.png" alt="texto alternativo" >

Para ver las informaciones archivadas en el sistema, se debe de seleccionar la opción Filtros y seleccionar la opción Archivados, donde se mostrará una vista con todos los datos archivados hasta el momento. Al acceder a ellos se puede cambiar el estado realizando la misma operación que para archivarlo, pero saldrá la opción Desarchivar.

<img src="img/archivo1.1.png" alt="texto alternativo" >

<img src="img/archivo1.2.png" alt="texto alternativo" >

**Impuestos**

Se va a generar una vista lista con todos los Impuestos registrados en el sistema hasta el momento. 

<img src="img/Fact-74.png" alt="texto alternativo" >

Al seleccionar el botón Crear se va a mostrar una interfaz con todos los campos necesarios para crear un nuevo Impuesto.

<img src="img/Fact-75.png" alt="texto alternativo" >

Para esto se van a introducir los datos:

* Nombre del impuesto: El nombre con el que se va a identificar el impuesto en el sistema.

* Calculo de impuesto: Se va a seleccionar como va a ser el cálculo del impuesto.

* Activo: Se selecciona si el impuesto va a estar disponible para ser empleado, es decir activo. 

* EDI peruvian code: Se debe de seleccionar el tipo de código peruano que se emplera.

* EDI UNECE code: Se va a definir el código para la UNECE.

* Ámbito del impuesto: Se va a seleccionar cual va a ser el ámbito del impuesto que se está definiendo.

* Importe: Importe del impuesto.

De igual forma se van a definir otros campos específicos para el Impuesto en las pestañas Definición y Opciones avanzadas. 

Al seleccionar la opción Guardar se va a registrar el nuevo Impuesto y se va a mostrar en la vista lista de esta funcionalidad. 

Los impuestos también se pueden Archivar, el proceso se realiza de igual forma a como se realiza en el Plazo de pago, que se ha descrito anteriormente.

En el caso de los impuestos se va seguir mostrando el Impuesto una vez archivado en la vista lista, pero se va a encontrar desactivado. Para activarlo se puede hacer mediante la opción Activo en la vista lista o mediante el Filtro y Desarchivar.  

**Posición fiscal**

Se va a generar una vista lista con todos las Posiciones fiscales registradas en el sistema hasta el momento. 

<img src="img/Fact-76.png" alt="texto alternativo" >

Al seleccionar la opción Crear se van a mostrar los campos necesarios para crear una nueva Posición fiscal.
 
<img src="img/Fact-77.png" alt="texto alternativo" >

* Posición fiscal: Nombre con el que se identificara la nueva Posición fiscal. 

* Detectar automáticamente: Si se selecciona esta opción se generan los siguientes campos:

  -Requiere IVA: Definiendo si requiere aplicarle un Impuesto al Valor Agregado.

  -Grupo de países: Se selecciona el grupo de países Europa, Sudáfrica, o el que defina el usuario.

  -País: Este valor se va a cargar a partir del Grupo de países que se seleccione. 

  -Mapeo de impuestos: Se definirán los datos de los Impuestos para el producto y los Impuestos para aplicar. 

<img src="img/Fact-78.png" alt="texto alternativo" >

Luego de introducir todos los campos correspondientes a la nueva funcionalidad, se selecciona el botón Guardar, y se va a mostrar la nueva Posición fiscal en la vista lista de la funcionalidad. 

<img src="img/Fact-79.png" alt="texto alternativo" >

Las Posiciones fiscales también se pueden Archivar, el proceso se realiza de igual forma a como se realiza en el Plazo de pago, que se ha descrito anteriormente.

**Incoterms**

Al seleccionar esta funcionalidad se van a generar todos los Incoterms (términos de 3 letras) que se han definido en el sistema.

<img src="img/Fact-80.png" alt="texto alternativo" >

Al seleccionar el botón Crear, permitirá agregar un nuevo Incoterm en la última posición del listado que se muestra en la vista lista. 

<img src="img/Fact-81.png" alt="texto alternativo" >

Luego de introducidos los datos se selecciona la opción Guarda y guarda el nuevo elemento. 

**Diarios**

Al acceder a esta funcionalidad se va a mostrar un listado de los Diarios registrados hasta el momento.

<img src="img/Fact-82.png" alt="texto alternativo" >

Al seleccionar el botón Crear se muestran  los campos necesarios para crear un nuevo Diario.

<img src="img/Fact-83.png" alt="texto alternativo" >

* Nombre del diario: Nombre con el que se va a identificar el nuevo Diario en el sistema.

* Tipo: Este campo va a mostrar 5 tipos, en dependencia al tipo que se seleccione se cargaran los campos necesarios en las pestañas posteriores o se mostrara una nueva con los campos necesarios para describir el Diario:

*Ventas*

<img src="img/ventas-fac0.png" alt="texto alternativo" >

<img src="img/ventas-fac.png" alt="texto alternativo" >

*Compras*

<img src="img/compra-fac0.png" alt="texto alternativo" >

<img src="img/compra-fac.png" alt="texto alternativo" >

*Efectivo*

<img src="img/Efectivo-fac0.png" alt="texto alternativo" >

<img src="img/Efectivo-fac.png" alt="texto alternativo" >

*Banco*

<img src="img/banco1.png" alt="texto alternativo" >

<img src="img/banco2.png" alt="texto alternativo" >

<img src="img/banco3.png" alt="texto alternativo" >

*Varios*

<img src="img/varios1.png" alt="texto alternativo" >

<img src="img/varios2.png" alt="texto alternativo" >

Al seleccionar el botón Guardar se crea el nuevo Diario y se muestra en la vista lista de la funcionalidad.

<img src="img/Fact-84.png" alt="texto alternativo" >

Pagos
----

**Añada una cuenta bancaria** 

Al seleccionar la funcionalidad Añada una cuenta bancaria, se van a mostrar los distintos tipos opciones de cuentas bancarias existentes. Al seleccionar una de ellas se va a activar el botón Connect, permitiendo conectarse a la cuenta bancaria que se seleccionó. 

<img src="img/Fact-85.png" alt="texto alternativo" >

<img src="img/Fact-86.png" alt="texto alternativo" >

**Cuentas bancarias**

Al seleccionar la funcionalidad Cuentas bancarias, se va a mostrar el listado de las cuentas registradas en el sistema hasta el momento.

<img src="img/Fact-87.png" alt="texto alternativo" >

Al seleccionar el botón Crear se va a mostrar una interfaz con los campos necesarios para crear una nueva Cuenta bancaria.

<img src="img/Fact-88.png" alt="texto alternativo" >

Al seleccionar el valor en el campo Cuenta bancaria se debe de verificar que la cuenta que se está introduciendo o que se seleccione corresponda con la cuenta bancaria del usuario que está registrado en el sistema, porque de lo contrario se va a lanzar un mensaje de error, impidiendo realizar la operación.

<img src="img/Fact-89.png" alt="texto alternativo" >

Luego de introducir todos los campos y seleccionar el botón Guardar se muestra el nuevo elemento en la vista lista de esta funcionalidad.

<img src="img/Fact-90.png" alt="texto alternativo" >

**Método de pago**

Al seleccionar la funcionalidad Método de pago, se va a mostrar el listado de los Métodos de pagos registrados en el sistema hasta el momento.

<img src="img/Fact-91.png" alt="texto alternativo" >

Al seleccionar el botón Crear se va a mostrar una interfaz con los campos necesarios para crear un nuevo Método de pago.

<img src="img/Fact-92.png" alt="texto alternativo" >

En dependencia al Estado que se seleccione que tenga el Método de pago, es como se mostrará al guardar la información en la vista lista.

<img src="img/Fact-93.png" alt="texto alternativo" >

Contabilidad
-----

**Grupos de diarios**

Al seleccionar la funcionalidad Grupos de diarios, se va a mostrar el listado de los grupos registrados en el sistema hasta el momento.

<img src="img/Fact-94.png" alt="texto alternativo" >

Al seleccionar el botón Crear, se pueden introducir los datos correspondientes al nuevo Grupo de diarios en la vista lista.

<img src="img/Fact-95.png" alt="texto alternativo" >

Para esto se introduce el nombre con el que se va a identificar el Grupo de diario, y  se seleccionan los Diarios que se van a excluir, en caso que se vaya a asignar alguno, este contenido se va a mostrar en la funcionalidad Diario.

<img src="img/Fact-96.png" alt="texto alternativo" >

<img src="img/Fact-97.png" alt="texto alternativo" >

**Modelos de conciliación** 

Al seleccionar la funcionalidad Modelos de conciliación, se va a mostrar el listado de los Modelos registrados en el sistema hasta el momento.
Al seleccionar el botón Crear se va a mostrar una interfaz con los campos necesarios para crear un nuevo modelo.
En dependencia del Tipo que se seleccione serán los campos que se mostraran para introducir.

*Crear un descuadre manualmente haciendo clic en el botón*

<img src="img/Fact-98.png" alt="texto alternativo" >

*Sugerir valores de contrapartida*

<img src="img/Fact-99.png" alt="texto alternativo" >

*Casar con las facturas existentes* 
<img src="img/Fact-100.png" alt="texto alternativo" >

Luego de introducidos los datos y seleccionar el botón Guardar el nuevo Modelo de conciliación se muestra en la vista lista.



