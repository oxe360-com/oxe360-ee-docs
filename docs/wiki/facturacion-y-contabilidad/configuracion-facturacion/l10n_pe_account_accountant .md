
# Módulo: l10n_pe_account_accountant 
_________

* Se visualizan 3 tipos de informes 

-*Estado de Situación Financiera*

-*Estado de Resultados por Función*

-*Estado de Resultados por Naturaleza*

De cada uno de estos informes se va a mostrar los elementos que los componen, con la información que se ha generado hasta el momento.  

Para visualizar esta información se debe de acceder al módulo **Contabilidad**.

<img src="img_accountant/10n_pe_account_accountant_2.png" alt="texto alternativo" >

Al seleccionar la pestaña *Informes*, dentro se van a mostrar estos 3 tipos de reportes.

<img src="img_accountant/10n_pe_account_accountant_1.png" alt="texto alternativo" >

Al dar *clic* dentro de cada uno de ellos se va a mostrar la información que los compone.

  * **Estado de Situación Financiera**.

<img src="img_accountant/10n_pe_account_accountant_3.png" alt="texto alternativo" >

* **Estado de Resultados por Función**.

<img src="img_accountant/10n_pe_account_accountant_4.png" alt="texto alternativo" >

* **Estado de Resultados por Naturaleza**.

<img src="img_accountant/10n_pe_account_accountant_5.png" alt="texto alternativo" >

* Se incorporó el Diario *Asiento de Amarre*, y se le asignaron características al mismo.

Para visualizar esto dentro del módulo **Contabilidad** se debe de seleccionar la pestaña *Configuración* y dentro del agrupador *Facturación* está el menú *Diarios*.

<img src="img_accountant/10n_pe_account_accountant_6.png" alt="texto alternativo" >

Al dar *clic* en este menú se va a mostrar el listado de Diarios existente y entre ellos está *Asiento de Amarre*.

<img src="img_accountant/10n_pe_account_accountant_7.png" alt="texto alternativo" >

Al dar *clic* en el mismo se van a visualizar sus datos específicos.

<img src="img_accountant/10n_pe_account_accountant_8.png" alt="texto alternativo" >

* Se visualizan nuevos campos a las *Cuentas* de los *Apuntes contables*.

Para visualizar esto se debe de acceder en el módulo de **Contabilidad** a la pestaña *Contabilidad** y dentro del agrupador Varios* se selecciona *Asientos contables*.

<img src="img_accountant/10n_pe_account_accountant_9.png" alt="texto alternativo" >

Al acceder al menú *Asientos contables* y dar *clic* en el botón *Crear* se va a mostrar una vista, en la cual en la pestaña *Apuntes contables*, se va *Agregar una línea*. Entre los datos a introducir está el de crear una nueva *Cuenta*, si lo desea.

<img src="img_accountant/10n_pe_account_accountant_10.png" alt="texto alternativo" >

Al crear una nueva cuenta se va a visualizar la siguiente vista, con los campos que se han adicionado marcados en rojo. Para que se visualicen todos los campo nuevos el campo *Tener cuenta objetivo*, debe de estar marcado.

<img src="img_accountant/10n_pe_account_accountant_11.png" alt="texto alternativo" >

* Se adiciono el menú *Ajuste por Diferencia de Tipo de Cambio – Masivo*.

Para visualizar las características del mismo, se debe de acceder en el módulo de **Contabilidad**, a la pestaña *Contabilidad* y dentro del agrupador *Acciones*, se va a mostrar el menú *Ajuste por Diferencia de Tipo de Cambio – Masivo*.

<img src="img_accountant/10n_pe_account_accountant_12.png" alt="texto alternativo" >

Al acceder a este menú se va a mostrar una vista con los siguientes campos.

<img src="img_accountant/10n_pe_account_accountant_13.png" alt="texto alternativo" >

Al introducir los datos y seleccionar el botón *Ajustes*, se va a visualizar una nueva vista con los datos del ajuste según la fecha introducida, en la cual por defecto se carga la actual. 

<img src="img_accountant/10n_pe_account_accountant_14.png" alt="texto alternativo" >

Como la fecha que se introdujo es en septiembre el periodo que se va a visualizar es el mes de septiembre.

Para proceder con  esta acción se va a seleccionar el botón **Si**, y en caso contrario se selecciona el botón **No**.


