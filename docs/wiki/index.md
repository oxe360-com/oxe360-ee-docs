Este es el módulo básico de localización contable peruana, cedido a ODOO para que sea asimilado como parte estándar a partir de la versión 12 en adelante.
Este módulo será instalado como parte de la contabilidad, siempre y cuando se escoja Perú al momento de iniciar la BD de nuestra empresa, es importante ya que en caso no se haya establecido un código del país o no se haya encontrado un módulo de localización, el módulo de localización l10n_generic_coa (US) se instala de forma predeterminada.

Pasos para Instalación de un entorno:
 - Instalar el idioma de Perú
 - Cambiar en el usuario el idioma y la zona horaria.
 - Cambiarle la clave al usuario administrador.
 - Instalar módulos conversaciones y contactos.
 - Colocar el idioma español por defecto para la creación de partners.
 - Instalar módulos calendario y notas.
 - Instalar módulo de l10n_pe - Perú-Contabilidad
 - Instalar módulo de l10n_pe_oxe360 - Peru-Accounting
 - Probar creando algunos contactos con RUC: persona natural y jurídica.
 - Instalar módulo l10n_pe_account_invoicing - Peru - Account Invoicing
 - Activar la opción de multimoneda si fuese un requerimiento del cliente.
 - Desactivar la moneda Euros - EUR
 - Verificar que la taza por defecto de la moneda dólares sea 1.
 - Actualizar las tasas de cambio desde enero para la moneda soles.
 

                                                     FEATURES

*   [Carga del Plan de Cuentas](Carga-del-Plan-de-Cuentas)
*   [Creación de Properties Plan de Cuentas](Creacion-de-Properties-Plan-de-Cuentas)
*   [Carga de Impuestos](Carga-de-Impuestos)
*   [Seteo de Perú como País por defecto en Partners](Seteo-de-Perú-como-País-por-defecto-en-Partners)
*   [Validar que el doc_number por doc_type sea correcto](Valida-que-el-doc_number-por-doc_type-sea-correcto)
*   [Seteo de doc_number en Partners de prueba](Seteo-valores-en-doc_number)
*   [Estructura de Ubigeo](Estructura-de-ubigeo)
*   [Tipos de Documentos y Datos de Contacto](Tipos-de-Documento-y-datos-de-contacto-SUNAT)
*   [Validando Dependecias entre Pais - Departamento - Provincia - Distrito](Validando Dependecias entre Pais-Departamento-Provincia-Distrito)
*   [Descripción funcional del Modulo CRM](modulo_crm)
*   [Descripción funcional del Modulo Cenit Integration](Modulo_cenit_base)
*   [Descripción funcional del Modulo Contabilidad](Modulo_Contabilidad)

